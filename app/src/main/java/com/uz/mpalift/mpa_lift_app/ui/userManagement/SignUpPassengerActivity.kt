package com.uz.mpalift.mpa_lift_app.ui.userManagement

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.uz.mpalift.mpa_lift_app.module.DBConfig
import com.uz.mpalift.mpa_lift_app.module.entities.DrivingLicence
import com.uz.mpalift.mpa_lift_app.module.entities.MpaLiftUser
import com.uz.mpalift.mpa_lift_app.module.userManagement.DateConverter.Companion.dateToStringWithoutTime
import com.uz.mpalift.mpa_lift_app.module.userManagement.UserRepository
import com.uz.mpalift.mpa_lift_app.ui.home.MainActivity
import com.uz.mpalift.mpa_lift_app.utils.PreferenceHandler
import com.uz.mpalift.mpa_lift_app.utils.SecurityUtils
import com.uzalz.mpa_lift_app.R
import kotlinx.android.synthetic.main.activity_sign_up_passenger.*
import kotlinx.android.synthetic.main.content_sign_in_passenger.*
import kotlinx.android.synthetic.main.prompt_driver_ui_enter_destination.*
import java.io.IOException
import java.util.*

class SignUpPassengerActivity : AppCompatActivity() {


    var pref = PreferenceHandler(this)

    var isUserDriver = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_passenger)

        var bundle = intent.extras
        isUserDriver = bundle!!.getBoolean("isDriver")


        setUpToolBar()
        showView()




    }



    private fun showView(){



        layout_signup_passenger.visibility = View.VISIBLE
      //  layout_choose.visibility = View.INVISIBLE

        nameTextChange()
        emailTextChange()
        passwordTextChange()

        button_passenger_pic_image.setOnClickListener {

            openGallery()
        }


        button_signup_passenger_next.setOnClickListener {


            if(!isProfilePicEmpty() && isUserNameValid) {
                // label_signup_passenger.visibility = View.VISIBLE
                frame_signup_pass_username.visibility = View.GONE
                frame_signup_pass_email.visibility = View.VISIBLE

            }else if(isProfilePicEmpty()){
                var label = "your pic is required!"
                var snack = Snackbar.make(tv_input_passengerName, label, Snackbar.LENGTH_LONG)
               // snack.anchorView = button_signup_passenger_next
                snack.show()

            }


        }


        button_passenger_complete.setOnClickListener {
          //  progressBarSignUpPassenger.visibility = View.VISIBLE

            if(isEmailValid && isPassValid && !isProfilePicEmpty()) {
                publishThumbNailAndRegister()
            }
            isEmailValid =  false
            isPassValid= false
            isUserNameValid = false

        }

    }


    private fun setUpToolBar() {

        //    toolbar_main.logo = resources.getDrawable(R.drawable.ic_logo)
        // toolbar_main.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(toolbar_signup_passenger)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //  supportActionBar!!.setIcon(R.drawable.ic_logo)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        /*
        val str = SpannableStringBuilder("Vacuum")
        str.setSpan(StyleSpan(Typeface.BOLD), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        toolbar.title = str

        supportActionBar!!.title = str

         */
    }



    private fun isProfilePicEmpty():Boolean{

        if(posterImageFullPath== null || TextUtils.isEmpty(posterImageFullPath.toString())){
            return true
        }
        return false
    }
    private fun isUserNameEmpty():Boolean{

        //  var email = tv_input_passenger_email.text.toString()
        var user = tv_input_passengerName.text.toString()

        if(TextUtils.isEmpty(user)){

            return true
        }
        return false
    }
    private fun isUserEmailEmpty():Boolean{

        var email = tv_input_passenger_email.text.toString()
        var pass = tv_input_passenger_password.text.toString()

        if(TextUtils.isEmpty(email) || TextUtils.isEmpty(pass)){

            return true
        }
        return false
    }






    private var isUserNameValid = false
    private fun nameTextChange(){

        tv_input_passengerName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.toString().trim { it <= ' ' }.isEmpty()) {
                    tv_input_passengerName.error = "Field cannot be left blank."
                    isUserNameValid =false
                }
                else if (charSequence.toString().trim().length < 3) {
                    tv_input_passengerName.error = "Username too short!!."
                }else{
                    isUserNameValid = true
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }

    private var isEmailValid = false
    private fun emailTextChange(){

        tv_input_passenger_email.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.toString().trim { it <= ' ' }.isEmpty()) {
                    tv_input_passenger_email.error = "Field cannot be left blank."

                }else if(!SecurityUtils.isEmailValid(charSequence)){
                    tv_input_passenger_email.error = "Enter valid email address."
                }else {
                    isEmailValid = true
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }

    private var isPassValid = false
    private fun passwordTextChange(){

        tv_input_passenger_password.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.toString().trim { it <= ' ' }.isEmpty()) {
                    tv_input_passenger_password.error = "Field cannot be left blank."
                    isPassValid = false
                }else if (charSequence.toString().trim().length < 7) {
                    tv_input_passengerName.error = "password too short!!.(7)"
                    isPassValid = false
                }else{
                  isPassValid = true
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }



    private fun openGallery() {
        //val intent = Intent()
        //intent.type = "video/*"
        //intent.action = Intent.ACTION_GET_CONTENT
        // startActivityForResult(Intent.createChooser(intent, "Select Video"),REQUEST_CODE)
        if (Build.VERSION.SDK_INT < 20) {
            // var photoPickerIntent = Intent(Intent.ACTION_PICK);
            // photoPickerIntent.type = "video/*"
            // photoPickerIntent.setType("image/* video/*");
            //val  mimetypes = arrayOf("image/*", "video/*")
            //photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
            //startActivityForResult(photoPickerIntent, REQUEST_CODE)
        } else {
            var photoPickerIntent = Intent(Intent.ACTION_GET_CONTENT)
            photoPickerIntent.type = "image/*";
            startActivityForResult(photoPickerIntent,
                REQUEST_CODE_IMG
            )
        }



    }

    private  var posterImageFullPath : Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_IMG){

            if (data?.data != null) {

                posterImageFullPath = data.data!!
                Log.d("PublishActivity", "Image: "+posterImageFullPath);
                try {

                    var contentResolver = contentResolver

                    var bitmap = MediaStore.Images.Media.getBitmap(contentResolver,posterImageFullPath)
                    button_passenger_pic_image.setImageBitmap(bitmap)

                } catch (e: IOException) {
                    e.printStackTrace();
                }
            }

        }
    }
    // UploadImage method


    private fun publishThumbNailAndRegister() {


        var storageReference = DBConfig.firebaseStorage.reference

        //publish assets first then get their url and save the data

        // Code for showing progressDialog while uploading
        var progressDialog : ProgressDialog = ProgressDialog(this)
        progressDialog.setTitle("Registering...")
        progressDialog.show()

        // Defining the child of storageReference
        storageReference  = storageReference.child("passengerImages/"+ UUID.randomUUID().toString())


        // Upload from a local file
        // adding listeners on upload
        // or failure of image
        var uploadTask =  storageReference.putFile(posterImageFullPath!!)
        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener {
            // Handle unsuccessful uploads

            // Error, Image not uploaded
            progressDialog.dismiss();
            Toast.makeText(baseContext, "Failed " + it.message, Toast.LENGTH_SHORT).show();

        }.addOnSuccessListener { taskSnapshot ->
            // taskSnapshot.metadata contains file metadata such as size, content-type, etc.

            // Image uploaded successfully
            // Dismiss dialog
            progressDialog.dismiss();
            //Toast.makeText(baseContext, "Image Uploaded!!"+taskSnapshot.uploadSessionUri, Toast.LENGTH_SHORT).show()

            storageReference.downloadUrl.addOnSuccessListener { downloadUri ->

                //add the download url to the driver
                //  posterImageDownloadUrl = downloadUri.toString()
                registerPassenger(downloadUri.toString())


            }.addOnFailureListener {
                // Handle any errors
            }



        }.addOnProgressListener {taskSnapshot ->
            var  progress = (100.0 * taskSnapshot.bytesTransferred / taskSnapshot.totalByteCount)
            progressDialog.setMessage("Uploaded $progress%")
        }

    }


    var userRepository = UserRepository()

    private fun registerPassenger(downloadUrl: String){

         //var pass= SecurityUtils.encryptPassword(tv_input_passenger_password.text.toString())
         var pass =  tv_input_passenger_password.text.toString()
         var passenger = MpaLiftUser()


        passenger.passengerId = DBConfig.user!!.uid
        passenger.passengerFirstName = tv_input_passengerName.text.toString()
        passenger.passengerEmail = tv_input_passenger_email.text.toString()
        passenger.passengerPassword = pass
        passenger.passengerPicUrl = downloadUrl

        var driverDrivingLicence = DrivingLicence()
        if(isUserDriver){
            passenger.isDrivingLicenceActivated = true
            driverDrivingLicence.userId = DBConfig.user!!.uid
            driverDrivingLicence.dateIssued = dateToStringWithoutTime(Date())
            driverDrivingLicence.licenceId = UUID.randomUUID().toString()

            userRepository.registerAsDriver(driverDrivingLicence,object:UserRepository.RegisterDriverCallback{

                override fun onSuccess() {

                    addPassenger(passenger)
                }

                override fun onError(error: String) {

                }

            })
        }else{

            addPassenger(passenger)

        }

    }


    private fun addPassenger(passenger:MpaLiftUser){

        userRepository.registerPassenger(passenger,object:
            UserRepository.RegisterPassengerCallback{

            override fun onSuccess() {

                openHomeScreen(passenger.isDrivingLicenceActivated)
            }

            override fun onError(error: String) {

            }

        })


    }

    private fun openHomeScreen(isDriver: Boolean){
        progressBarSignUpPassenger.visibility = View.INVISIBLE

        if(isDriver){
            pref.setIntValue(PreferenceHandler.KEY_USER_SESSION, 2)
        }else{
            pref.setIntValue(PreferenceHandler.KEY_USER_SESSION, 1)
        }

        val intent = Intent(baseContext, MainActivity::class.java)
        startActivity(intent)
        finish()

    }



    companion object{

        private const val REQUEST_CODE_IMG = 1

    }



}
