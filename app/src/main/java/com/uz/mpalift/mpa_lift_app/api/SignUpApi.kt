package com.uz.mpalift.mpa_lift_app.api

import retrofit2.http.Body
import retrofit2.http.POST

interface SignUpApi {


        /*
        @GET("auth/{phoneNumber}")
        fun activateRegistration(@Path("phoneNumber") phone:String): Observable<String>
         */

        @POST("addUser/")
        fun requestSignUp(@Body user:SignUpRequest): SignUpRequest

        //what I remember here the request is normally an http request
        //but the controller gets the request and converts it into an object request the use case understands


}