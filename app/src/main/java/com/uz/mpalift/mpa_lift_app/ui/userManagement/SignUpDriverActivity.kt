package com.uz.mpalift.mpa_lift_app.ui.userManagement

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import com.uzalz.mpa_lift_app.R
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpDriverActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_sign_up)
        setUpToolBar()

       // navHost()


    }

    private fun navHost(){
        val finalHost = NavHostFragment.create(R.navigation.nav_graph)
        supportFragmentManager.beginTransaction()
            .replace(R.id.nav_graph_signup, finalHost)
            .setPrimaryNavigationFragment(finalHost) // equivalent to app:defaultNavHost="true"
            .commit()
    }

    private fun setUpToolBar() {

        //    toolbar_main.logo = resources.getDrawable(R.drawable.ic_logo)
        // toolbar_main.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //  supportActionBar!!.setIcon(R.drawable.ic_logo)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        /*
        val str = SpannableStringBuilder("Vacuum")
        str.setSpan(StyleSpan(Typeface.BOLD), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        toolbar.title = str

        supportActionBar!!.title = str

         */
    }


    /*
    private fun createSignInIntent() {
        // [START auth_fui_create_intent]
        // Choose authentication providers
        val providers = arrayListOf(
            //AuthUI.IdpConfig.EmailBuilder().build(),
            AuthUI.IdpConfig.PhoneBuilder().build(),
            //  AuthUI.IdpConfig.FacebookBuilder().build(),
            //    AuthUI.IdpConfig.TwitterBuilder().build() ,
            AuthUI.IdpConfig.GoogleBuilder().build())


        var logo = R.drawable.logo_icon_google_guideline_final_tranparent

        // this.getResources().getDrawable(R.drawable.login_background);

        // Create and launch sign-in intent
        startActivityForResult(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .setTheme(R.style.FullscreenTheme)
                .setLogo(logo)
                .build(),
            RC_SIGN_IN
        )
        // [END auth_fui_create_intent]
    }

     */








}
