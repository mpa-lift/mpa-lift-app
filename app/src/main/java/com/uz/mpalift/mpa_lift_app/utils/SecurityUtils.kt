package com.uz.mpalift.mpa_lift_app.utils

import android.R.attr
import android.R.attr.password
import android.R.id.message
import android.util.Patterns
import com.scottyab.aescrypt.AESCrypt
import java.security.GeneralSecurityException


class SecurityUtils {


     companion object{

         val key = "###1234mpalift3000"



         fun isEmailValid(email: CharSequence): Boolean {
             return Patterns.EMAIL_ADDRESS.matcher(email).matches()
         }

         fun decryptPassword(encryptedMsg:String){
             try {
                 val messageAfterDecrypt = AESCrypt.decrypt(key, encryptedMsg)
             } catch (e: GeneralSecurityException) { //handle error - could be due to incorrect password or tampered encryptedMsg
             }
         }


         fun encryptPassword(password:String):String{
             var encryptedMsg= ""
             try {
                  encryptedMsg = AESCrypt.encrypt(password, key)
             } catch (e: GeneralSecurityException) { //handle error
                // encryptedMsg = e.message!!
             }
             return encryptedMsg
         }


     }




}