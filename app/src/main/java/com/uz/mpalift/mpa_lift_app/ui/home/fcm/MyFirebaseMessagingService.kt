package com.uz.mpalift.mpa_lift_app.ui.home.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.location.Location
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.JsonObject
import com.uz.mpalift.controllers.MapUtils
import com.uz.mpalift.mpa_lift_app.ui.home.MainActivity
import com.uz.mpalift.mpa_lift_app.ui.home.RequestResponse
import com.uz.mpalift.mpa_lift_app.utils.PreferenceHandler
import com.uzalz.mpa_lift_app.R
import java.util.*

class MyFirebaseMessagingService : FirebaseMessagingService() {

    /**
     * Called when message is received.
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messag es. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: ${remoteMessage.data}")

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
             Log.d(TAG, "Message data payload data: ${remoteMessage.data}")
             Log.d(TAG, "Message data payload noty: ${remoteMessage.notification}")

            var msgType = remoteMessage.data["messageType"]
            if(msgType== messageTypeRequestSent){

                //1. the driver receives request sent by passenger
                requestSentIsReceivedByDriver(remoteMessage)

            }else if(msgType== messageTypeRequestAccepted){

                //2. the passenger receives acceptance from driver
                  requestSentIsAcceptedByDriver(remoteMessage)
            }

        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            Log.d(TAG, "Message Notification Body: ${it.body}")
         }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    // [END receive_message]

           private fun requestSentIsReceivedByDriver(remoteMessage: RemoteMessage){
              //The is a response from passenger to driver

               var tripId = remoteMessage.data["tripId"]
               var userId = remoteMessage.data["userId"]
               var requestId = remoteMessage.data["requestId"]
               var userName = remoteMessage.data["userName"]
               var fare = remoteMessage.data["fare"]

               var vehicleName = remoteMessage.data["vehicleName"]
               var vehicleSeats = remoteMessage.data["vehicleSeats"]

               var originLat = remoteMessage.data["originLat"]
               var originLon = remoteMessage.data["originLon"]
               var destLat = remoteMessage.data["destLat"]
               var destLon = remoteMessage.data["destLon"]


               var locOriginLtdLon = LatLng(originLat!!.toDouble(),originLon!!.toDouble())
               var locDesLtdLon = LatLng(destLat!!.toDouble(),destLon!!.toDouble())

               var locO = MapUtils.convertFromLatLngToLocation(locOriginLtdLon)
               var locD = MapUtils.convertFromLatLngToLocation(locDesLtdLon)

               var locOT = MapUtils.convertFromLocationToTripLocation(locO)
               var locDT = MapUtils.convertFromLocationToTripLocation(locD)

                var request = RequestResponse()
                request.responseType = "requestFromPassenger"
                request.userId = userId
                request.requestId = requestId
                request.tripId = tripId
                request.calculatedFare = fare
                request.userName = userName
                request.tripDriverDestination = locDT
                request.tripDriverOrigin = locOT
                request.vehicleName = vehicleName
                request.vehicleSeats = vehicleSeats

                val intent = Intent().apply {
                   action = INTENT_ACTION_SEND_MESSAGE
                   putExtra("requestResponse", request)
                 }
                 LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
           }

           private fun requestSentIsAcceptedByDriver(remoteMessage: RemoteMessage){
               //This is an accepted response from driver to passenger
               //Received by passenger
               Log.d("FCM","acceptanceFromDriver-ms")

               var tripId = remoteMessage.data["tripId"]
               var userId = remoteMessage.data["userId"]
               var requestId = remoteMessage.data["requestId"]
               var userName = remoteMessage.data["userName"]
               var requestDate = remoteMessage.data["requestDate"]
               var requestStatus = remoteMessage.data["requestStatus"]

               var originLat = remoteMessage.data["originLat"]
               var originLon = remoteMessage.data["originLon"]
               var destLat = remoteMessage.data["destLat"]
               var destLon = remoteMessage.data["destLon"]


             var locOriginLtdLon = LatLng(originLat!!.toDouble(),originLon!!.toDouble())
             var locDesLtdLon = LatLng(destLat!!.toDouble(),destLon!!.toDouble())
             var locO = MapUtils.convertFromLatLngToLocation(locOriginLtdLon)
             var locD = MapUtils.convertFromLatLngToLocation(locDesLtdLon)
             var locOT = MapUtils.convertFromLocationToTripLocation(locO)
             var locDT = MapUtils.convertFromLocationToTripLocation(locD)

             var request = RequestResponse()
               request.responseType = "acceptanceFromDriver"
               request.userId = userId
               request.requestId = requestId
               request.tripId = tripId
               request.userName = userName
               request.tripDriverDestination = locDT
               request.tripDriverOrigin = locOT
               request.requestDate = requestDate
               request.requestStatus = requestStatus

              val intent = Intent().apply {
                  action = INTENT_ACTION_SEND_MESSAGE
                 putExtra("requestResponse", request)
               }
             LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }















    // [START on_new_token]
    /**
     * Called if the FCM registration token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the
     * FCM registration token is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // FCM registration token to your app server.
        sendRegistrationToServer(token)
    }
    // [END on_new_token]

    /**
     * Schedule async work using WorkManager.
     */
    private fun scheduleJob() {
        // [START dispatch_job]
       // val work = OneTimeWorkRequest.Builder(MyWorker::class.java).build()
       // WorkManager.getInstance(this).beginWith(work).enqueue()
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private fun handleNow() {
        Log.d(TAG, "Short lived task is done.")
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String?) {
        // TODO: Implement this method to send token to your app server.
        Log.d(TAG, "sendRegistrationTokenToServer($token)")

        PreferenceHandler(this).putPref(PreferenceHandler.FCM_TOKEN, token)

    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(messageBody: String) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT)

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
              //  .setSmallIcon(R.drawable.ic_stat_ic_notification)
                .setContentTitle(getString(R.string.fcm_message))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

    companion object {

        private const val TAG = "MyFirebaseMsgService"
        const val INTENT_ACTION_SEND_MESSAGE = "INTENT_ACTION_SEND_MESSAGE"

        val messageTypeRequestSent = "RequestSent"
        val messageTypeRequestAccepted = "RequestAccepted"
    }
}