package com.uz.mpalift.mpa_lift_app.ui.userManagement

import com.uz.mpalift.controllers.userManagement.UserProfilePort
import com.uz.mpalift.mpa_lift_app.module.entities.DrivingLicence
import com.uz.mpalift.mpa_lift_app.module.entities.MpaLiftUser
import com.uz.mpalift.mpa_lift_app.module.entities.TripVehicle

class UserProfilePresenter(var userProfileViewModel: UserProfileViewModel) : UserProfilePort {

    //The Presenter is the UX

    override fun presentUserDetails(userName: String, userEmail: String, userPhotoUrl: String) {
        userProfileViewModel.setUserDetails(userName,userEmail,userPhotoUrl)
    }

    override fun showLoadError(error: String) {
       userProfileViewModel.setError(error)
    }

    override fun isShowProgress(isShow:Boolean) {
       userProfileViewModel.setProgress(isShow)
    }


    override fun showDriverDetails(driver: DrivingLicence) {
        userProfileViewModel.mGetDriverDetails.postValue(driver)
    }

    override fun showPassengerDetails(user: MpaLiftUser) {
        userProfileViewModel.mGetPassengerDetails.postValue(user)
    }

    override fun showRegisteredVehicles(vehicles: MutableList<TripVehicle>) {
       userProfileViewModel.mGetRegisteredVehicles.postValue(vehicles)
    }

    override fun showRegisteredVehiclesError(error: String) {
      userProfileViewModel.mGetRegisteredVehiclesEmpty.postValue(error)
    }

    override fun isShowUserDriver(isDriver: Boolean) {
      userProfileViewModel.mIsUserDriver.postValue(isDriver)
    }

    override fun errorOnCheckIsUserDriver(error: String) {
      userProfileViewModel.mIsUserDriverError.postValue(error)
    }


    override fun isShowUserPasswordValid(isValid: Boolean) {
        userProfileViewModel.mIsUserPasswordValid.value = isValid
    }

    override fun errorOnCheckIsUserPasswordValid(error: String) {
      userProfileViewModel.mIsUserPasswordValidError.postValue(error)
    }


    override fun showUserRegistered(isRegisteredMsg: String) {

    }


    override fun showUserRegisteredError(error: String) {

    }


    override fun showUserDriverLicenceRegistered(isRegisteredMsg: String) {
        userProfileViewModel.mIsUserDriverLicenceRegistered.postValue(isRegisteredMsg)
    }

    override fun showUserDriverLicenceRegisteredError(error: String) {
        userProfileViewModel.mIsUserDriverLicenceRegisteredError.postValue(error)
    }


    override fun isUserRegisterForFcm(boolean: Boolean) {
        userProfileViewModel.mIsUserRegisteredForFCM.postValue(boolean)
    }

    override fun showUserRegistrationErrorForFcm(error: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}