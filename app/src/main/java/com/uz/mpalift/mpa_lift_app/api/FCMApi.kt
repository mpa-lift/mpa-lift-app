package com.uz.mpalift.mpa_lift_app.api

import com.google.gson.JsonObject
import com.uzalz.mpa_lift_app.BuildConfig
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.HeaderMap
import retrofit2.http.Headers
import retrofit2.http.POST


interface FCMApi {

    //@Headers( "Authorization: key="+ BuildConfig.FCM_SERVER_KEY,"Content-Type: application/json")

    @Headers( "Authorization: key="+BuildConfig.FCM_SERVER_KEY,"Content-Type: application/json")
    @POST("fcm/send")
    fun sendNotification(@Body jsonObject: JsonObject): Call<JsonObject>


}