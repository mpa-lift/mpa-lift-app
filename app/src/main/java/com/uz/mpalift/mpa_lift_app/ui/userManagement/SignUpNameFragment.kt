package com.uz.mpalift.mpa_lift_app.ui.userManagement

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.uz.mpalift.controllers.userManagement.SignUpController
import com.uz.mpalift.mpa_lift_app.module.DBConfig
import com.uz.mpalift.mpa_lift_app.module.DBConfig.Companion.firebaseStorage
import com.uz.mpalift.mpa_lift_app.ui.home.MainActivity
import com.uz.mpalift.mpa_lift_app.utils.ImageUtils
import com.uz.mpalift.mpa_lift_app.utils.PreferenceHandler
import com.uzalz.mpa_lift_app.R
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.fragment_signup_name.*
import kotlinx.android.synthetic.main.fragment_signup_name.view.*
import java.io.IOException
import java.util.*

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SignUpNameFragment : Fragment() {

    lateinit var signUpViewModel: SignUpViewModel
    lateinit var signController : SignUpController
    lateinit var signUpPresenterI: SignUpPresenterI

    lateinit var progressBarSignUpDriver: ProgressBar


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        var v = inflater.inflate(R.layout.fragment_signup_name, container, false)
        signUpViewModel = ViewModelProviders.of(requireActivity()).get(SignUpViewModel::class.java)

        signUpPresenterI = SignUpPresenterI(signUpViewModel)
        signController = SignUpController(signUpPresenterI)

        progressBarSignUpDriver = v.progressBarSignUpDriver


     return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        signUpUserInput()
        signUp()
        signUpProcessing()

        activity?.toolbar?.setNavigationOnClickListener{
         //   findNavController().navigate(R.id.action_signUpPasswordFragment_to_NameFragment)
        }


    }



    private fun signUpProcessing(){
        showSignUpProgress()
        onRegisterDriver()
    }

    private fun signUpUserInput(){

        nameTextChange()
        cityTextChange()
        observeFields()

        button_upload_profile_image.setOnClickListener{
            openGallery()
        }

    }

    private fun signUp(){

        button_username_next.setOnClickListener {
            if(isCheckInitials()) {
                publishThumbNailAndRegister()
            }else{
                Toast.makeText(requireContext(),"Add Profile Pic",Toast.LENGTH_LONG).show()
            }
        }

    }

    private fun isCheckInitials():Boolean{
      if(posterImageFullPath!=null){
          return true
      }
        return false

    }


    private fun back(){
        //findNavController().navigate(R.id.action_EmailFragment_to_NameFragment)

    }



    //SIGNUP INPUT ////////////////////////////////////////////////////////////////////////
    fun nameTextChange(){

        nameInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                nameInput.error = "Fill in name."
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.toString().trim { it <= ' ' }.isEmpty()) {
                    nameInput.error = "Field cannot be left blank."

                } else {
                    signUpViewModel.setName(charSequence)

                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }

    fun cityTextChange(){

        last_nameInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.toString().trim { it <= ' ' }.isEmpty()) {
                    last_nameInput.error = "Field cannot be left blank."

                } else {
                    signUpViewModel.setCity(charSequence)

                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }


    var myName : String ? = null
    var myCity : String ? = null
    var myEmail: String ? = null
    var myPassword : String ? = null

    private fun observeFields(){
        signUpViewModel.emailM.observe(requireActivity(), Observer { email ->
            myEmail = email.toString()
            //    Toast.makeText(context,email, Toast.LENGTH_LONG).show()

        })
        signUpViewModel.passwordM.observe(requireActivity(), Observer { password ->
            myPassword = password.toString()

        })
        signUpViewModel.nameM.observe(requireActivity(), Observer { name ->
            myName = name.toString()
        })
        signUpViewModel.cityM.observe(requireActivity(), Observer { city ->
            myCity = city.toString()
        })



    }


    private fun openGallery() {
        //val intent = Intent()
        //intent.type = "video/*"
        //intent.action = Intent.ACTION_GET_CONTENT
        // startActivityForResult(Intent.createChooser(intent, "Select Video"),REQUEST_CODE)
        if (Build.VERSION.SDK_INT < 20) {
            // var photoPickerIntent = Intent(Intent.ACTION_PICK);
            // photoPickerIntent.type = "video/*"
            // photoPickerIntent.setType("image/* video/*");
            //val  mimetypes = arrayOf("image/*", "video/*")
            //photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
            //startActivityForResult(photoPickerIntent, REQUEST_CODE)
        } else {
            var photoPickerIntent = Intent(Intent.ACTION_GET_CONTENT)
            photoPickerIntent.type = "image/*";
            startActivityForResult(photoPickerIntent, REQUEST_CODE_IMG)
        }



    }
    private  var posterImageFullPath : Uri ? = null
    private lateinit var posterImageDownloadUrl: String
    //private var con = requireContext()

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_IMG){

            if (data?.data != null) {

                posterImageFullPath = data.data!!
                Log.d("PublishActivity", "Image: "+posterImageFullPath);
                try {

                    var contentResolver = context?.contentResolver

                    var bitmap = MediaStore.Images.Media.getBitmap(contentResolver,posterImageFullPath)

                    var drawable = ImageUtils.makeBitmapCircular(requireContext(),bitmap)

                    //button_upload_profile_image.background = drawCircle(resources.getColor(android.R.color.darker_gray), resources.getColor(android.R.color.holo_red_dark));
                    button_upload_profile_image.setImageDrawable(drawable)



                } catch (e: IOException) {
                    e.printStackTrace();
                }
            }

        }
    }
    // UploadImage method


   fun  drawCircle(backgroundColor:Int, borderColor:Int): GradientDrawable {

       val numbers: IntArray = intArrayOf(10, 20, 30, 40, 50)
       var flosts: FloatArray = floatArrayOf(0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F)

    var shape = GradientDrawable();
    shape.setShape(GradientDrawable.OVAL);
       shape.cornerRadii = flosts
    shape.setColor(backgroundColor);
    shape.setStroke(10, borderColor);
    return shape;
   }



    //SIGNUP //////////////////////////////////////////////////////////////////////////////
    private fun publishThumbNailAndRegister() {


        var storageReference =  firebaseStorage .reference

        //publish assets first then get their url and save the data

        // Code for showing progressDialog while uploading
        var progressDialog : ProgressDialog = ProgressDialog(requireContext())
        progressDialog.setTitle("Registering...")
        progressDialog.show()

        // Defining the child of storageReference
        storageReference  = storageReference.child("images/"+ UUID.randomUUID().toString())


        // Upload from a local file
        // adding listeners on upload
        // or failure of image
        var uploadTask =  storageReference.putFile(posterImageFullPath!!)
        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener {
            // Handle unsuccessful uploads

            // Error, Image not uploaded
            progressDialog.dismiss();
            Toast.makeText(requireContext(), "Failed " + it.message, Toast.LENGTH_SHORT).show();

        }.addOnSuccessListener { taskSnapshot ->
            // taskSnapshot.metadata contains file metadata such as size, content-type, etc.

            // Image uploaded successfully
            // Dismiss dialog
            progressDialog.dismiss();
          //  Toast.makeText(requireContext(), "Image Uploaded!!"+taskSnapshot.uploadSessionUri, Toast.LENGTH_SHORT).show()

            storageReference.downloadUrl.addOnSuccessListener { downloadUri ->

                //add the download url to the driver
              //  posterImageDownloadUrl = downloadUri.toString()
                registerDriver(downloadUri.toString())


            }.addOnFailureListener {
                // Handle any errors
            }



        }.addOnProgressListener {taskSnapshot ->
            var  progress = (100.0 * taskSnapshot.bytesTransferred / taskSnapshot.totalByteCount)
            progressDialog.setMessage("Uploaded $progress%")
        }

    }

    private fun registerDriver(downloadUrl: String){
      //  progressBarSignUpDriver.visibility = View.VISIBLE

        signController.registerDriver(DBConfig.user!!.uid,myName!!,myCity!!,myEmail!!,myPassword!!,downloadUrl)


    }


    //SIGN UP PROCESSING /////////////////////////////////////////////////////////////////
    private fun showSignUpProgress(){

        signUpViewModel.mSignUpIsShowProgressBar.observe(requireActivity(), Observer {makeVisible->

            if(makeVisible){
                progressBarSignUpDriver.visibility = View.VISIBLE

            }else{
                progressBarSignUpDriver.visibility = View.INVISIBLE
            }

        })

    }

    private fun onRegisterDriver(){

        signUpViewModel.mSignUpWelcomeUser.observe(requireActivity(), Observer {welcome->
           openHomeScreen(welcome)
        })

        signUpViewModel.mSignUpError.observe(requireActivity(), Observer {error->
            Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show()

        })

    }

    private fun openHomeScreen(welcome: String){
        Toast.makeText(requireContext(),welcome, Toast.LENGTH_LONG).show()

        var pref = PreferenceHandler(requireContext())
        pref.putBoolPref(PreferenceHandler.KEY_IS_REGISTERED,true)
        //pref.putPref(PreferenceHandler.KEY_USER_SESSION_TYPE,PreferenceHandler.VALUE_USER_SESSION_TYPE_DRIVER)


        val intent = Intent(requireContext(), MainActivity::class.java)
        startActivity(intent)
        requireActivity().finish()

    }







    companion object{

        private const val REQUEST_CODE_IMG = 1



    }





}
