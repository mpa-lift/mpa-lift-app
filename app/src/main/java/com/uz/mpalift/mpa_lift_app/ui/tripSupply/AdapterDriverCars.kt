package com.uz.mpalift.mpa_lift_app.ui.tripSupply

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.uz.mpalift.mpa_lift_app.module.entities.TripVehicle
import com.uzalz.mpa_lift_app.R


class AdapterDriverCars(private var context: Context, private var cars: ArrayList<TripVehicle?>, var driverlistner: DriverCarlistner) : RecyclerView.Adapter<AdapterDriverCars.VideoHolder>() {



     var lastClickedPosition = -1
     var selectedItem = 0

      init {
         // setHasStableIds(true)
      }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoHolder {

        val  view = LayoutInflater.from(parent.context).inflate(R.layout.item_driver_car, parent, false)
        return VideoHolder(
            view
        )
    }


    //2. Our Adapter needs 2 ViewHolders
    class VideoHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        var carNameText: TextView = itemView.findViewById(R.id.item_driver_car_name)
         var carNoPlateText: TextView = itemView.findViewById(R.id.item_driver_car_no_plate)
//        var driverCarSeatsText: TextView = itemView.findViewById(R.id.item_driver_car_seats)
       var driverAvator: ImageView = itemView.findViewById(R.id.item_driver_car_avator)
        val cardCar:MaterialCardView = itemView.findViewById(R.id.cardCar)

        fun bind(driver: TripVehicle, driverlistner: DriverCarlistner) {


        }

    }

    var isAdapterFresh = true

    // // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: VideoHolder, position: Int) {
        val car: TripVehicle? = cars[position]
       // holder.bind(video,driverlistner)

        holder.carNameText.text = car?.vehicleName
        holder.carNoPlateText.text = car?.vehicleNoPlate
        //  holder.driverCarSeatsText.text = driver.carSeats
        // holder.driverAvator.setImageResource(car.carPhoto)

        //This shows default clothing
        holder.cardCar.setCardBackgroundColor(context.resources.getColor(R.color.rowBackgroundShade))
        holder.cardCar.setBackgroundDrawable(context.resources.getDrawable(R.drawable.shape_stroke_grey))
        holder.carNameText.setTextColor(context.resources.getColor(R.color.textColorPrimary))
        holder.carNoPlateText.setTextColor(context.resources.getColor(R.color.textColorSecondary))

        if (selectedItem == position) {
            if(!isAdapterFresh) {
                holder.cardCar.setBackgroundDrawable(context.resources.getDrawable(R.drawable.shape_stroke_color_accent))
               // holder.cardCar.setCardBackgroundColor(context.resources.getColor(R.color.colorPrimary))
               // holder.carNameText.setTextColor(context.resources.getColor(R.color.colorWhite))
               // holder.carNoPlateText.setTextColor(context.resources.getColor(R.color.colorWhite))
            }else{
                isAdapterFresh= false
            }

        }

        holder.cardCar.setOnClickListener {

            val previousItem = selectedItem
            selectedItem = position

            notifyItemChanged(previousItem)
            notifyItemChanged(position)

            driverlistner.onDriverCarClick(holder.cardCar,position,car!!)

        }




    }

    override fun getItemCount(): Int {
        return cars.size
    }



    fun getCar(pos: Int): TripVehicle? {
        return cars[pos]
    }


    fun clear(){

        cars.clear()

    }





    //Clicklistner implementation
    interface DriverCarlistner{

        fun onDriverCarClick(view: MaterialCardView, pos:Int, carSelected: TripVehicle)

    }





}