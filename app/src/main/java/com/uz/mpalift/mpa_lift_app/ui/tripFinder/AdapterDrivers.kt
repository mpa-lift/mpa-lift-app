package com.uz.mpalift.mpa_lift_app.ui.tripFinder

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.avatarfirst.avatargenlib.AvatarConstants
import com.avatarfirst.avatargenlib.AvatarGenerator
import com.google.android.material.button.MaterialButton
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso
import com.uz.mpalift.controllers.tripFinder.TripFinderViewModel
import com.uz.mpalift.mpa_lift_app.module.DBConfig
import com.uz.mpalift.mpa_lift_app.module.tripRequests.TripRequestsRepository
import com.uz.mpalift.mpa_lift_app.utils.AppExecutorsUI
import com.uzalz.mpa_lift_app.R
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


class AdapterDrivers(private var context: Context, private var history: List<TripFinderViewModel>, private var driverlistner: Driverlistner) : RecyclerView.Adapter<AdapterDrivers.VideoHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoHolder {

        val  view = LayoutInflater.from(parent.context).inflate(R.layout.item_request_driver, parent, false)
        return VideoHolder(
            view
        )
    }


    //2. Our Adapter needs 2 ViewHolders
    class VideoHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var driverNameText: TextView = itemView.findViewById(R.id.item_driver_name)
        private var driverCarNameText: TextView = itemView.findViewById(R.id.item_driver_car_name)

        private var driverCarSeatsText: TextView = itemView.findViewById(R.id.item_driver_car_seats)
        private var driverWalletBalanceButton: MaterialButton = itemView.findViewById(R.id.button_wallet)
        private var driverRequestButton: MaterialButton = itemView.findViewById(R.id.button_driver_request)

        private var tripPickupText: TextView = itemView.findViewById(R.id.item_driver_trip_details_pickup)
        private var tripDropoffText: TextView = itemView.findViewById(R.id.item_driver_trip_details_dropoff)

        private var tripDistanceText: TextView = itemView.findViewById(R.id.item_driver_trip_details_distance)
        private var tripAmountText: TextView = itemView.findViewById(R.id.item_driver_trip_details_amount)

        private var driverAvator: ImageView = itemView.findViewById(R.id.item_driver_avator)

        var appExecutors = AppExecutorsUI()

        fun reverseGeocoding(location : Location, textView: TextView) {
            // Although latitude and longitude are useful for calculating distance or displaying a map position,
            // in many cases the address of the location is more useful.
            //but needs to be run in the background
            var runnable = Runnable {

                //Geocode and update on mainthread
                var geocoder = Geocoder(itemView.context, Locale.getDefault())

                var addresses : List<Address> ? = null
                var resultMessage = ""

                try {
                    addresses = geocoder.getFromLocation(
                        location.getLatitude(),
                        location.getLongitude(),
                        // In this sample, get just a single address
                        1)

                    if (addresses == null || addresses.isEmpty()) {
                        if (resultMessage.isEmpty()) {
                           // resultMessage = requireContext().getString(R.string.no_address_found);
                           // Log.e(TAG, resultMessage)
                           // Log.d("HomeFragment",resultMessage)
                        }
                    }else {
                        Log.d("HomeFragment","result :no empty")
                        // If an address is found, read it into resultMessage
                        var address : Address = addresses[0]
                        var addressParts = ArrayList<String>()
                        Log.d("HomeFragment","result: no empty: "+address.getAddressLine(0))

                        // Fetch the address lines using getAddressLine,
                        // join them, and send them to the thread
                        for (i in 0..address.maxAddressLineIndex) {
                            addressParts.add(address.getAddressLine(i))
                            Log.d("HomeFragmentAddress","result :address"+address.getAddressLine(i))
                        }
                      //  Log.d("HomeFragmentAddress","result :address part 1"+addressParts[1])
                        resultMessage = TextUtils.join("\n", addressParts)
                       // resultMessage = addressParts[1].toString()
                    }


                    appExecutors.mainThread().execute {
                     //   Log.d("HomeFragment","result :" +resultMessage)
                        //Toast.makeText(requireContext(), resultMessage, Toast.LENGTH_LONG).show()
                        var split = resultMessage.split(",")

                        var address2 : String ? = null
                        var address3  : String ? = null
                        var address  : String ? = null

                        if(split.size > 2) {
                             address2 = split[1]
                             address3 = split[2]
                             address = "$address2, $address3"
                        }else{
                            address2 = split[0]
                            address3 = split[1]
                            address = "$address2, $address3"
                        }
                        textView.text = address
                    }


                }catch (ioException: IOException) {
                    // Catch network or other I/O problems
                    //resultMessage = requireContext().getString(R.string.service_not_available)
                    //Log.e(TAG, resultMessage, ioException)
                }
                catch (illegalArgumentException:IllegalArgumentException ) {
                    // Catch invalid latitude or longitude values
                    //resultMessage = requireContext().getString(R.string.invalid_lat_long_used)
                  //  Log.e(TAG, resultMessage + ". " + "Latitude = " + location.getLatitude() + ", Longitude = " + location.getLongitude(), illegalArgumentException);
                }

            }
            appExecutors.diskIO().execute(runnable)

        }

        fun checkIfRequestPending(btn:MaterialButton){
            var repo = TripRequestsRepository()
            repo.isRequestPending(DBConfig.user!!.uid, object : TripRequestsRepository.IsRequestPendingCallback{

                override fun isRequestPending(isPending: Boolean) {

                    var pending = "PENDING"

                    if(isPending){
                        btn.isEnabled = false
                        btn.text = pending
                        btn.setBackgroundColor(btn.context.resources.getColor(R.color.grey_pallete_300))
                    }

                }

                override fun onFailed(error: String) {

                }

            })



        }


        fun bind(tripFinderViewModel: TripFinderViewModel, driverlistner: Driverlistner) {


            driverNameText.text = tripFinderViewModel.driverName
            driverCarNameText.text = tripFinderViewModel.vehicleName

            var fare = ""+tripFinderViewModel.tripAmount+"/="
            var distance = ""+tripFinderViewModel.tripDistance+"KM"


            var seats = " | "+tripFinderViewModel.vehicleSeats +"seats| "+distance
            driverCarSeatsText.text = seats
            driverWalletBalanceButton.text = "SORT OUT"
          //  driverAvator.setImageResource(R.drawable.android_plain)

            tripAmountText.text = fare
            tripDistanceText.text = distance


            reverseGeocoding(tripFinderViewModel.tripDriverOrigin!!,tripPickupText)
            reverseGeocoding(tripFinderViewModel.tripDriverDestination!!,tripDropoffText)
            checkIfRequestPending(driverRequestButton)

            val imageUri = tripFinderViewModel.driverPicUrl
            Picasso.with(itemView.context).load(imageUri)
                .fit().centerCrop()
                .placeholder(AvatarGenerator.avatarImage(itemView.context, 100, AvatarConstants.RECTANGLE, tripFinderViewModel.driverName!!))
                //.error(R.drawable.user_placeholder_error)
                .into(driverAvator)

            val position = adapterPosition

            itemView.setOnClickListener {

            }

            driverRequestButton.setOnClickListener{

                driverlistner.onDriverClick(driverRequestButton,position,tripFinderViewModel)

            }



        }




    }


    // // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: VideoHolder, position: Int) {
        val trip: TripFinderViewModel = history[position]
        holder.bind(trip,driverlistner)
    }

    override fun getItemCount(): Int {
        return history.size
    }

    fun getVideo(pos: Int): TripFinderViewModel {
        return history[pos]
    }






    //Clicklistner implementation
    interface Driverlistner{

        fun onDriverClick(view: MaterialButton, pos:Int, driver: TripFinderViewModel)

    }





}