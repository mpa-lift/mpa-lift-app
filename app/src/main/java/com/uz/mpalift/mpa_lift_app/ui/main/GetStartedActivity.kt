package com.uz.mpalift.mpa_lift_app.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import com.firebase.ui.auth.AuthUI
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.uz.mpalift.mpa_lift_app.module.userManagement.UserRepository
import com.uz.mpalift.mpa_lift_app.ui.home.MainActivity
import com.uz.mpalift.mpa_lift_app.ui.userManagement.ChooseRoleActivity
import com.uz.mpalift.mpa_lift_app.ui.userManagement.WelcomeBackActivity
import com.uz.mpalift.mpa_lift_app.utils.MyFonts
import com.uz.mpalift.mpa_lift_app.utils.PreferenceHandler
import com.uzalz.mpa_lift_app.R
import kotlinx.android.synthetic.main.activity_get_started.*
import java.util.concurrent.TimeUnit

class GetStartedActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    private var verificationInProgress = false
    private var storedVerificationId: String? = ""
    private lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks

    private  lateinit var progressBar : ProgressBar


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fullscreen()
        setContentView(R.layout.activity_get_started)
        setUpToolBar()
        //progressBar = findViewById(R.id.progressBarStarted)

        auth = Firebase.auth
        //  auth = FirebaseAuth.getInstance()

        toolbar_started.setNavigationOnClickListener{
            finish()
        }


        //1. Check if person registered
        //2. show ui - continue as driver
        //3.         - show phone
        //           - welcome back -
        //           - show driver UI
        //4. if not registered
                    // - phone
                    // - choose
                    // - signup


        fonts()

        buttonGetStarted.setOnClickListener{
            // val intent = Intent(this, PhoneNumberAuthActivity::class.java)
            // startActivity(intent)

              showProgress(true)
              showUI(true)

        }

        buttonVerifyPhone.setOnClickListener{
           verifyPhoneNumber()
        }

        pin_view.addTextChangedListener(){
            if(pin_view.text.toString().length == 6 ){
                val code = pin_view.text.toString().trim()
                verifyPhoneNumberWithCode(code)
            }

        }
        buttonVerifyCode.setOnClickListener{

            val code = pin_view.text.toString().trim()
            if (code.isEmpty() || code.length < 6) {
                Toast.makeText(this,"Enter valid code", Toast.LENGTH_LONG).show()
            }
            verifyPhoneNumberWithCode(code)

        }



        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                //Log.d(PhoneNumberAuthActivity.TAG, "onVerificationCompleted:$credential")


                showUI(false)


                //sometime the code is not detected automatically
                //in this case the code will be null
                //so user has to manually enter the code
                if (credential.smsCode != null) {
                    val code = credential.smsCode
                    //   editTextCode.setText(code)
                    //verifying the code
                    verifyPhoneNumberWithCode(code!!)
                }

            }

            override fun onVerificationFailed(e: FirebaseException) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                // Log.w(PhoneNumberAuthActivity.TAG, "onVerificationFailed", e)

                Toast.makeText(baseContext, e.message, Toast.LENGTH_SHORT).show()

                if (e is FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // ...
                } else if (e is FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // ...
                }



                // Show a message and update the UI
                // ...
            }

            override fun onCodeSent(verificationId: String, token: PhoneAuthProvider.ForceResendingToken) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                // Log.d(PhoneNumberAuthActivity.TAG, "onCodeSent:$verificationId")
                Toast.makeText(baseContext, verificationId, Toast.LENGTH_SHORT).show()

                // Save verification ID and resending token so we can use them later
                storedVerificationId = verificationId
                resendToken = token

                var msg = "Check SMS and enter the one time we code sent on "+countryCodeHolder.formattedFullNumber
                tv_otp_title.text = msg

                showProgress(false)
                showUI(false)

                // ...
            }
        }


        Log.d("USER_LIFT_TYPE","oncreate")


    }

    private fun setUpToolBar() {

        //    toolbar_main.logo = resources.getDrawable(R.drawable.ic_logo)
        // toolbar_main.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(toolbar_started)
        // supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //  supportActionBar!!.setIcon(R.drawable.ic_logo)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        /*
        val str = SpannableStringBuilder("Vacuum")
        str.setSpan(StyleSpan(Typeface.BOLD), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        toolbar.title = str

        supportActionBar!!.title = str

         */
    }



    fun fonts(){

        logoId.typeface = MyFonts.getBoldTypeFace(this)
        logoSubtitle.typeface = MyFonts.getTypeFace(this)

    }


    var pref = PreferenceHandler(this)




    private fun openSignInWelcome(userId:String){

        var b = Bundle()
        b.putString("userId",userId)
        val intent = Intent(this, WelcomeBackActivity::class.java)
        intent.putExtras(b)
        startActivity(intent)
    }

    private fun openChooseRole(){

        val intent = Intent(this, ChooseRoleActivity::class.java)
        startActivity(intent)
    }



    private fun showProgress(isShowProgress:Boolean){
        if(isShowProgress){
            progressBarStarted.visibility = View.VISIBLE
        }else{
            progressBarStarted.visibility = View.INVISIBLE
        }
    }

    private fun verifyPhoneNumberWithCode(code: String) {
        showProgress(true)
        //called after code sent
        // [START verify_with_code]
        val credential = PhoneAuthProvider.getCredential(storedVerificationId!!, code)
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential)

    }




    private fun verifyPhoneNumber(){
        showProgress(true)
        //attach
        countryCodeHolder.registerCarrierNumberEditText(editText_carrierNumber)

        if(countryCodeHolder.isValidFullNumber) {
            ///////////////TESTING//////////////////////
            ////////////////////////////////////////////
            Toast.makeText(this,countryCodeHolder.formattedFullNumber, Toast.LENGTH_LONG).show()

            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                countryCodeHolder.formattedFullNumber, // Phone number to verify
                60, // Timeout duration
                TimeUnit.SECONDS, // Unit of timeout
                this, // Activity (for callback binding)
                callbacks
            ) // OnVerificationStateChangedCallbacks

        }

    }


    private fun showUI(isShowPhoneView: Boolean){
         showProgress(false)
        if(isShowPhoneView){
            main_layout.visibility = View.INVISIBLE
            layoutPhone.visibility = View.VISIBLE
            layoutCode.visibility = View.INVISIBLE
        }else{
            layoutPhone.visibility = View.INVISIBLE
            layoutCode.visibility = View.VISIBLE
            main_layout.visibility = View.INVISIBLE
        }

    }


    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")
                    showProgress(false)

                    val user = task.result?.user
                  //  Toast.makeText(this,user?.phoneNumber, Toast.LENGTH_LONG).show()

                    if(auth.currentUser!= null) {
                        //check if the user exists as registered
                        //he might be registering

                        var userRepo = UserRepository()
                        userRepo.isUserRegistered(auth.uid!!,object: UserRepository.IsUserRegisteredCallback{

                            override fun isRegistered(boolean: Boolean) {
                                if(boolean){
                                    openSignInWelcome(auth.uid!!)
                                }else{
                                    openChooseRole()
                                }
                            }

                            override fun onError(error: String) {

                            }

                        })



                    }else{
                        val intent = Intent(this, ChooseRoleActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                    // ...
                } else {
                    // Sign in failed, display a message and update the UI
                    showProgress(false)
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        // The verification code entered was invalid
                        Toast.makeText(this,"Invalid code", Toast.LENGTH_LONG).show()
                    }
                }
            }
    }


    private fun signout(){

        Firebase.auth.signOut()
    }

    private fun createSignInIntent() {
        // [START auth_fui_create_intent]
        // Choose authentication providers
        val providers = arrayListOf(
            //AuthUI.IdpConfig.EmailBuilder().build(),
            AuthUI.IdpConfig.PhoneBuilder().build(),
            //  AuthUI.IdpConfig.FacebookBuilder().build(),
            //    AuthUI.IdpConfig.TwitterBuilder().build() ,
            AuthUI.IdpConfig.GoogleBuilder().build())


        // Create and launch sign-in intent
        startActivityForResult(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .setTheme(R.style.FullscreenTheme)
                .build(),
            RC_SIGN_IN
        )

        // [END auth_fui_create_intent]
    }


    companion object {
        private const val TAG = "PhoneAuthActivity"
        private const val RC_SIGN_IN = 123

        private const val KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress"
        private const val STATE_INITIALIZED = 1
        private const val STATE_VERIFY_FAILED = 3
        private const val STATE_VERIFY_SUCCESS = 4
        private const val STATE_CODE_SENT = 2
        private const val STATE_SIGNIN_FAILED = 5
        private const val STATE_SIGNIN_SUCCESS = 6
    }


    private fun fullscreen(){

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }

}


