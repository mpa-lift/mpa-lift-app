package com.uz.mpalift.mpa_lift_app.ui.userManagement

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.uz.mpalift.mpa_lift_app.utils.SecurityUtils.Companion.encryptPassword
import com.uz.mpalift.mpa_lift_app.utils.SecurityUtils.Companion.isEmailValid
import com.uzalz.mpa_lift_app.R
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.fragment_signup_email.*


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class SignupEmailFragment : Fragment() {



    lateinit var signUpViewModel: SignUpViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var v = inflater.inflate(R.layout.fragment_signup_email, container, false)
       signUpViewModel = ViewModelProviders.of(requireActivity()).get(SignUpViewModel::class.java)

        activity?.actionBar?.setDisplayHomeAsUpEnabled(true)

        activity?.toolbar?.setNavigationOnClickListener{
            activity?.finish()
        }




        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        emailTextChange()
        passwordTextChange()
        button_email_next.setOnClickListener {

            next()

        }



    }

    fun next(){
        findNavController().navigate(R.id.action_EmailFragment_to_NameFragment)
    }



    fun emailTextChange(){

        emailInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.toString().trim { it <= ' ' }.isEmpty()) {
                    emailInput.error = "Field cannot be left blank."

                } else if(!isEmailValid(charSequence)){
                    emailInput.error = "Enter valid email address."
                }else {
                    signUpViewModel.setEmail(charSequence)
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }


    fun passwordTextChange(){

        passInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.toString().trim { it <= ' ' }.isEmpty()) {
                    passInput.error = "Field cannot be left blank."

                }else if(charSequence.toString().trim().length < 6){
                    passInput.error = "Password too short."

                } else {

                   var pass= encryptPassword(charSequence.toString())
                    signUpViewModel.setPassword(pass)
                }
            }

            override fun afterTextChanged(editable: Editable) {

            }
        })
    }




}
