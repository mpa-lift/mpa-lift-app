package com.uz.mpalift.mpa_lift_app.utils

import android.content.Context
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.material.bottomsheet.BottomSheetBehavior


class BottomSheetUI(private var context: Context) {


    /////////////////// SETUP UI ELEMENTS //////////////////////////////////////////////////////////////
    private lateinit var sheetBehavior: BottomSheetBehavior<ConstraintLayout>
    private lateinit var layoutBottomSheet: ConstraintLayout

     fun setUpBottomSheetBehavior(layoutBottomSheet: ConstraintLayout){
         this.layoutBottomSheet  = layoutBottomSheet

        // layoutBottomSheet = findViewById(R.id.id_bottom_sheet_dest)
        // Provides callbacks and make the BottomSheet work with CoordinatorLayout
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet)
        // sheetBehavior.isFitToContents = false
        // sheetBehavior.halfExpandedRatio = 0.6f
        // val metrics = resources.displayMetrics
        // sheetBehavior.peekHeight = metrics.heightPixels / 2
        // sheetBehavior.state = BottomSheetBehavior.STATE_HALF_EXPANDED
        // sheetBehavior.peekHeight = 300

        sheetBehavior.setBottomSheetCallback(object: BottomSheetBehavior.BottomSheetCallback(){
            override fun onStateChanged(bottomSheet: View, state: Int) {

                print(state)
                when (state) {

                    BottomSheetBehavior.STATE_HIDDEN -> {
                        //persistentBtn.text = "Show Bottom Sheet"
                        //fab.visibility = View.VISIBLE
                     //   bottomSheet.post {  sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }



                    }
                    BottomSheetBehavior.STATE_EXPANDED ->{
                        //persistentBtn.text = "Close Bottom Sheet"
                        // fab.visibility = View.INVISIBLE
                      //  sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

                    }
                    BottomSheetBehavior.STATE_COLLAPSED ->{
                        //  fab.visibility = View.VISIBLE
                        //if you try to collapse the state it will remain expanded
                     //  sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED


                    }


                    BottomSheetBehavior.STATE_DRAGGING -> {

                    //    bottomSheet.post {  sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED }

                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                        // fab.visibility = View.INVISIBLE
                       // sheetBehavior.setState(BottomSheetBehavior.STATE_HALF_EXPANDED)
                    }
                }
            }
            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }
        })



    }

    fun showBottomSheetPeekHeight(peekHeight : Int){
     sheetBehavior.peekHeight = peekHeight
    }

     fun showBottomSheet(isShowSheet: Boolean, height: Int){
        if(isShowSheet){
          sheetBehavior.peekHeight = height
          layoutBottomSheet.visibility = View.VISIBLE
        }else{
            sheetBehavior.peekHeight = 0
            layoutBottomSheet.visibility = View.GONE
        }
     }

    fun showBottomSheetReceit(isShowSheet: Boolean){
        if(isShowSheet){
            sheetBehavior.peekHeight = 900
            layoutBottomSheet.visibility = View.VISIBLE
        }else{
            sheetBehavior.peekHeight = 0
            layoutBottomSheet.visibility = View.GONE
        }
    }

     fun toggleBottomSheet() {


        if (sheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            //btnBottomSheet.setText("Close sheet");
        } else {
              sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
            // btnBottomSheet.setText("Expand sheet");
        }


    }





    companion object{

        fun searchByfragment(autocompleteFragment: AutocompleteSupportFragment){

            // 1. SEARCH YOUR DESTINATION

            var text = "Enter Destination"
            // autocompleteFragment.setText(text)

            autocompleteFragment.setTypeFilter(TypeFilter.ADDRESS)

            // Specify the types of place data to return.
            autocompleteFragment.setPlaceFields(listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG))

            // Set up a PlaceSelectionListener to handle the response.


        }


    }


}