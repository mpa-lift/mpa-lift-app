package com.uz.mpalift.mpa_lift_app.ui.home

import android.content.Context
import android.content.Intent
import com.uz.mpalift.mpa_lift_app.ui.userManagement.ChooseRoleActivity
import com.uz.mpalift.mpa_lift_app.ui.userManagement.SignUpDriverActivity

class MockActivities {


   companion object{


       fun mockRegisterDriver(c: Context){

           val intent = Intent(c, SignUpDriverActivity::class.java)
           c.startActivity(intent)
       }

       fun mockChooseRole(c: Context){

           val intent = Intent(c, ChooseRoleActivity::class.java)
           c.startActivity(intent)
       }

   }


}