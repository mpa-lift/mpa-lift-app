package com.uz.mpalift.mpa_lift_app.ui.tripFinder

import android.location.Location
import com.google.android.gms.maps.model.LatLng

class DriverViewModel {

    var driverName : String ? = null
    var driverCarName : String ? = null
    var driverCarSeats : Int = 0
    var driverAvator : Int = 0


    var tripDriverOrigin : LatLng ? = null
    var tripDriverDestination : LatLng ? = null
    var tripDriverDropOff : LatLng ? = null

    var tripDistance : String ? = null
    var tripAmount: String ? = null

    var driverWalletAmount : String ? = null



}