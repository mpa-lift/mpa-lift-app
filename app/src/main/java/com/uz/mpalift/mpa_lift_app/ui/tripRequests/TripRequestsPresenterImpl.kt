package com.uz.mpalift.mpa_lift_app.ui.tripRequests

import com.uz.mpalift.controllers.MapUtils
import com.uz.mpalift.controllers.tripRequests.TripRequestsPresenter
import com.uz.mpalift.mpa_lift_app.module.entities.TripLocation
import com.uz.mpalift.mpa_lift_app.module.entities.TripRequest
import com.uz.mpalift.mpa_lift_app.module.entities.TripResponse


class TripRequestsPresenterImpl(var tripRequestsViewModel: TripRequestsViewModel) : TripRequestsPresenter {

    override fun showRequestAccepted(isRequestAccepted: Boolean) {
     tripRequestsViewModel.mIsRequestAccepted.postValue(isRequestAccepted)
    }

    override fun showRequestWaiting(requests: MutableList<TripRequest>) {
        tripRequestsViewModel.mWaitOnRequests.postValue(requests)
    }

    override fun showRequestsWaitingError(error: String) {
        tripRequestsViewModel.mWaitOnRequestsError.postValue(error)
    }

    override fun showTrackingDriverArriving(
        trackArrivalNotice: String,
        trackArrivalTitle: String,
        trackArrivalSubtitle: String,
        isShowNoPlate: Boolean,
        trackArrivalNoPlate: String,
        locationDes: TripLocation) {

        var locationMyDes =
            MapUtils.convertFromTripLocationToLocation(
                locationDes
            )

          var arrivingViewModel = TripRequestsViewModel.DriverArrivingViewModel(
              trackArrivalNotice, trackArrivalTitle,
              trackArrivalSubtitle, isShowNoPlate, trackArrivalNoPlate, locationMyDes
          )
        tripRequestsViewModel.mTrackRequestDriverArriving.postValue(arrivingViewModel)

    }

    override fun showDriverArrivingError(error: String) {
       tripRequestsViewModel.mTrackRequestDriverArrivingError.postValue(error)
    }

    override fun showTrackingDriverComing(driverPicUrl: String, driverName: String, carName: String, carSeats: String) {
        var driverDetails = TripRequestsViewModel.DriverComingViewModel(
            driverPicUrl,
            driverName,
            carName,
            carSeats
        )
        tripRequestsViewModel.mTrackRequestDriverComing.postValue(driverDetails)

    }


    override fun showRequestResponseSentTracking(request: TripRequest) {
        // if accepted setTrackDataFromDriverToPassenger
      tripRequestsViewModel.mTrackRequestResponse.postValue(request)
    }

    override fun showRequestResponseSent(res: TripResponse) {
       tripRequestsViewModel.mSendRequestResponse.postValue(res)
    }

    override fun showRequestSentSeatsAvailiableResponse(msg: String) {
       tripRequestsViewModel.mSendRequestSeatsUnAvailable.postValue(msg)
    }

    override fun showRequestSentError(error: String) {
       tripRequestsViewModel.mSendRequestError.postValue(error)
    }

    /*
    homeViewModel.mIsRequestAccepted.postValue(false)
    homeViewModel.mWaitOnRequests.postValue(requests)
    homeViewModel.mWaitOnRequestsError.postValue(error)
    homeViewModel.mTrackRequestDriverArriving.postValue(arrivingViewModel) ++error
    homeViewModel.mTrackRequestDriverComing.postValue(driverDetails)

    homeViewModel.mTrackRequestResponse.postValue(request)
    homeViewModel.setTrackDataFromDriverToPassenger()
    homeViewModel.mSendRequestResponse.postValue(res)
    homeViewModel.mSendRequestSeatsUnAvailable.postValue(msg)
    homeViewModel.mSendRequestError.postValue(error)

     */




}