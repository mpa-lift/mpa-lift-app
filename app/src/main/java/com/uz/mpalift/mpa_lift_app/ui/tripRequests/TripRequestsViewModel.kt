package com.uz.mpalift.mpa_lift_app.ui.tripRequests

import android.location.Location
import androidx.lifecycle.*
import com.uz.mpalift.mpa_lift_app.module.entities.*

class TripRequestsViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply { value = "This is home Fragment" }
    val text: LiveData<String> = _text


    //REQUEST UI
    val mSendRequestResponse = MutableLiveData<TripResponse>()
    val mSendRequestSeatsUnAvailable = MutableLiveData<String>()
    val mSendRequestError = MutableLiveData<String>()
    val mTrackRequestResponse = MutableLiveData<TripRequest>()
    val mTrackRequestDriverArriving = MutableLiveData<DriverArrivingViewModel>()
    val mTrackRequestDriverArrivingError = MutableLiveData<String>()
    val mTrackRequestDriverComing = MutableLiveData<DriverComingViewModel>()
    val mTrackRequestDriverComingError = MutableLiveData<String>()

    var mWaitOnRequests = MutableLiveData<MutableList<TripRequest>>()
    val mWaitOnRequestsError = MutableLiveData<String>()

    val mIsRequestAccepted = MutableLiveData<Boolean>()
    val mIsRequestAcceptedError = MutableLiveData<Boolean>()


    var mPendingRequest = MutableLiveData<PendingTrip>()
    val mPendingRequestError = MutableLiveData<String>()





    //DRIVER ARRIVING UI
    // These data classes are for organisation of Ui data
    data class DriverArrivingViewModel(val trackArrivalNotice:String,val trackArrivalTitle:String, val trackArrivalSubtitle:String,
                                       val isShowNoPlate:Boolean, val trackArrivalNoPlate:String, val locationDes: Location)

    data class DriverComingViewModel(val imageUrl:String,val driverDisplayName:String, val vehicleDetailsName:String,
                                     val vehicleDetailsSeats:String)



    fun setTrackDataFromDriverToPassenger(){
        /*
        if(mDriverOrigin.value!= null && mPassengerOrigin.value!=null){
            var tracktrip = MapRoute()
            tracktrip.origin = mDriverOrigin.value
            tracktrip.destination =  mPassengerOrigin.value
            Log.d("ORIGNS1", "NOT null: "+mDriverOrigin.value)
            Log.d("ORIGNS2", "NOT null: "+mPassengerOrigin.value)
            driverToPassengerRoute.value = tracktrip
        }else{
            Log.d("ORIGNS1", "null: "+mDriverOrigin.value)
            Log.d("ORIGNS2", "null: "+mPassengerOrigin.value)
        }

         */
    }
   // val driverToPassengerRouteLiveData : LiveData<MapRoute> = driverToPassengerRoute









}