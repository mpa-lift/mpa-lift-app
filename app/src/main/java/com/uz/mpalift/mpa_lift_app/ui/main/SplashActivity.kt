package com.uz.mpalift.mpa_lift_app.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.uz.mpalift.mpa_lift_app.ui.home.MainActivity
import com.uz.mpalift.mpa_lift_app.utils.AppExecutorsUI
import com.uz.mpalift.mpa_lift_app.utils.PreferenceHandler

class SplashActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var pref: PreferenceHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        auth = Firebase.auth
        pref = PreferenceHandler(this)
       // pref.setIntValue(PreferenceHandler.KEY_USER_SESSION,1)


        //This is for refreshing UI
        if (intent.extras != null){
           var s = intent.extras!!.getInt("key_session")
            Log.d("Current_Session", "s: "+s)
            pref.setIntValue(PreferenceHandler.KEY_USER_SESSION,s)
            resume()
         }
    }

    private fun openGetStarted(){

        // var  isDriver =  pref.isUserDriver()
        // var isUserNull = pref.isUserNull()

        MainThreadExecutor(500).execute(Runnable {
            val intent = Intent(this, GetStartedActivity::class.java)
            startActivity(intent)
            finish()
        })

    }



    private fun openHome(){


        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }



    fun stuuf(){
        var  myuser =  pref.getPref(PreferenceHandler.KEY_USER_SESSION)

        var isDriver = PreferenceHandler.VALUE_USER_SESSION_TYPE_DRIVER == myuser
        var isPass = PreferenceHandler.VALUE_USER_SESSION_TYPE_PASSENGER == myuser


        Log.d("USER_LIFT_TYPE","user?"+isDriver)
        if(isDriver){
            pref.putPref(PreferenceHandler.KEY_USER_SESSION, PreferenceHandler.VALUE_USER_SESSION_TYPE_DRIVER)
            Log.d("USER_LIFT_TYPE","type: aa"+isDriver)

        }else if(isPass) {
            pref.putPref(PreferenceHandler.KEY_USER_SESSION, PreferenceHandler.VALUE_USER_SESSION_TYPE_PASSENGER)
        }
    }


    private fun resume(){

        var session = pref.getIntValue(PreferenceHandler.KEY_USER_SESSION)

        if (auth.currentUser != null) {
            //Now check if user data is available
            if(session == 0){//no user
                // Log.d("UserPrefTypeSpl", session)

                openGetStarted()
            }else{

                //  Log.d("UserPrefTypeSpl", ""+session)

                openHome()
            }
        }

    }

    override fun onResume() {
        super.onResume()

        resume()

    }


    override fun onStart() {
        super.onStart()

        if (auth.currentUser != null) {
            //Now check if user is driver
            if (!pref.isUserNull()) {
              //  Log.d("UserPrefTypeSpl","start: "+pref.getPref(PreferenceHandler.KEY_USER_SESSION))

                openHome()
            }else{//logput
                 openGetStarted()
            }

        }else{

            openGetStarted()

        }
    }


}
