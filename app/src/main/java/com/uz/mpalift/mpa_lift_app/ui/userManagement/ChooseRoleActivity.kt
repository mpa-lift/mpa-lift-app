package com.uz.mpalift.mpa_lift_app.ui.userManagement

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.uz.mpalift.mpa_lift_app.utils.PreferenceHandler
import com.uzalz.mpa_lift_app.R
import kotlinx.android.synthetic.main.activity_choose_role.*

class ChooseRoleActivity : AppCompatActivity() {

    var pref = PreferenceHandler(this)
    var isUSerDriverSelected = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_role)


        btnDriverLayout.setBackgroundResource(R.drawable.ic_user_type_driver_selector)
        btnTakeRideLayout.setBackgroundResource(R.drawable.ic_user_type_driver_selector)
        iv_User_type_driver.setImageResource(R.drawable.ic_user_type_driver_icon_selector)
        iv_User_type_passenger.setImageResource(R.drawable.ic_user_type_passenger_icon_selector)

        isSetDriverSelection(true)

        btnDriver.setOnClickListener{
            isSetDriverSelection(true)
        }
        btnTakeRide.setOnClickListener{

            isSetDriverSelection(false)
        }


        buttonSelectUser.setOnClickListener {
            var bundle = Bundle()
            if(isUSerDriverSelected){

                // pref.putPref(PreferenceHandler.KEY_USER_SESSION_TYPE,PreferenceHandler.VALUE_USER_SESSION_TYPE_DRIVER)
                // val intent = Intent(this, MainActivity::class.java)
                //  val intent = Intent(this, SignUpDriverActivity::class.java)
                val intent = Intent(this, SignUpPassengerActivity::class.java)

                bundle.putBoolean("isDriver",true)
                intent.putExtras(bundle)
                startActivity(intent)
                // startActivity(intent)
                // mockRegisterDriver(baseContext)
            }else{
                val intent = Intent(this, SignUpPassengerActivity::class.java)
                bundle.putBoolean("isDriver",false)
                intent.putExtras(bundle)
                startActivity(intent)

            }


        }



    }


    private fun isSetDriverSelection(isDriver: Boolean){

        if(isDriver){

            btnDriverLayout.isSelected = true
            btnTakeRide.isSelected = false
            iv_User_type_passenger.isSelected = false
            iv_User_type_driver.isSelected = true

            isUSerDriverSelected = true

        }else{

            btnTakeRide.isSelected = true
            btnDriverLayout.isSelected = false
            iv_User_type_passenger.isSelected = true
            iv_User_type_driver.isSelected = false

            isUSerDriverSelected = false

        }

    }




}
