package com.uz.mpalift.mpa_lift_app.utils;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.MonthDay;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class DateCalendarConverter {


    //Convert Date to Calendar
    public static GregorianCalendar dateToCalendar(Date date) {

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar;

    }

    //Convert Calendar to Date
    public static Date calendarToDate(GregorianCalendar calendar) {
        return calendar.getTime();
    }




    public static long calculateDifferenceInDates(Date firstDate, Date secondDate){

        long diffDays;

        //in milliseconds
        long diff = firstDate.getTime() - secondDate.getTime();

        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;

        diffDays = diff / (24 * 60 * 60 * 1000);

        return diffDays;


    }





    public static boolean isTimeAutomatic(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.Global.getInt(c.getContentResolver(), Settings.Global.AUTO_TIME, 0) == 1;
        } else {
            return android.provider.Settings.System.getInt(c.getContentResolver(), android.provider.Settings.System.AUTO_TIME, 0) == 1;
        }
    }

    public static boolean isTimeCurrentValid(){

       Date date = DateCalendarConverter.dateWithTimeToDateWithoutTime(new Date());

       Calendar calendar = Calendar.getInstance();
       calendar.set(Calendar.MONTH,0);
       calendar.set(Calendar.YEAR,2015);
       calendar.set(Calendar.DAY_OF_MONTH,1);

       Date date1 = DateCalendarConverter.dateToDateWithoutTimeAndDay(calendar.getTime());

       Log.d("DATECHECK","now date: "+date+" old date: "+date1);

       if(!date.equals(date1)){

           return true;
       }

     return false;
    }







    static  String dateString = null;
    public static String dateToString(Date date) {
       //  SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.US);
       SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.US);
       dateString = dateFormatter.format(date);
       return dateString;
    }



    public static String dateToStringWithoutTime(Date date) {
        String justDateNoTime;

        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMMM d, yyyy", Locale.US);
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

        //SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        justDateNoTime = dateFormatter.format(date);
        return justDateNoTime;
    }


    public static Date stringToDateWithoutTime(String date) {


        // Sat Jul 17 11:09:18 GMT+03:00 2021

        Date date1 = null;
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        //SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        try {
            date1 = dateFormatter.parse(date);
        } catch (ParseException e) {
            Log.d("MpaLiftDate", "convert: e"+e.getMessage());
        }
        return date1;
    }






    public static String dateToStringWithoutTimeAndDay(Date date) {
        String justDateNoTime;

        //  SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.US);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMMM, yyyy", Locale.US);
        justDateNoTime = dateFormatter.format(date);
        return justDateNoTime;
    }

    public static Date dateWithTimeToDateWithoutTime(Date date) {

        return stringToDateWithoutTime(dateToStringWithoutTime(date));
    }


    //yyyy-MM-dd, EEE, dd MMM yyyy HH:mm:ss zzz




    public static Date dateToDateWithoutTimeAndDay(Date date) {
       Date date1 = null;
        String notime = DateCalendarConverter.dateToStringWithoutTimeAndDay(date);
        //  SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.US);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMMM, yyyy", Locale.US);
        try {
            date1 = dateFormatter.parse(notime);
        } catch (ParseException e) {
        }
        return date1;
    }

    public static String dateToStringGetYear(Date date) {
        String justDateNoTime;

        //  SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.US);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy", Locale.US);


        justDateNoTime = dateFormatter.format(date);

        return justDateNoTime;
    }

    public static int dateToIntGetMonth(Date date) {

         Calendar calendar = Calendar.getInstance();
         calendar.setTime(date);
         int month = calendar.get(Calendar.MONTH);

        return month;
    }





}
