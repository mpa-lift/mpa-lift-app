package com.uz.mpalift.mpa_lift_app.ui.main;

import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;

import java.util.concurrent.Executor;

public class MainThreadExecutor implements Executor {

    long time;

    public MainThreadExecutor(long time) {
        this.time = time;
    }

    private Handler mainThreadHandler = new Handler(Looper.getMainLooper());


    @Override
    public void execute(@NonNull Runnable runnable) {

        mainThreadHandler.postDelayed(runnable, time);
    }
}