package com.uz.mpalift.mpa_lift_app.ui.userManagement

import com.uz.mpalift.controllers.userManagement.SignUpPresenter

class SignUpPresenterI(var signUpViewModel: SignUpViewModel) :
    SignUpPresenter {

    //The Presenter is the UX

    override fun showSignUpProgress(isShow: Boolean) {
        signUpViewModel.mSignUpIsShowProgressBar.postValue(isShow)
    }


    override fun showSignUpSuccess(driverName:String) {
            //stop progress
            //record signup
            //welcome user
            var welcome = "Thank you $driverName and welcome"
            signUpViewModel.mSignUpWelcomeUser.postValue(welcome)
    }

    override fun showSignUpError(error:String) {
        signUpViewModel.mSignUpError.postValue(error)

    }


}