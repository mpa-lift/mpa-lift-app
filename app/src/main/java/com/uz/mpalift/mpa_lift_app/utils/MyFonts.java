package com.uz.mpalift.mpa_lift_app.utils;

import android.content.Context;
import android.graphics.Typeface;

public class MyFonts {



    //private static final String workSansRegular = "fonts/workSans/WorkSans-Regular.otf";
   // private static final String workSansMedium = "fonts/workSans/WorkSans-Medium.otf";
   // private static final String workSansSemiBold = "fonts/workSans/WorkSans-SemiBold.otf";
   // private static final String workSansBold = "fonts/workSans/WorkSans-Bold.otf";
    //private static final String tradeGothic = "fonts/TradeGothic/TradeGothicLT.ttf";
    //private static final String thunder = "fonts/Thunder/Thunder.ttf";


    private static final String robotoMedium = "fonts/Roboto/Roboto-Medium.ttf";
    private static final String robotoRegular = "fonts/Roboto/Roboto-Regular.ttf";
    private static final String robotoBold = "fonts/Roboto/Roboto-Bold.ttf";



    public static Typeface getTypeFace(Context context) {

        return Typeface.createFromAsset(context.getAssets(),  robotoRegular);
    }



    public static Typeface getBoldTypeFace(Context context) {

        return Typeface.createFromAsset(context.getAssets(), robotoBold);
    }

    public static Typeface getSemiBoldTypeFace(Context context) {

        return Typeface.createFromAsset(context.getAssets(), robotoMedium);
    }







}
