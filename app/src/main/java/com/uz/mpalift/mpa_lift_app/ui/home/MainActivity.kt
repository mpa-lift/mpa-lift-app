package com.uz.mpalift.mpa_lift_app.ui.home

import android.content.*
import android.graphics.Color
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.findNavController
import androidx.navigation.ui.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.avatarfirst.avatargenlib.AvatarConstants
import com.avatarfirst.avatargenlib.AvatarGenerator
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.android.material.button.MaterialButton
import com.google.android.material.card.MaterialCardView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.google.gson.JsonObject
import com.makeramen.roundedimageview.RoundedTransformationBuilder
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation
import com.uz.mpalift.controllers.*
import com.uz.mpalift.controllers.tripFinder.TripFinderController
import com.uz.mpalift.controllers.userManagement.UserMgtController
import com.uz.mpalift.mpa_lift_app.module.entities.*
import com.uz.mpalift.mpa_lift_app.ui.main.MainThreadExecutor
import com.uz.mpalift.mpa_lift_app.ui.tripSupply.AdapterDriverCars
import com.uz.mpalift.mpa_lift_app.ui.tripSupply.RegisterVehicleActivity
import com.uz.mpalift.mpa_lift_app.ui.tripFinder.AdapterDrivers
import com.uz.mpalift.mpa_lift_app.module.DBConfig
import com.uz.mpalift.mpa_lift_app.module.tripMgt.TripManagementRepository
import com.uz.mpalift.mpa_lift_app.module.tripSupply.TripSupplyRepository
import com.uz.mpalift.controllers.tripFinder.TripFinderViewModel
import com.uz.mpalift.controllers.tripRequests.TripRequestsController
import com.uz.mpalift.mpa_lift_app.api.APIClient
import com.uz.mpalift.mpa_lift_app.api.FCMApi
import com.uz.mpalift.mpa_lift_app.module.billManagement.BillManagementRepository
import com.uz.mpalift.mpa_lift_app.module.walletManagement.ManageWalletRepository
import com.uz.mpalift.mpa_lift_app.ui.home.fcm.MyFirebaseMessagingService
import com.uz.mpalift.mpa_lift_app.ui.home.fcm.MyFirebaseMessagingService.Companion.INTENT_ACTION_SEND_MESSAGE
import com.uz.mpalift.mpa_lift_app.ui.main.SplashActivity
import com.uz.mpalift.mpa_lift_app.ui.tripFinder.TripFinderPresenter
import com.uz.mpalift.mpa_lift_app.ui.tripFinder.TripFinderVModel
import com.uz.mpalift.mpa_lift_app.ui.tripRequests.TripRequestsPresenterImpl
import com.uz.mpalift.mpa_lift_app.ui.tripRequests.TripRequestsViewModel
import com.uz.mpalift.mpa_lift_app.ui.userManagement.UserProfilePresenter
import com.uz.mpalift.mpa_lift_app.ui.userManagement.UserProfileViewModel
import com.uz.mpalift.mpa_lift_app.ui.walletManagement.DepositMoneyActivity
import com.uz.mpalift.mpa_lift_app.utils.*
import com.uzalz.mpa_lift_app.R
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.item_request_driver.*
import kotlinx.android.synthetic.main.nav_header_main.*
import kotlinx.android.synthetic.main.prompt_driver_ui_enter_destination.*
import kotlinx.android.synthetic.main.prompt_driver_ui_manage_trip_content.*
import kotlinx.android.synthetic.main.prompt_driver_ui_receipt.*
import kotlinx.android.synthetic.main.prompt_driver_ui_track_passenger_content.*
import kotlinx.android.synthetic.main.prompt_driver_ui_trip_config_content.*
import kotlinx.android.synthetic.main.prompt_ui_manage_trip_content.*
import kotlinx.android.synthetic.main.prompt_ui_search_dest.*
import kotlinx.android.synthetic.main.prompt_ui_search_drivers.*
import kotlinx.android.synthetic.main.prompt_ui_search_drivers_content.*
import kotlinx.android.synthetic.main.prompt_ui_track_driver_content.*
import kotlinx.android.synthetic.main.sheet_driver_ui.*
import kotlinx.android.synthetic.main.sheet_passenger_ui.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity(), AdapterDrivers.Driverlistner,
    AdapterDriverCars.DriverCarlistner,
    DriverUI {


    private lateinit var mPlacesClient: PlacesClient

    private lateinit var appBarConfiguration: AppBarConfiguration


    //Sheets
    lateinit var sheetDriver: BottomSheetUI
    lateinit var layoutBottomSheetDriver: ConstraintLayout

    lateinit var sheetPassengerMap: BottomSheetUI
    lateinit var layoutBottomSheetPassenger: ConstraintLayout

    lateinit var sheetDriverReceipt: BottomSheetUI
    lateinit var layoutBottomSheetDriverReceipt: ConstraintLayout

    lateinit var sheetPassengerReceipt: BottomSheetUI
    lateinit var layoutBottomSheetPassengerReceipt: ConstraintLayout

    private var heightHigh = 900
    private var heightMedium = 800
    private var heightLow = 300


    // Controllers ////////////////////////////////////////////
    lateinit var tripFinderController: TripFinderController
    lateinit var userMgtController: UserMgtController
    lateinit var tripRequestsController: TripRequestsController


    // ViewModels //////////////////////////////////////////////
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var tripRequestsViewModel: TripRequestsViewModel
    private lateinit var tripFinderVModel: TripFinderVModel
    private lateinit var userProfileViewModel: UserProfileViewModel


    // Presenters //////////////////////////////////////////////
    private lateinit var userProfilePresenter: UserProfilePresenter
    private lateinit var tripRequestsPresenterImpl: TripRequestsPresenterImpl
    private lateinit var tripFinderPresenter: TripFinderPresenter
    private lateinit var driverPresenter: DriverPresenter


    //Databases /////////////////////////////////////////////////
    private var pref = PreferenceHandler(this)
    private var mAuth: FirebaseAuth? = null


    //Notifications ////////////////////////////////////////////////
    var receiver: BroadcastReceiver? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        mAuth = FirebaseAuth.getInstance()

        Log.d("MyID"," is null"+mAuth?.uid)




        // val viewModel = ViewModelProvider(this).get(YourViewModel::class.java)
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        // userProfileViewModel = ViewModelProviders.of(this).get(UserProfileViewModel::class.java)


        // [REQUESTS] ////////////////////////////////////////////////////////
        tripRequestsViewModel = ViewModelProvider(this).get(TripRequestsViewModel::class.java)
        tripRequestsPresenterImpl = TripRequestsPresenterImpl(tripRequestsViewModel)
        tripRequestsController = TripRequestsController(tripRequestsPresenterImpl)


        // [FINDER] ///////////////////////////////////////////////////////////
        tripFinderVModel = ViewModelProvider(this).get(TripFinderVModel::class.java)
        tripFinderPresenter = TripFinderPresenter(tripFinderVModel)
        tripFinderController = TripFinderController(tripFinderPresenter)


        //[USER PROFILE] ///////////////////////////////////////////////////////
        userProfileViewModel = ViewModelProviders.of(this).get(UserProfileViewModel::class.java)
        userProfilePresenter = UserProfilePresenter(userProfileViewModel)
        userMgtController = UserMgtController(userProfilePresenter)


        //passengerUiPresenter = PassengerPresenter(this)
        driverPresenter = DriverPresenter(this)


        // [UI SETUP] /////////////////////////////////////////
        DriverUISetup.setUpRecyclerView(this, recycler_driver_mycars)
        setUpNavigationDrawer()
        fabGOOnline.setOnClickListener {
            sheetDriver.showBottomSheet(true, heightMedium)
            configDriverTrip()
        }
        showUserUI()

        // Initialize the Places client
        initPlacesClient()

        // [NOTIFICATIONS requests] /////////////////////////////////////////
        registerForFCM()

        receiver = object : BroadcastReceiver() {

            override fun onReceive(p0: Context?, p1: Intent?) {

                val requestResponse = p1!!.getParcelableExtra<RequestResponse>("requestResponse")

                // showDialogUI(request!!.userName!!, request.userId!!)

                if(requestResponse?.responseType == "requestFromPassenger"){

                    val tripRequest = TripRequest()
                    tripRequest.requestId = requestResponse.requestId
                    tripRequest.requestDate = requestResponse.requestDate
                    tripRequest.passengerName = requestResponse.userName
                    tripRequest.passengerId = requestResponse.userId
                    tripRequest.tripId = requestResponse.tripId
                    tripRequest.requestStatus = requestResponse.requestStatus

                    tripRequest.origin = requestResponse.tripDriverOrigin
                    tripRequest.destination= requestResponse.tripDriverDestination
                    tripRequest.calculatedFare = requestResponse.calculatedFare

                    showDialogUIRequestReceivedByDriver(tripRequest)

                }else if(requestResponse?.responseType == "acceptanceFromDriver"){ //request was accepted //acceptanceFromDriver
                    //This is for the passenger1
                    //Activate ui to wait for driver
                    Log.d("FCM","acceptanceFromDriver-activity")

                    val tripRequest = TripRequest()
                    tripRequest.requestId = requestResponse.requestId
                    tripRequest.requestDate = requestResponse.requestDate
                    tripRequest.passengerName = requestResponse.userName
                    tripRequest.passengerId = requestResponse.userId
                    tripRequest.tripId = requestResponse.tripId
                    tripRequest.requestStatus = requestResponse.requestStatus
                    tripRequest.calculatedFare = requestResponse.calculatedFare

                    tripRequest.origin = requestResponse.tripDriverOrigin
                    tripRequest.destination= requestResponse.tripDriverDestination

                    MainThreadExecutor(300).execute(Runnable {
                        showUserUIifRequestIsAcceptedFCM(tripRequest)
                    })

                }

            }

        }

        val filter = IntentFilter(INTENT_ACTION_SEND_MESSAGE)
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver!!, filter)

       // manageWalletRepository.removeWalletDatabase()

    }



    lateinit var navView: NavigationView
    fun setUpNavigationDrawer() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        appBarConfiguration = AppBarConfiguration(
            setOf(R.id.nav_home, R.id.nav_wallet, R.id.nav_history), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        //  FirebaseAuth.getInstance().signOut()
        //  pref.putPref(PreferenceHandler.KEY_USER_SESSION_TYPE,null)

        navController.addOnDestinationChangedListener { _, destination, _ ->

            if(destination.id == R.id.nav_home) {
               // toolbar.visibility = View.GONE

            } else if (destination.id == R.id.nav_wallet){
               // toolbar.visibility = View.VISIBLE

                openDepositScreen()


            }
        }







    }



    private fun showDialogUIRequestReceivedByDriver(request: TripRequest) {


     val alertDialog =  MaterialAlertDialogBuilder(this)
            .setTitle(request.passengerName)
            .setMessage(request.requestId)
            .setPositiveButton("Accept!",
                DialogInterface.OnClickListener {dialog, which ->
                  //  Toast.makeText(baseContext, "Acceptance Sent", Toast.LENGTH_LONG).show()
                    sendRequestAcceptance(request)
                    dialog.dismiss()
                    dialog.cancel()

                })

        MainThreadExecutor(1000).execute(Runnable {

            if (!this.isFinishing) {
                alertDialog.show();
            }
        })


           // .show()

        /*
       .setNegativeButton("Cancel",DialogInterface.OnClickListener { dialog, which -> finish() })
        */
    }

    private fun showDialogUI(title: String, msg: String) {
        MaterialAlertDialogBuilder(this)
            .setTitle(title)
            .setMessage(msg)
            .setPositiveButton("Finish",
                DialogInterface.OnClickListener { dialog, which ->

                })
            .show()
        /*
       .setNegativeButton("Cancel",DialogInterface.OnClickListener { dialog, which -> finish() })
        */
    }



    //////////////////// FCM //////////////////////////////////////////////////////////////////
    // This is like the server
    var api = APIClient().getRetrofitInstance()!!.create(FCMApi::class.java)

    private fun sendRequestNotification(tripModel: TripFinderViewModel) {

        //GET THE DRIVER DETAILS - TOKEN MAINLY
        userMgtController.getPassengerDetails(tripModel.driverId!!)
        userProfileViewModel.mGetPassengerDetails.observe(this, Observer { mydriver ->

            // var tripRequest = TripRequest()
            // tripRequest.tripId = driver.tripId
            // tripRequest.passengerId = user.passengerId //driver.driverId
            // tripRequest.passengerName = user.passengerFirstName

            /*
            var locO = MapUtils.convertFromLocationToTripLocation(driver.tripDriverOrigin!!)
            var locD = MapUtils.convertFromLocationToTripLocation(driver.tripDriverDestination!!)
            tripRequest.destination = locD
            tripRequest.origin = locO
             */

            var payload = JsonObject()
            payload.addProperty("to", mydriver.fcmToken) //TO DRIVER PHONE
            //payload.addProperty("to", pref.getPref(PreferenceHandler.FCM_TOKEN))

            var originLat = tripModel.tripDriverOrigin?.latitude
            var originLon = tripModel.tripDriverOrigin?.longitude
            var destLat = tripModel.tripDriverDestination?.latitude
            var destLon = tripModel.tripDriverDestination?.longitude

            //FARE
            var pickOrigin = LatLng(originLat!!, originLon!!)
            var pickDest = LatLng(destLat!!, destLon!!)
            var fare = MapUtils.findFareByDistance(pickOrigin,pickDest)

            var request = JsonObject()
            request.addProperty("messageType", MyFirebaseMessagingService.messageTypeRequestSent)

            var requestId = UUID.randomUUID().toString()


            val passengerId = mAuth?.uid
            val passengerName = tv_profile_username.text.toString()


            request.addProperty("tripId", tripModel.tripId)
            request.addProperty("userId", passengerId)
            request.addProperty("userName", passengerName)
            request.addProperty("fare", fare)

            request.addProperty("requestId", requestId)
            request.addProperty("vehicleName", tripModel.vehicleName)
            request.addProperty("vehicleSeats", tripModel.vehicleSeats)



            request.addProperty("originLat", originLat)
            request.addProperty("originLon", originLon)
            request.addProperty("destLat", destLat)
            request.addProperty("destLon", destLon)


            payload.add("data", request)

            api.sendNotification(payload).enqueue(object : Callback<JsonObject> {

                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                    if (response.isSuccessful) {

                        var tripResponse = TripResponse()
                        tripResponse.vehicleName =  tripModel.vehicleName
                        tripResponse.driverName =   tripModel.driverName
                        tripResponse.driverOrigin = MapUtils.convertFromLocationToLatLng(tripModel.tripDriverOrigin!!)

                        tripResponse.responseMessage
                        tripResponse.tripId = tripModel.driverId
                        tripResponse.requestId = requestId

                        tripRequestsController.presentRequestSent(tripResponse)
                        requestsViewShowRequestSent()//this observes first

                        Toast.makeText(baseContext, "Notification send successful" + response.body(), Toast.LENGTH_LONG).show();
                    }
                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                    Toast.makeText(
                        baseContext,
                        "Notification send unsuccessful" + t.message,
                        Toast.LENGTH_LONG
                    ).show()
                }


            })

        })

    }

    var billMgt = BillManagementRepository()

    var tempBill : String ? = null
    fun sendRequestAcceptance(tripRequest: TripRequest) {
       // var api = APIClient().getRetrofitInstance()!!.create(FCMApi::class.java)

        Log.d("MainActivityReq","request owner: "+tripRequest.passengerId)

        userMgtController.getPassengerDetails(tripRequest.passengerId!!)
        userProfileViewModel.mGetPassengerDetails.observe(this, Observer { user ->

            Log.d("MainActivityReq","user: "+user.fcmToken)

            var payload = JsonObject()
            payload.addProperty("to", user.fcmToken)
            // payload.addProperty("to", pref.getPref(PreferenceHandler.FCM_TOKEN))

            var originLat = tripRequest.origin?.latitude
            var originLon =  tripRequest.origin?.longitude
            var destLat =  tripRequest.destination?.latitude
            var destLon = tripRequest.destination?.longitude

            var request = JsonObject()
            request.addProperty("messageType", MyFirebaseMessagingService.messageTypeRequestAccepted)

            // var requestId = UUID.randomUUID().toString()

            request.addProperty("tripId",tripRequest.tripId)
            request.addProperty("userId", user.passengerId)
            request.addProperty("requestId", tripRequest.requestId)
            request.addProperty("userName", user.passengerFirstName)
            request.addProperty("requestDate", tripRequest.requestDate)
            request.addProperty("requestStatus", tripRequest.requestStatus)

            request.addProperty("originLat", originLat)
            request.addProperty("originLon", originLon)
            request.addProperty("destLat", destLat)
            request.addProperty("destLon", destLon)

            payload.add("data", request)

            api.sendNotification(payload).enqueue(object : Callback<JsonObject> {

                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {

                    if (response.isSuccessful) {

                        Toast.makeText(baseContext, "driver accepts", Toast.LENGTH_LONG).show()
                        showResponseToRequestFCM(tripRequest, true)

                        //2. SUBMIT THE BILL TO THE

                        var pickOrigin = LatLng(originLat!!, originLon!!)
                        var pickDest = LatLng(destLat!!, destLon!!)

                        // var fare = MapUtils.findFareByDistance(pickOrigin,pickDest)
                        var dist = MapUtils.findDistance(pickOrigin, pickDest)


                        var bill = Bill()
                        bill.distance = dist.toString()
                        bill.fare = tripRequest.calculatedFare // from passenger //
                        bill.tripId = tripRequest.tripId
                        bill.userId = tripRequest.passengerId
                        bill.userName = tripRequest.passengerName

                        Log.d("BillMgt",""+bill.userId+":"+tripRequest.passengerId)


                        //To prevent resubmitting

                            billMgt.submitBill(bill,
                                object : BillManagementRepository.SubmitBillCallback {

                                    override fun onBillSubmitted() {

                                       // tempBill = bill.fare

                                        Toast.makeText(baseContext,
                                            "bill submitted: " + tempBill,
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }
                                    override fun onFail(error: String) {}

                                })


                        // tripRequestsController.presentRequestSent(tripResponse)
                        // requestsViewShowRequestSent() // This observes first
                        // Toast.makeText(baseContext, "Notification send successful" + response.body(), Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(baseContext,"code: "+response.code(),Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {

                    Toast.makeText(
                        baseContext,
                        "Notification send unsuccessful" + t.message,
                        Toast.LENGTH_LONG
                    ).show()
                }


            })

        })

    }



    fun registerForFCM(){
        // Log.d(TAG, "Subscribing to weather topic")
        // [START subscribe_topics]
        /*
        Firebase.messaging.subscribeToTopic("requests")
            .addOnCompleteListener { task ->
                var msg = getString(R.string.msg_subscribed)
                if (!task.isSuccessful) {
                    msg = getString(R.string.msg_subscribe_failed)
                }
              //  Log.d(TAG, msg)
                Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
            }
         */
        // [END subscribe_topics]


        // Get token
        // [START log_reg_token]
        Firebase.messaging.token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
               // Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result


            // send the token to register it as on for the user, update the user
            userMgtController.registerUserForFCM(mAuth?.uid!!,token)
            userProfileViewModel.mIsUserRegisteredForFCM.observe(this,Observer{isUser->

                var userMsg = "User activated for fcm"

                Toast.makeText(baseContext, userMsg, Toast.LENGTH_SHORT).show()

               // var model = TripFinderViewModel()

              //  sendRequestNotification(model)



            })


            // Log and toast
            val msg = getString(R.string.msg_token_fmt, token)
          //  Log.d(TAG, msg)
            Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
        })
        // [END log_reg_token]


    }

    ////////////////////  FCM  /////////////////////////////////////////////////////////////////////

    private fun showUserUI(){
        var isDriver = pref.getIntValue(PreferenceHandler.KEY_USER_SESSION)

        if(isDriver == 2){  // Driver
            // showUserUI(true)
            displayProfileDetailsForDriver()
            initDriverBottomSheet()
            initDriverReceiptBottomSheet()
            //sheetDriverReceipt.toggleBottomSheet()
            observeDriverStartedTrip()

        }else{ //Passsenger
            // showUserUI(false)
            fabGOOnline.visibility = View.GONE
            displayProfileDetailsForPassenger()
            initPassengerBottomSheet()
            sheetPassengerMap.showBottomSheet(true,heightMedium)
            searchView()
            //sheetPassengerMap.toggleBottomSheet()
            // sheetPassengerMap.showBottomSheetPeekHeight(150)
            initPassengerReceiptBottomSheet()
            sheetPassengerReceipt.showBottomSheet(false,heightHigh)
            searchViewSelectPlace()
            setUpRecyclerView()

            //nav_header.background = (resources.getDrawable(R.drawable.side_nav_bar_accent))
            var header = navView.getHeaderView(0)
            header.background = resources.getDrawable(R.drawable.side_nav_bar_accent)
        }
    }

    private fun showUserUIifRequestIsAcceptedFCM(request: TripRequest){
        Log.d("FCM","acceptanceFromDriver-activity-ui")

       // Toast.makeText(baseContext,"SHOW USER UI TRACK DRIVER", Toast.LENGTH_LONG).show()

        pref.putBoolPref(PreferenceHandler.KEY_MONITOR_REQUEST,false)

        //show trip route
        homeViewModel.setTrackDataFromDriverToPassenger()

        prompt_drivers.visibility = View.GONE
        prompt_search.visibility = View.GONE
        // prompt_dest.visibility = View.GONE
        prompt_trip.visibility = View.GONE
        prompt_track.visibility = View.VISIBLE


        //Monitor the OnTrip in case the driver has set it
        var myLocation = Location(LocationManager.GPS_PROVIDER)
        myLocation.latitude = request.origin!!.latitude!!
        myLocation.longitude = request.origin!!.longitude!!


        tripRequestsController.getRequestedDriverComingDetails(request.tripId!!)
        tripRequestsViewModel.mTrackRequestDriverComing.observe(this,Observer{driverComingViewModel->

            //1. display profile photo
            val imageUri = driverComingViewModel.imageUrl
            Picasso.with(baseContext).load(imageUri)
                .fit().centerCrop()
                .placeholder(AvatarGenerator.avatarImage(baseContext, 100, AvatarConstants.RECTANGLE, driverComingViewModel.driverDisplayName))
                //.error(R.drawable.user_placeholder_error)
                .into(iv_track_driver_avator)

            //2. display name
            tv_passenger_track_driver_name.text = driverComingViewModel.driverDisplayName
            tv_passenger_track_driver_car_name.text = driverComingViewModel.vehicleDetailsName
            tv_passenger_track_driver_car_seats.text = driverComingViewModel.vehicleDetailsSeats

            requestsViewMonitorDriverArriving(request.tripId!!,myLocation)


        })


    }








    private fun initPlacesClient(){

        var api_key = "AIzaSyD4oF5o4pbYXYpMsidFLYdC75fCI4lu64w"
        // Initialize the Places client
        val apiKey = getString(R.string.maps_api_key)
        Places.initialize(applicationContext, apiKey)
        mPlacesClient = Places.createClient(this)

    }




    private fun displayProfileDetailsForDriver(){

        userMgtController.getUserProfile(mAuth?.uid!!)
        userProfileViewModel.liveDataUserDetails.observe(this,Observer{userDetails->


            val transformation: Transformation = RoundedTransformationBuilder()
                .borderColor(Color.BLACK)
                .borderWidthDp(0.5f)
                .cornerRadiusDp(5f)
                .oval(false)
                .build()

            Picasso.with(baseContext).load(userDetails.userPhotoUrl)
                .fit().centerCrop()
                .transform(transformation)
                .placeholder(AvatarGenerator.avatarImage(baseContext, 100, AvatarConstants.CIRCLE, userDetails.userName))
                //.error(R.drawable.user_placeholder_error)
                .into(imageViewProfile)

            tv_profile_email.text = userDetails.userEmail
            tv_profile_username.text = userDetails.userName

        })

    }

    private fun displayProfileDetailsForPassenger(){

        userMgtController.getUserProfile(mAuth?.uid!!)
        userProfileViewModel.liveDataUserDetails.observe(this,Observer{userDetails->

            /*
            val transformation: Transformation = RoundedTransformationBuilder()
                .borderColor(Color.BLACK)
                .borderWidthDp(0.5f)
                .cornerRadiusDp(18f)
                .oval(true)
                .build()
             */
              //  .transform(transformation)

            Picasso.with(baseContext).load(userDetails.userPhotoUrl)
                .fit().centerCrop()
                .placeholder(AvatarGenerator.avatarImage(baseContext, 100, AvatarConstants.CIRCLE, userDetails.userName))
                //.error(R.drawable.user_placeholder_error)
                .into(imageViewProfile)

            tv_profile_email.text = userDetails.userEmail
            tv_profile_username.text = userDetails.userName

        })

    }





    ///////////// BOTTOMSHEET /////////////////////////////////////////////////////////////////////////


    private fun initDriverBottomSheet(){
        sheetDriver = BottomSheetUI(this)
        layoutBottomSheetDriver = findViewById<ConstraintLayout>(R.id.id_bottom_sheet_driver)

        sheetDriver.setUpBottomSheetBehavior(layoutBottomSheetDriver)
    }

    fun initDriverReceiptBottomSheet(){
        sheetDriverReceipt = BottomSheetUI(this)
        layoutBottomSheetDriverReceipt = findViewById<ConstraintLayout>(R.id.id_bottom_sheet_driver_receipt)
        sheetDriverReceipt.setUpBottomSheetBehavior(layoutBottomSheetDriverReceipt)
    }

    fun initPassengerBottomSheet(){
      //  sheetDriver = BottomSheetUI(this)
       layoutBottomSheetPassenger = findViewById<ConstraintLayout>(R.id.id_bottom_sheet_dest)
        sheetPassengerMap = BottomSheetUI(this)
        sheetPassengerMap.setUpBottomSheetBehavior(layoutBottomSheetPassenger)
    }

    fun initPassengerReceiptBottomSheet(){
        //  sheetDriver = BottomSheetUI(this)
        layoutBottomSheetPassengerReceipt = findViewById<ConstraintLayout>(R.id.id_bottom_sheet_passenger_receipt)
        sheetPassengerReceipt = BottomSheetUI(this)
        sheetPassengerReceipt.setUpBottomSheetBehavior(layoutBottomSheetPassengerReceipt)
    }




    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        //   val navController = findNavController(R.id.nav_host_fragment)

        val navController = findNavController(R.id.nav_host_fragment)

        when (item.itemId) {

            R.id.action_sign_out-> {
                //  val intent = Intent(this, CarProfileActivity::class.java)
                // startActivity(intent)
                // finish()
                //  Toast.makeText(baseContext,"Toasted",Toast.LENGTH_LONG).show()

                // Firebase.auth.signOut()
                mAuth!!.signOut()

                pref.setIntValue(PreferenceHandler.KEY_USER_SESSION,0)
                pref.clear()

                //  Log.d("MyStuffPref",""+pref.getPref(PreferenceHandler.KEY_USER_SESSION_TYPE) )

                val intent = Intent(this, SplashActivity::class.java)
                startActivity(intent)
                finish()

                return true

            } // Do Activity menu item stuff here

            else -> {
            }

        }

        return false
        //   return item.onNavDestinationSelected(navController) || super.onOptionsItemSelected(item)

    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    var isReceiverRegistered = false

    override fun onStart() {
        super.onStart()

        val filter = IntentFilter(INTENT_ACTION_SEND_MESSAGE)
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver!!,filter)
        isReceiverRegistered = true

        showUserUI()
        reloadShowRequestIUserDriver()

         // passengers
         // pref.putPref(PreferenceHandler.KEY_MONITOR_REQUEST_TRIP_ID,"71f804e3-b8c7-4123-a8f9-d080ef36aee0")
         // pref.putBoolPref(PreferenceHandler.KEY_MONITOR_REQUEST,true)

        val isMonitored = pref.getBoolPref(PreferenceHandler.KEY_MONITOR_REQUEST)
        // Log.d("MainActivityMonitor","isMonitor: "+isMonitored)
        if(isMonitored){
            //I want to execute monitoring when user comsback onResume
            val monitoredTripId = pref.getPref(PreferenceHandler.KEY_MONITOR_REQUEST_TRIP_ID)
            requestsViewMonitorRequestShowDriverComing(monitoredTripId!!)
        }


    }

    override fun onPause() {
        super.onPause()
        if (receiver != null) {
          //  unregisterReceiver(receiver)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (receiver!= null && isReceiverRegistered){
           // unregisterReceiver(receiver)
           }
    }



    override fun onResume() {
        super.onResume()
        Log.d("UserCheckResume","OnCheck")

        //pref.setIntValue(PreferenceHandler.KEY_USER_SESSION,1)

       // pref.setIntValue(PreferenceHandler.KEY_USER_SESSION,1)

        val filter = IntentFilter(INTENT_ACTION_SEND_MESSAGE)
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver!!,filter)


        showUserUI()
        // pref.putPref(PreferenceHandler.KEY_TRIP_ID,"f12d33ee-74e3-42b9-842c-1baa502cde97")
        reloadShowRequestIUserDriver()

        // passengers
        // pref.putPref(PreferenceHandler.KEY_MONITOR_REQUEST_TRIP_ID,"71f804e3-b8c7-4123-a8f9-d080ef36aee0")
        // pref.putBoolPref(PreferenceHandler.KEY_MONITOR_REQUEST,true)


        val isMonitored = pref.getBoolPref(PreferenceHandler.KEY_MONITOR_REQUEST)
        // Log.d("MainActivityMonitor","isMonitor: "+isMonitored)
        if(isMonitored){
            //I want to execute monitoring when user come back onResume
            val monitoredTripId = pref.getPref(PreferenceHandler.KEY_MONITOR_REQUEST_TRIP_ID)
            requestsViewMonitorRequestShowDriverComing(monitoredTripId!!)
        }

        val isReceiptSubmitted = pref.getBoolPref("isSubmitted")
        if(isReceiptSubmitted){
         //   refreshUI()
        }



    }

    private fun reloadShowRequestIUserDriver(){

        var run = Runnable {
            val childEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {

                    // onChildAdded() will be called for each node at the first time
                    if (snapshot.value != null){ // Driver

                        var tripID = pref.getPref(PreferenceHandler.KEY_TRIP_ID)
                        var isDriver = pref.isUserDriver()

                        if(isDriver) {

                            /* was used with ending requests
                            TripSupplyRepository().getPendingTrip(mAuth?.uid!!,
                                object : TripSupplyRepository.GetPendingTripCallback {

                                    override fun onGetPendingTrip(pendingTrip: PendingTrip) {
                                        Log.d("MainActivity-Trips", "ID: " + tripID)
                                        showPendingRequest(pendingTrip.tripId!!)
                                    }
                                    override fun onFailed(error: String) {

                                        Toast.makeText(baseContext, error, Toast.LENGTH_LONG).show()
                                    }
                                })

                             */
                        }

                       /*
                        var isDriver = pref.isUserDriver()

                        if(tripID!=null && isDriver){
                            Log.d("MainActivity-Trips","ID: "+tripID)

                            showPendingRequest(tripID)
                        }

                        */

                    }else {  // Passenger

                        tripRequestsController.monitorRequestAcceptance("")
                        ///trackRequests("ed3a7c9f-5dd9-430b-be75-3781b8e1bfc2")
                    }
                }
                override fun onCancelled(error: DatabaseError) {
                    Log.d("DRIVER-GetStartedl", "on canceled:"+error.message)

                }
            }
            // databaseReference.child(DBConfig.refRequests).equalTo(tripId).addListenerForSingleValueEvent(childEventListener)
            DBConfig.reference.child(DBConfig.usersDb).child(mAuth?.uid!!).addListenerForSingleValueEvent(childEventListener)
        }
        AppExecutorsUI().diskIO().execute(run)



    }





    // HOME SCREEN - SEARCH VIEWS /////////////////////////////////////////////////////////////////

    private fun searchView() {
        prompt_search.visibility = View.VISIBLE
        prompt_drivers.visibility = View.GONE
        prompt_track.visibility = View.GONE
        prompt_trip.visibility = View.GONE
        // prompt_dest.visibility = View.GONE

        // Components
        prompt_text_search.visibility = View.VISIBLE
        autocomplete_fragment.requireView().visibility = View.VISIBLE
        button_confirm_dest.visibility = View.GONE
        prompt_text_confirm.visibility = View.GONE
        prompt_text_process_request.visibility = View.GONE

    }

    private fun searchViewSelectPlace(){

       // Places.initialize(getApplicationContext(),resources.getString(R.string.apikey));

        var autocompleteFragment : AutocompleteSupportFragment =
            // Initialize the AutocompleteSupportFragment.
            supportFragmentManager.findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment
        // 1. SEARCH YOUR DESTINATION
        var text = "Enter Destination"
        // autocompleteFragment.setText(text)
        autocompleteFragment.setTypeFilter(TypeFilter.ADDRESS)

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG))


        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {

                   // Log.d("MainActivity-MA", "Observe-Location-dest: $place.latLng")
                   // Toast.makeText(baseContext,"OnConfirm: ${place.address}",Toast.LENGTH_LONG).show()
                    homeViewModel.mPassengerDestination.value = place.latLng

                    // The passenger trip route is made after the destination is observed in the HomeFragment of the  map
                    // Then we display to confirm destination

                    searchViewConfirmPlace()

                //If you change the destination the it also changes in the HomeFragment
            }
            override fun onError(status: Status) {
                // TODO: Handle the error.
                // Log.d("PLACES_ACTIVITY", "An error occurred: $status")
                Toast.makeText(baseContext,"OnSelected: "+status.statusMessage,Toast.LENGTH_LONG).show()
            }
        })

    }


    private fun searchViewConfirmPlace(){

        // destination is still on the search prompt
        //Components
        //  sheetPassengerMap.toggleBottomSheet()

        MainThreadExecutor(500).execute(Runnable {
            prompt_text_search.visibility = View.GONE
            prompt_text_confirm.visibility = View.VISIBLE
            button_confirm_dest.visibility = View.VISIBLE
            prompt_text_process_request.visibility = View.GONE
        })


        // 2. AT THIS TIME THE ORIGIN AND DESTINATION HAS BEEN SET
        var origin = homeViewModel.mPassengerOrigin.value
        var destination = homeViewModel.mPassengerDestination.value

        Log.d("MyOrigin","or: "+origin)

        // 3. SEND A REQUEST TO FIND DRIVERS
        var trip = MapRoute()
        trip.origin = origin
        trip.destination = destination

        var pickLocation = LatLng(origin!!.latitude, origin.longitude)

        var fare = MapUtils.findFareByDistance(trip.origin!!,trip.destination!!)
        var dollars ="Calculated Fee: "+MapUtils.formatToDollars(fare)

        Log.d("PolyPath","MyPathBefore: "+pickLocation)


        tv_price_of_distance.text = dollars


        var tempPickLocation = LatLng(0.44193,33.20191)


        button_confirm_dest.setOnClickListener{

            searchViewProcessSearch()
            tripFinderController.findLifts(tempPickLocation)
            liftsViewShowLifts()

        }

    }


    private fun searchViewProcessSearch(){

       // 3.1 -  SEND A REQUEST TO FIND DRIVERS - DISPLAY PROCESS
       prompt_text_search.visibility = View.INVISIBLE
       prompt_text_confirm.visibility = View.INVISIBLE
       button_confirm_dest.visibility = View.INVISIBLE

       autocomplete_fragment.view?.visibility = View.GONE
       prompt_text_process_request.visibility = View.VISIBLE

   }

    // HOME SCREEN  - FIND LIFTS VIEWS
    private lateinit var recycler : RecyclerView
    private fun setUpRecyclerView(){
        recycler = findViewById(R.id.recycler_drivers)
        recycler_drivers.visibility = View.VISIBLE

        // val layoutManager = LinearLayoutManager(context)
        var gridColumns = 2

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false)
        recycler.layoutManager = layoutManager


        //  decoration = DividerItemDecoration(getContext(), VERTICAL)

        var offset = 10
        // val end =  StartOffsetItemDecoration(offset)
        // recycler.addItemDecoration()

        //setting animation on scrolling
        recycler.setHasFixedSize(true); //enhance recycler view scroll
        recycler.isNestedScrollingEnabled = false;// enable smooth scrooll



    }


    private fun liftsViewShowLifts(){
        //use the trifinder viewmodel
        //observe available drivers

        tripFinderVModel.listOfLiftsError.observe(this, Observer{error->

            Toast.makeText(this, error,Toast.LENGTH_LONG).show()

             searchView()

        })

        tripFinderVModel.listOfLifts.observe(this, Observer {liftResult->

            prompt_drivers.visibility = View.VISIBLE
            //prompt_dest.visibility = View.GONE
            prompt_track.visibility = View.GONE
            prompt_trip.visibility = View.GONE
            prompt_search.visibility = View.GONE


            recycler_drivers.visibility = View.VISIBLE
            layout_search_drivers_request_sent.visibility =View.GONE

            // 3.3 SEND A REQUEST TO FIND DRIVERS - UPDATE RESULTS
            var noOfDrivers = ""+liftResult.size +" driver(s)"
            tv_search_drivers_no_of_drivers.text = noOfDrivers

            var adapter =
                AdapterDrivers(
                    this,
                    liftResult,
                    this
                )
            adapter.setHasStableIds(true)
            adapter.notifyItemInserted(0)
            recycler.adapter  = adapter

            //  passengerUiPresenter.display3ShowDrivers(list)
        })

        // 3.2 SEND A REQUEST TO FIND DRIVERS -- FIND DRIVERS


        liftsViewShowWalletBalance()
        liftsViewDepositMoney()

    }

    var manageWalletRepository = ManageWalletRepository()

    private fun liftsViewShowWalletBalance(){

        manageWalletRepository.getWalletBalance(mAuth?.uid!!,object: ManageWalletRepository.GetWalletBalanceCallback{

            override fun onGetWalletBalance(balance: BigDecimal) {


               var dollars =  MapUtils.formatToDollars(balance)

                var balText = "Balance: $dollars"

                btnWalletBalance.text = balText

            }

            override fun onFailed(error: String) {

            }

        })

    }

    private fun liftsViewDepositMoney(){

        btnWalletBalance.setOnClickListener {

           openDepositScreen()

        }

    }

    private fun openDepositScreen(){
     //remove the recycler view and put sending of a request
        val intent = Intent(this,DepositMoneyActivity::class.java)
        val bundle = Bundle()


        Log.d("MyID","to put: "+mAuth?.uid)
        bundle.putString("userId",mAuth?.uid)
        intent.putExtras(bundle)
        startActivity(intent)

    }




    // HOME SCREEN - REQUESTS VIEW ///////////////////////////////////////////////////////
    override fun onDriverClick(view: MaterialButton, pos: Int, driver: TripFinderViewModel) {

        // 4. ON DRIVER CLICK - SEND DRIVER A REQUEST
         view.isEnabled = false
         var processtext = "Processing request..."
         progressBarRequest.visibility = View.VISIBLE
         view.visibility = View.INVISIBLE

         label_search_drivers.text = processtext
        // view.setTextColor(resources.getColor(R.color.quantum_grey500))
         label_search_drivers.setBackgroundColor(resources.getColor(R.color.quantum_grey900))

         Log.d("TripFinder","seats: "+driver.vehicleSeats)

         // Simulate driver waiting for request
         // val dtext = "PROCESSING REQUEST ..."
         // view.text = dtext

        manageWalletRepository.getWalletBalance(mAuth?.uid!!,object: ManageWalletRepository.GetWalletBalanceCallback{

            override fun onGetWalletBalance(balance: BigDecimal) {


                //var cFare = MapUtils.convertFareByDistance()

                if(balance.toFloat() > 10000){
                    var runMain = Runnable {
                        // sendDriverRequest(driver)
                        sendRequestNotification(driver)
                    }
                    // AppExecutors().mainThread().execute(runMain)
                    MainThreadExecutor(2000).execute(runMain)

                }else{

                    view.isEnabled = true
                    val reqtext = "REQUEST"
                    progressBarRequest.visibility = View.GONE
                    view.visibility = View.VISIBLE

                    label_search_drivers.text = reqtext

                    accountBalanceError.visibility = View.VISIBLE
                    Toast.makeText(baseContext,"Top up account to make a request", Toast.LENGTH_LONG).show()
                }

            }

            override fun onFailed(error: String) {

            }

        })


    }


    //Run this methhod after getting the notification
    private fun requestsViewShowRequestSent(){
        //  Toast.makeText(baseContext,msg,Toast.LENGTH_LONG).show()
        tripRequestsViewModel.mSendRequestResponse.observe(this,Observer{res->

            progressBarRequest.visibility = View.INVISIBLE

            // 4.1 ON DRIVER CLICK - SEND DRIVER A REQUEST - SET TRACKING DRIVER DATA TO BE OBSERVED
            // 4.2 ON DRIVER REQUEST SENT  - A LOCATION CALLBACK WILL NOTIFY DRIVER ARRIVAL
            // Simulate location callback

            // Display UI to show waiting -- requesting please wait
            recycler.visibility = View.GONE
            layout_search_drivers_request_sent.visibility = View.VISIBLE


            label_search_drivers_request_car.text = res.vehicleName
            label_search_drivers_request_driver_name.text = res.driverName
            // label_search_drivers_request_title
            // label_search_drivers_request_subtitle
            tv_search_drivers_no_of_drivers.text = ""
            
            //SET THE DRIVER ORIGIN IT WILL BE MAPPED WITH THE PASSENGER ORIGIN
            homeViewModel.mDriverOrigin.postValue(res.driverOrigin)

            // start tracking driver
            // sheetPassengerMap.toggleBottomSheet()

          //  requestsViewMonitorRequestShowDriverComing(res.tripId!!)

            //I want to execute monitoring when user comsback onResume
            pref.putBoolPref(PreferenceHandler.KEY_MONITOR_REQUEST,true)
            pref.putPref(PreferenceHandler.KEY_MONITOR_REQUEST_TRIP_ID,res.tripId!!)



        })

    }



    // [NOT IN USE] ///////////////
    // [Replaced observiance of monitoring request acceptance] //////////////
    private fun requestsViewMonitorRequestShowDriverComing(tripId: String){

        /////////////   Check Whether the request was accepted or not //////////////////////

        tripRequestsController.monitorRequestAcceptance(tripId)
        // If Request has been accepted
        tripRequestsViewModel.mTrackRequestResponse.observe(this, Observer{request->
            pref.putBoolPref(PreferenceHandler.KEY_MONITOR_REQUEST,false)

            //show trip route
            homeViewModel.setTrackDataFromDriverToPassenger()

            prompt_drivers.visibility = View.GONE
            prompt_search.visibility = View.GONE
            // prompt_dest.visibility = View.GONE
            prompt_trip.visibility = View.GONE
            prompt_track.visibility = View.VISIBLE


            //Monitor the OnTrip in case the driver has set it
            var myLocation = Location(LocationManager.GPS_PROVIDER)
            myLocation.latitude = request.origin!!.latitude!!
            myLocation.longitude = request.origin!!.longitude!!


            tripRequestsController.getRequestedDriverComingDetails(request.tripId!!)
            tripRequestsViewModel.mTrackRequestDriverComing.observe(this,Observer{driverComingViewModel->

                //1. display profile photo
                val imageUri = driverComingViewModel.imageUrl
                Picasso.with(baseContext).load(imageUri)
                    .fit().centerCrop()
                    .placeholder(AvatarGenerator.avatarImage(baseContext, 100, AvatarConstants.RECTANGLE, driverComingViewModel.driverDisplayName))
                     //.error(R.drawable.user_placeholder_error)
                    .into(iv_track_driver_avator)

                //2. display name
                tv_passenger_track_driver_name.text = driverComingViewModel.driverDisplayName
                tv_passenger_track_driver_car_name.text = driverComingViewModel.vehicleDetailsName
                tv_passenger_track_driver_car_seats.text = driverComingViewModel.vehicleDetailsSeats

                requestsViewMonitorDriverArriving(request.tripId!!,myLocation)

            })

        })


    }


    private fun requestsViewMonitorDriverArriving(tripId: String,myLocation: Location){
        tripRequestsController.monitorRequestDriverArrival(tripId,myLocation)

        var run = Runnable {

            tripRequestsViewModel.mTrackRequestDriverArriving.observe(this,Observer{arrivingViewModel->

                // GET DRIVER INFORMATION
                //1. show driver has arrived text
                label_trip_tracking_header_card.text = arrivingViewModel.trackArrivalNotice

                //2. show enjoy ride info
                label_passenger_trip_track_driver.text = arrivingViewModel.trackArrivalTitle
                label_passenger_trip_tracking_wait_subtitle.text = arrivingViewModel.trackArrivalSubtitle

                //3. show plate and destination
                layout_trip_tracking_show_plate.visibility = View.VISIBLE
                label_passenger_trip_track_car_plate.text = arrivingViewModel.trackArrivalNoPlate
                MapUtils(baseContext).reverseGeocoding(arrivingViewModel.locationDes,label_passenger_trip_track_car_destination,true)

                // Listen for the trip if driver clicked start
                listenForStartOfTrip(tripId)
                // Listen for end of trip

                button_trip_tracking_start.setOnClickListener{
                    //user clicks start trip

                }

            })


        }
        MainThreadExecutor(5000).execute(run)


        tripRequestsViewModel.mTrackRequestDriverArrivingError.observe(this,Observer { error ->

            Toast.makeText(this,error,Toast.LENGTH_LONG).show()

        })

    }


    // HOME SCREEN - TRIP BEGIN VIEW
    private var isOnTripStarted = 0
    private fun listenForStartOfTrip(tripId: String){

        TripManagementController().tripMgtRepo.getOnTripDetails(tripId,object:
            TripManagementRepository.OnTripDetailsCallback {
            override fun onTripReceived(onTrip: Trip) {

                // 4.4 ON DRIVER LOCATION ARRIVAL - SET DATA FOR THE FINAL TRIP - PASSENGER ORIGIN - DROPOFF
                // Set joined trip
                // Set cordinates to track trip
                // Passenger origin - driver destination

                // Start tracking the trip
                if(onTrip.startedTrip?.tripStatus == Trip.StartTrip.TRIP_STARTED) {
                    if(isOnTripStarted == 0) {
                        isOnTripStarted++ //thus the other check will find it incremented to 1
                        tripViewShowStartTrip(onTrip)
                    }
                }else{
                  //  Toast.makeText(baseContext,"Waiting to start Trip",Toast.LENGTH_LONG).show()
                }


            }

            override fun onFailed(error: String) {

            }

        })

    }


    private fun tripViewShowStartTrip(onTrip: Trip) {

        prompt_drivers.visibility = View.GONE
        prompt_search.visibility = View.GONE
        // prompt_dest.visibility = View.GONE
        prompt_track.visibility = View.GONE
        prompt_trip.visibility = View.VISIBLE

        progressBarEndTripByPassenger.visibility = View.VISIBLE
        layout_driver_on_trip_passenger_profile.visibility = View.GONE
        layout_started_trip_details_header.visibility = View.GONE



        var mapRoute = MapRoute()
        mapRoute .customerId = onTrip.startedTrip!!.passengerId
        mapRoute .tripRequestId = onTrip.requestId

        var locationOrigin =  LatLng(onTrip.startedTrip!!.tripOrigin!!.latitude!!,onTrip.startedTrip!!.tripOrigin!!.longitude!!)
        var locationDestination =  LatLng(onTrip.startedTrip!!.tripDestination!!.latitude!!,onTrip.startedTrip!!.tripDestination!!.longitude!!)

        mapRoute.destination = locationDestination
        mapRoute.origin = locationOrigin

        //homeViewModel.mPassengerTripOrigin.postValue(locationOrigin)
        //homeViewModel.mDriverTripDestination.postValue(locationDestination)
        homeViewModel.setTrackDataForFinalTrip(locationOrigin,locationDestination)

        // GET DRIVER INFORMATION
        TripManagementController().tripMgtRepo.getOnTripDetails(onTrip.tripId!!, object : TripManagementRepository.OnTripDetailsCallback{

            override fun onTripReceived(onTrip: Trip) {

                progressBarEndTripByPassenger.visibility = View.GONE
                layout_driver_on_trip_passenger_profile.visibility = View.VISIBLE
                layout_started_trip_details_header.visibility = View.VISIBLE


                //1. display profile photo
                val imageUri = onTrip.driverPicUrl
                Picasso.with(baseContext).load(imageUri)
                    .fit().centerCrop()
                    .placeholder(AvatarGenerator.avatarImage(baseContext, 100, AvatarConstants.RECTANGLE, onTrip.driverName!!))
                    //.error(R.drawable.user_placeholder_error)
                    .into(iv_item_driver_avator_ontrip)

                //2. display name
                tv_on_trip_details_driver_name.text = onTrip.driverName
                //3. care details
                tv_on_trip_details_car_name.text = onTrip.tripvehicle!!.vehicleName
                //4. trip details
                var distance = ""+onTrip.distance+" km"


                /////// DISTANCE AND DROP OFF ///////////////////////////////////////////////////////
                layout_end_trip_bypassenger.visibility = View.VISIBLE
                layout_rate_trip_bypass.visibility = View.GONE

                // tv_passenger_on_trip_details_amount.text = onTrip.fare
                tv_passenger_on_trip_details_distance.text =  distance

                var dest = Location(LocationManager.GPS_PROVIDER)
                dest.longitude = onTrip.tripDestination!!.longitude!!
                dest.latitude = onTrip.tripDestination!!.latitude!!

                var origin = Location(LocationManager.GPS_PROVIDER)
                origin.longitude = onTrip.tripOrigin!!.longitude!!
                origin.latitude = onTrip.tripOrigin!!.latitude!!

                MapUtils(baseContext).reverseGeocoding(dest,tv_on_trip_details_dropoff,true)
                // MapUtils(baseContext).reverseGeocoding(origin,tv_on_trip_details_pickup,true)
                //////////////////////////////////////////////////////////////////////////////////////

                button_end_trip_by_passenger.setOnClickListener{
                    //endTripByPassenger(onTrip,onTrip.requestId!!)
                    //progressBarEndTripByPassenger.visibility = View.VISIBLE
                    showProcessPayment(true)

                    endTrip(false,onTrip,onTrip.startedTrip?.requestId!!)
                }


                 monitorEndedTrip(false,onTrip)
                 monitorTripReached(onTrip.tripId!!,dest)

            }

            override fun onFailed(error: String) {

            }

        })


    }

           //functions
    private fun monitorTripReached(tripId: String,tripDestination: Location){

        TripManagementController().tripMgtRepo.isTripReached(tripId,tripDestination,object:
            TripManagementRepository.TripReachedCallback {


            override fun onSuccess(info:String) {

                Toast.makeText(baseContext, "Destination Reached! $info",Toast.LENGTH_LONG).show()

            }

            override fun onFailed(error: String) {

            }


        })

    }



    private fun monitorEndedTrip(isUIDriver: Boolean, trip: Trip){
        TripManagementController().tripMgtRepo.monitorTripEnded(trip.tripId!!, object:
            TripManagementRepository.MonitorTripEndedCallback {
            override fun onTripEnded() {

                if(isUIDriver){

                    Toast.makeText(baseContext,"Thank you Ended",Toast.LENGTH_LONG).show()
                    showDriverUITripStarted(false,trip.tripId!!)



                    // getTripSummaryDetails(trip.tripId!!)

                }else{


                    // BEFORE YOU SHOW PASSENGER RECEIPT, MAKE THE TRANSACTION
                    // var billId = pref.getPref("billId")
                    //  var billId =  PreferenceHandler(baseContext).getPref("billId")

                    billMgt.getBillDetails(mAuth?.uid!!, object : BillManagementRepository.GetBillCallback {

                        override fun onGetBill(bill: Bill) {

                            //1. I have the bill
                            //2. Get the pass balance
                            var amountToPass = bill.fare

                            Log.d("PassId","IdPass: "+amountToPass)

                            manageWalletRepository.transactPayment(mAuth?.uid!!,trip.driverId!!,amountToPass!!, object:ManageWalletRepository.TransactPaymentCallback{

                                override fun onPaymentTransacted(transaction: WalletAccount.AccountTransaction) {

                                    Toast.makeText(baseContext, "Amount: "+transaction.amount+" transacted", Toast.LENGTH_LONG).show()

                                   // manageWalletRepository.removeRecordWalletTransactionListener()
                                    manageWalletRepository.resetX()
                                    billMgt.removeBill(transaction.payerId!!,object : BillManagementRepository.RemoveBillCallback{

                                        override fun onRemoveBill() {


                                            // AFTER PROCESS THE PAYMENT //////////////////////////////////////
                                            showProcessPayment(false)
                                            ///////////////////////////////////////////////////////////////////

                                            showPassengerReceipt(trip,transaction)


                                        }

                                        override fun onFail(error: String) {

                                        }

                                    })


                                }

                                override fun onFailed(error: String) {

                                    Toast.makeText(baseContext, "Error "+error, Toast.LENGTH_LONG).show()

                                }

                            })


                        }

                        override fun onFail(error: String) {

                        }

                    })



                }


            }

            override fun onTripEndedFailed(error: String) {

            }

        })

    }


    // HOME SCREEN - RECEIPT VIEW
    private fun showPassengerReceipt(onTrip: Trip,transaction: WalletAccount.AccountTransaction){
        // display a receipt to the riverR
       // sheetPassengerReceipt.showBottomSheet(true,heightHigh)
       // sheetPassengerReceipt.toggleBottomSheet()
       // sheetPassengerMap.showBottomSheet(false,heightMedium)
        // CLEAR DOWN ALL THE SHEETS ///////////////////////////////////////

        var driverName = "How was your trip with!! "+onTrip.driverName

        var dest = Location(LocationManager.GPS_PROVIDER)
        dest.longitude = onTrip.tripDestination!!.longitude!!
        dest.latitude = onTrip.tripDestination!!.latitude!!

        var origin = Location(LocationManager.GPS_PROVIDER)
        origin.longitude = onTrip.tripOrigin!!.longitude!!
        origin.latitude = onTrip.tripOrigin!!.latitude!!

        var labelTransaction = "TOTAL COST: "+MapUtils.formatToDollars(BigDecimal(transaction.amount))
        label_transaction.text = labelTransaction


       // MapUtils(this).reverseGeocoding(dest,tv_passenger_trip_receipt_details_dropoff,true)
       // MapUtils(this).reverseGeocoding(origin,tv_passenger_trip_receipt_details_pickup,true)

       tv_rate_trip_title_bypass.text = driverName
       // tv_passenger_trip__amount.text = onTrip.fare
       // tv_passenger_trip_distance.text =onTrip.distance.toString()

        btn_submit_trip_rating_pass.setOnClickListener{

            var progress = "Submitting your rating.."
            //btn_submit_trip_rating_pass.text = progress
            var rating = rating_bar_rate_trip_bypass.rating.toString()

                submitPassengerRating(onTrip.tripId!!,rating)

    }

    }


    fun submitPassengerRating(tripId: String,tripRating: String){

        btn_submit_trip_rating_pass.isEnabled = false
        progressBarEndTripByPassenger.visibility = View.VISIBLE


        var dMan = TripManagementController().tripMgtRepo

        dMan.submitRating(false,tripId, tripRating, object :
            TripManagementRepository.RatingTripCallback {

            override fun onSubmited() {


                MainThreadExecutor(2000).execute(Runnable {

                    progressBarRequestEndTrip.visibility = View.GONE
                    //layout_end_trip_bypassenger.visibility = View.VISIBLE
                    //layout_rate_trip_bypass.visibility = View.GONE

                    // layout_driver_on_trip_passenger_profile.visibility = View.GONE
                    // layout_started_trip_details_header.visibility = View.GONE
                    // progressBarEndTripByPassenger.visibility = View.GONE

                    layout_started_trip_details_header.visibility = View.GONE
                    layout_driver_on_trip_passenger_profile.visibility = View.GONE

                    val processP = "Thankyou! Bye"
                    label_on_trip_pass.text = processP

                    refreshUI(false)
                })

            }

            override fun onFailed(error: String) {

            }
        })

    }
























    // DRIVER UI SETUP/////////////////////////////////

    private fun configDriverTrip(){

        addCar.setOnClickListener {

            var b = Bundle()
            b.putString("driverId", mAuth?.uid!!)
            var intent = Intent(this, RegisterVehicleActivity::class.java)
            intent.putExtras(b)
            startActivity(intent)
        }

        userMgtController.getRegisteredVehicles(mAuth?.uid!!)
        userProfileViewModel.mGetRegisteredVehicles.observe(this,Observer{vehicles->

           displayUIDriverCars(vehicles)

        })
        userProfileViewModel.mGetRegisteredVehiclesEmpty.observe(this,Observer{emptyText->

            Toast.makeText(baseContext,emptyText,Toast.LENGTH_LONG).show()

        })

    }

    var array = ArrayList<TripVehicle?>()
    private fun isItemInArray(vehicle: TripVehicle):Boolean{
      for (item in array){
          if(item!!.vehicleNoPlate.equals(vehicle.vehicleNoPlate)){
              return true
          }
      }
       return false
    }

    private fun removeDuplicates(liftResult: MutableList<TripVehicle>): ArrayList<TripVehicle?> {

        for(item in liftResult){
            //check if item is in the array
           var isCarInArray = isItemInArray(item!!)
            if(!isCarInArray){
                array.add(item)
            }
        }

     return array
    }

    private  var adapterCars : AdapterDriverCars? = null
    private fun displayUIDriverCars(liftResult:MutableList<TripVehicle>) {

        showDriverUI(UI_NO)
        showDriverUI(UI_CONFIG)


        if(adapterCars!=null) {
            adapterCars!!.clear()
            adapterCars = null
        }

        val unDuplicatesList: ArrayList<TripVehicle?> = ArrayList<TripVehicle?>(LinkedHashSet<TripVehicle?>(liftResult))
        var tripVehicles = removeDuplicates(liftResult)

        var availableDrivers = "You have "+tripVehicles.size+" vehicles in garage"
        add_car_subtitle.text = availableDrivers

        adapterCars = AdapterDriverCars(this, tripVehicles, this)


        // adapterCars.setHasStableIds(true)
        // adapterCars.notifyItemInserted(0)
        adapterCars?.notifyDataSetChanged()
        recycler_driver_mycars.adapter  = adapterCars

    }

    override fun onDriverCarClick(view: MaterialCardView, pos: Int, carSelected: TripVehicle) {

        adapterCars?.notifyDataSetChanged()
        // view.setCardBackgroundColor(resources.getColor(R.color.colorPrimary))

        resetSeats()
        //  var run = Runnable {}
        //  MainThreadExecutor(500).execute(run)

        // open layout online
        layout_go_online.visibility = View.VISIBLE
        layout_go_online_select_seats.visibility = View.VISIBLE

        selectCarSeats()
        btn_go_online.setOnClickListener{

            driverPresenter.displayDriverUISearchDestination()

            var seats = tv_select_car_seats.text
            carSelected.vehicleSeats = seats.toString()
            //you set the car selected to be go by the fragment
            setCarSelected(carSelected.vehicleId!!,carSelected.vehicleName!!,carSelected.vehicleNoPlate!!,carSelected.vehicleSeats!!)

            searchDriverDestinationByfragment() //setup search

            //After clicking DisplayCarDetails on the search ui
            var car = ""+carSelected.vehicleName+" | "+carSelected.vehicleNoPlate!! +" seats"
            tv_selected_car_confirm.text = car
           // iv_selected_car_confirm.setImageResource(carSelected.carPhoto)

        }
    }


    lateinit var carSelected : TripVehicle
    fun setCarSelected(carId:String,carName:String,carPlate:String,carSeats:String){
        carSelected = TripVehicle()
        carSelected.vehicleId = carId
        carSelected.vehicleName = carName
        carSelected.vehicleSeats = carSeats
        carSelected.vehicleNoPlate = carPlate
        carSelected.vehiclePhoto = "url"
    }

    var seats = 1
    var textSeats = ""
    fun resetSeats(){
        seats = 1
        textSeats = ""+seats
        tv_select_car_seats.text = textSeats
    }
    fun selectCarSeats(){

        btn_select_car_add_seats.setOnClickListener {
            seats++
            textSeats = ""+seats
            tv_select_car_seats.text = textSeats
        }
        btn_select_car_minus_seats.setOnClickListener {
            if(seats>1) {
                seats--
                textSeats = ""+seats
                tv_select_car_seats.text = textSeats
            }
        }

    }


    override fun displayDriverUISearchDestination() {
       showDriverUI(UI_SEARCH)
        //prompt_dest.visibility = View.GONE
        showDriverUISearchDestination(1)
    }

    fun searchDriverDestinationByfragment(){

        var autocompleteFragment : AutocompleteSupportFragment =
            // Initialize the AutocompleteSupportFragment.
            supportFragmentManager.findFragmentById(R.id.autocomplete_driver_fragment)
                    as AutocompleteSupportFragment

        // 1. SEARCH YOUR DESTINATION
        var text = ""
         autocompleteFragment.setText(text)

        autocompleteFragment.setTypeFilter(TypeFilter.ADDRESS)

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG))

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {

                //2. set the destination to draw line
                homeViewModel.mPassengerDestination.value = place.latLng!!
               // Log.d("MainActivity-MA", "Observe-Location-dest: $place.latLng")
               // Log.d("MainActivity-MA", "Observe-Location-dest:" +homeViewModel.mPassengerDestination.value)

            //    Log.d("TripActive","Active")

                if(checkIfTripIsActive()){
                    var label = "You have an active trip"
                    var snack = Snackbar.make(layout_text_driver_create_trip, label, Snackbar.LENGTH_LONG)
                    snack.anchorView = layoutBottomSheetDriver
                    snack.show()

                }else{
                    setDriveRouteAfterDestinationSelection()
                    driverPresenter.displayDriverUICreateTrip(place.latLng!!,place.address!!)
                }//then observe

                /*after observation the route is created and drawn but the directions maynot be found
                var isDirectionsFound = pref.getBoolPref(PreferenceHandler.KEY_IS_DIRECTIONS_FOUND)
                if(isDirectionsFound) {
                 */
                //}

                // got after selecting the vehicle
            }
            override fun onError(status: Status) {
                // TODO: Handle the error.
                Log.d("PLACES_ACTIVITY", "An error occurred: $status")
            }
        })

    }

    fun setDriveRouteAfterDestinationSelection(){
        homeViewModel.passengerDestination.observe(this, Observer {

            Log.d("MainActivity-MA", "TripDestination Observed")
            //1. first set the car
            var car = TripVehicle()
            car.vehicleSeats = carSelected.vehicleSeats
            car.vehicleNoPlate = carSelected.vehicleNoPlate
            car.vehicleName = carSelected.vehicleName

            // Make the MapRoute
            //The trip needs the driver name and driver pic
            userMgtController.getUserProfile(mAuth?.uid!!)

            userProfileViewModel.liveDataUserDetails.observe(this,Observer{details->
                homeViewModel.setDriverTripRoute("2000",mAuth?.uid!!,details.userName,details.userPhotoUrl,car)
            })

            /*
             userMgtController.getDriverDetails(user!!.uid)
            homeViewModel.mGetDriverDetails.observe(this,Observer{
                homeViewModel.setDriverTripRoute("2000",user!!.uid,it,car)
            })
             */

        })

    }


    private fun checkIfTripIsActive():Boolean{

        var isActive = false

        var isTripSupplied = pref.getPref(PreferenceHandler.KEY_TRIP_ID)

        if(isTripSupplied != null) {
            isActive = true
            button_create_trip.isEnabled = false
            //  Toast.makeText(this, "You have an active trip", Toast.LENGTH_LONG).show()
        }else{
            isActive =false
            button_create_trip.isEnabled = true
        }

        return isActive
    }

    private fun showDriverUISearchDestination(level: Int){

            if(level == 1){ //Search drivers
                layout_text_driver_search.visibility = View.VISIBLE
                layout_driver_search_header.visibility = View.VISIBLE

                layout_text_driver_create_trip.visibility = View.GONE
                button_create_trip.visibility = View.GONE
                layout_text_driver_process_request.visibility = View.GONE

            }else if(level == 2){ //Create trip
                Log.d("YES_VISIBLE","level 2")

                // sheetDriver.toggleBottomSheet()
                layout_text_driver_create_trip.visibility = View.VISIBLE
                button_create_trip.visibility = View.VISIBLE

                layout_text_driver_process_request.visibility = View.GONE
                layout_text_driver_search.visibility = View.GONE
                //  sheetDriverReceipt.showBottomSheetReceit(false)
            }
    }




    /////// TRIP CREATION /////////////////////////////////
    override fun displayDriverUICreateTrip(driverDestination: LatLng, address:String) {

        prompt_driver_trip_track_passenger.visibility = View.GONE

        // autocomplete_driver_fragment.view?.visibility = View.GONE
        showDriverUISearchDestination(2)

        button_create_trip.setOnClickListener {
                progressBarCompleteTrip.visibility = View.VISIBLE


                // Get the trip details from the livedata
                homeViewModel.tripLiveData.observe(this, Observer { trip ->
                  //  Toast.makeText(this, "observed: "+trip.driverName, Toast.LENGTH_LONG).show()
                  //   textView.text = it
                if (trip != null) {

                    var tripOriginLatLng = MapUtils.convertFromTripLocationToLatLng(trip.tripOrigin!!)
                    var tripDesLatLng = MapUtils.convertFromTripLocationToLatLng(trip.tripDestination!!)

                    var mapRoute = MapRoute()
                    mapRoute.origin = tripOriginLatLng
                    mapRoute.destination = tripDesLatLng

                    //Get the location points of the polyline
                    MapUtils(this).getTripLocationsFromPolylines(mapRoute, object : MapUtils.TripRouteCallback {

                        override fun onSuccess(tripLocations: ArrayList<TripLocation>) {

                            trip.triproute = tripLocations
                            TripSupplyController().tripSupplyRepo.createTrip(trip,
                                object : TripSupplyRepository.OnTripCreatedCallback {

                                    override fun onSuccess() {

                                        showDriverUI(UI_NO)

                                        showDialogUI("Trip Details","Your trip to $address created successfully" )

                                        pref.putPref(PreferenceHandler.KEY_TRIP_ID, trip.tripId)
                                        pref.putBoolPref(PreferenceHandler.KEY_IS_ON_TRIP, true)
                                        progressBarCompleteTrip.visibility = View.GONE
                                        waitForRequestsAfterTripCreation()

                                        homeViewModel.tripLiveData.removeObservers(this@MainActivity)


                                    }

                                    override fun onFailed(error: String) {
                                        Toast.makeText(baseContext, error, Toast.LENGTH_LONG).show()
                                    }


                                })
                        }

                        override fun onFailed(error: String) {

                        }

                    })

                } else {

                }
            })



        }
    }







    ///////   TRIP REQUEST WAITING /////////////////////////////////

    private var isRequestWaitingInitiated = false

    fun waitForRequestsAfterTripCreation(){

        homeViewModel.tripLiveData.observe(this, Observer {
            //   textView.text = it
            if(it!=null){

                Log.d("DRIVER-REQUEST-OBServed", "OriginDestination true: ${it.driverName}")

              //  showPendingRequest(it.tripId!!)

            }else{

            }
        })

    }


    private fun showPendingRequest(tripId: String){

        // This observes the request if it's pending
        // But now we have to

        Log.d("DRIVER-REQUEST-GOT", "pending request:")
        tripRequestsController.waitOnRequests(tripId)
        //homeViewModel.mWaitOnRequests

        tripRequestsViewModel.mWaitOnRequests.observe(this, Observer {requests->

            if(requests[0].requestStatus == TripRequest.PENDING) {
                sheetDriver.showBottomSheet(true,heightMedium)
                showDriverUI(UI_WAIT_START) //to track and start
                isRequestWaitingInitiated = true

                Log.d("DRIVER-REQUEST-GOT", "onChildChanged:" + requests[0].requestId)
                // show the passenger
                // from here track the passenger
                // when you reach show start trip
                userMgtController.getPassengerDetails(requests[0].passengerId!!)
                userProfileViewModel.mGetPassengerDetails.observe(this, Observer { passenger->

                    val imageUri = passenger.passengerPicUrl
                    Picasso.with(baseContext).load(imageUri)
                        .fit().centerCrop()
                        .placeholder(AvatarGenerator.avatarImage(baseContext, 100, AvatarConstants.RECTANGLE, passenger.passengerFirstName!!))
                        //.error(R.drawable.user_placeholder_error)
                        .into(request_track_passenger_avator)

                         Log.d("DRIVER-REQUEST-GOT", "request status:" + requests[0].requestStatus)


                    //DETAILS  ///////////////////////////////////////
                    var reqMiniId = requests[0].requestId!!.split("-")
                    var req_id = reqMiniId[0]

                    label_request_track_passenger_subtitle.text = req_id
                    request_track_passenger_name.text = requests[0].passengerName
                    // request_track_passenger_detail.text = request.tripId

                    var locDes = Location(LocationManager.GPS_PROVIDER)
                    locDes.longitude = requests[0].destination!!.longitude!!
                    locDes.latitude = requests[0].destination!!.latitude!!

                    var locOrigin = Location(LocationManager.GPS_PROVIDER)
                    locOrigin.longitude = requests[0].origin!!.longitude!!
                    locOrigin.latitude = requests[0].origin!!.latitude!!


                    MapUtils(baseContext).reverseGeocoding(locDes,request_track_passenger_detail_dropoff,true)
                    MapUtils(baseContext).reverseGeocoding(locOrigin,request_track_passenger_detail_pickup,true)
                    //request_track_passenger_detail_dropoff.text = locDes
                    //request_track_passenger_detail_pickup.text = locOr


                    //Now you can offer to accept or reject the request
                    button_trip_request_accept.setOnClickListener{

                        progressBarRequestAccept.visibility = View.VISIBLE
                        button_trip_request_accept.isEnabled = false
                        button_trip_request_reject.isEnabled = false

                        respondToRequest(requests[0], TripRequest.ACCEPTED)
                    }
                    button_trip_request_reject.setOnClickListener{

                        progressBarRequestAccept.visibility = View.VISIBLE
                        button_trip_request_accept.isEnabled = false
                        button_trip_request_reject.isEnabled = false

                        respondToRequest(requests[0], TripRequest.REJECTED)
                    }
                })


            }else{
                //Log.d("DRIVER-REQUEST-GOT", "2nd request:" + requests[1].requestId)
            }
        })

        tripRequestsViewModel.mWaitOnRequestsError.observe(this, Observer {empty->

            Toast.makeText(this,empty,Toast.LENGTH_LONG).show()

        })
    }

    private fun showPendingRequestFCM(request: TripRequest){

        // This observes the request if it's pending
        // But now we have to

        Log.d("DRIVER-REQUEST-GOT", "pending request:")


                sheetDriver.showBottomSheet(true,heightMedium)
                showDriverUI(UI_WAIT_START) //to track and start
                isRequestWaitingInitiated = true

                Log.d("DRIVER-REQUEST-GOT", "onChildChanged:" + request.requestId)
                // show the passenger
                // from here track the passenger
                // when you reach show start trip
                userMgtController.getPassengerDetails(request.passengerId!!)
                userProfileViewModel.mGetPassengerDetails.observe(this, Observer { passenger->

                    val imageUri = passenger.passengerPicUrl
                    Picasso.with(baseContext).load(imageUri)
                        .fit().centerCrop()
                        .placeholder(AvatarGenerator.avatarImage(baseContext, 100, AvatarConstants.RECTANGLE, passenger.passengerFirstName!!))
                        //.error(R.drawable.user_placeholder_error)
                        .into(request_track_passenger_avator)

                    Log.d("DRIVER-REQUEST-GOT", "request status:" + request.requestStatus)


                    //DETAILS  ///////////////////////////////////////
                    var reqMiniId = request.requestId!!.split("-")
                    var req_id = reqMiniId[0]

                    label_request_track_passenger_subtitle.text = req_id
                    request_track_passenger_name.text = request.passengerName
                    // request_track_passenger_detail.text = request.tripId

                    var locDes = Location(LocationManager.GPS_PROVIDER)
                    locDes.longitude = request.destination!!.longitude!!
                    locDes.latitude = request.destination!!.latitude!!

                    var locOrigin = Location(LocationManager.GPS_PROVIDER)
                    locOrigin.longitude = request.origin!!.longitude!!
                    locOrigin.latitude = request.origin!!.latitude!!


                    MapUtils(baseContext).reverseGeocoding(locDes,request_track_passenger_detail_dropoff,true)
                    MapUtils(baseContext).reverseGeocoding(locOrigin,request_track_passenger_detail_pickup,true)
                    //request_track_passenger_detail_dropoff.text = locDes
                    //request_track_passenger_detail_pickup.text = locOr


                    //Now you can offer to accept or reject the request
                    button_trip_request_accept.setOnClickListener{

                        progressBarRequestAccept.visibility = View.VISIBLE
                        button_trip_request_accept.isEnabled = false
                        button_trip_request_reject.isEnabled = false

                      //  respondToRequest(requests[0], TripRequest.ACCEPTED)
                    }
                    button_trip_request_reject.setOnClickListener{

                        progressBarRequestAccept.visibility = View.VISIBLE
                        button_trip_request_accept.isEnabled = false
                        button_trip_request_reject.isEnabled = false

                      //  respondToRequest(requests[0], TripRequest.REJECTED)
                    }
                })



        tripRequestsViewModel.mWaitOnRequestsError.observe(this, Observer {empty->

            Toast.makeText(this,empty,Toast.LENGTH_LONG).show()

        })
    }


    private var isRequestAcceptedReceived = true
    private fun respondToRequest(request: TripRequest, tripRequestResponse: String){
        //1. Need the token of the request
        //2. Using the token send the accept


      //  tripRequestsController.respondToRequest(mAuth?.uid!!,request,tripRequestResponse)
        tripRequestsController.showRespondToRequest(true)
        tripRequestsViewModel.mIsRequestAccepted.observe(this, Observer {isAcceptedPressed->

          // If you pressed accepted
          if(isAcceptedPressed){

              if(tripRequestResponse == TripRequest.ACCEPTED) {
                  //layout_trip_details_driver_accept.visibility = View.GONE
                  //1. track the user
                  if(isRequestAcceptedReceived) {
                      Log.d("MainRequests","response: "+TripRequest.ACCEPTED)
                     // layout_trip_details_driver_accept.visibility = View.GONE




                      //2. draw the line from dr to ps
                      var passDestination = LatLng(
                          request.destination!!.latitude!!,
                          request.destination!!.longitude!!
                      )
                      // homeViewModel.mPassengerDestination.postValue(place.latLng!!)
                      homeViewModel.mPassengerDestination.postValue(passDestination)
                      driverPresenter.displayDriverUITrackUser(request)
                      isRequestAcceptedReceived = false
                  }


              }else{
                  refreshUI(true)
              }



          }

        })

    }


    private fun showResponseToRequestFCM(request: TripRequest, isAccepted:Boolean){
        //1. Need the token of the request
        //2. Using the token send the accept


        //  tripRequestsController.respondToRequest(mAuth?.uid!!,request,tripRequestResponse)
        tripRequestsController.showRespondToRequest(isAccepted)

        tripRequestsViewModel.mIsRequestAccepted.observe(this, Observer {isAcceptedObserved->

            // If you pressed accepted
            if(isAcceptedObserved){

                    //layout_trip_details_driver_accept.visibility = View.GONE
                    //1. track the user
                    if(isRequestAcceptedReceived) {
                        Log.d("MainRequests","response: "+TripRequest.ACCEPTED)
                        // layout_trip_details_driver_accept.visibility = View.GONE



                        //Load the details early
                        userMgtController.getPassengerDetails(request.passengerId!!)
                        userProfileViewModel.mGetPassengerDetails.observe(this, Observer { passenger->

                            val imageUri = passenger.passengerPicUrl
                            Picasso.with(baseContext).load(imageUri)
                                .fit().centerCrop()
                                .placeholder(AvatarGenerator.avatarImage(baseContext, 100, AvatarConstants.RECTANGLE, passenger.passengerFirstName!!))
                                //.error(R.drawable.user_placeholder_error)
                                .into(request_track_passenger_avator)

                            Log.d("DRIVER-REQUEST-GOT", "request status:" + request.requestStatus)


                            //DETAILS  ///////////////////////////////////////
                            request_track_passenger_name.text = request.passengerName

                            var locDes = Location(LocationManager.GPS_PROVIDER)
                            locDes.longitude = request.destination!!.longitude!!
                            locDes.latitude = request.destination!!.latitude!!

                            var locOrigin = Location(LocationManager.GPS_PROVIDER)
                            locOrigin.longitude = request.origin!!.longitude!!
                            locOrigin.latitude = request.origin!!.latitude!!

                            MapUtils(baseContext).reverseGeocoding(locDes,request_track_passenger_detail,true)
                            //  MapUtils(baseContext).reverseGeocoding(locOrigin,request_track_passenger_detail_pickup,true)
                            //request_track_passenger_detail_dropoff.text = locDes
                            //request_track_passenger_detail_pickup.text = locOr

                        })

                        //2. draw the line from dr to ps
                        var passDestination = LatLng(
                            request.destination!!.latitude!!,
                            request.destination!!.longitude!!
                        )


                            homeViewModel.mPassengerDestination.postValue(passDestination)
                            driverPresenter.displayDriverUITrackUser(request)

                            isRequestAcceptedReceived = false

                        // homeViewModel.mPassengerDestination.postValue(place.latLng!!)
                    }

            }else{
                refreshUI(true)
            }

        })

    }




    ///////  TRIP MANAGE REQUEST - ACCEPT OR REJECT ///////////////////////////////////////////////










    ///////  TRIP TRACK REQUEST ///////////////////////////////////////////////////////////////////

    private var UI_CONFIG = 0
    private var UI_SEARCH = 1
    private var UI_WAIT_START = 2
    private var UI_TRIP = 3
    private var UI_NO = 4

    private fun showDriverUI(UI:Int){
        if(UI_CONFIG==UI){

            prompt_driver_trip_start.visibility = View.GONE
            prompt_driver_trip_track_passenger.visibility = View.GONE
            prompt_driver_trip_config.visibility = View.VISIBLE
            prompt_driver_search.visibility = View.GONE
        }
        else if(UI_SEARCH==UI){

            prompt_driver_trip_start.visibility = View.GONE
            prompt_driver_trip_track_passenger.visibility = View.GONE
            prompt_driver_trip_config.visibility = View.GONE
            prompt_driver_search.visibility = View.VISIBLE


        }
        else if(UI_WAIT_START==UI){

            prompt_driver_trip_start.visibility = View.GONE
            prompt_driver_trip_track_passenger.visibility = View.VISIBLE
            prompt_driver_trip_config.visibility = View.GONE
            prompt_driver_search.visibility = View.GONE

        }
        else if(UI_TRIP==UI){

            prompt_driver_trip_start.visibility = View.VISIBLE

            prompt_driver_trip_track_passenger.visibility = View.GONE
            prompt_driver_trip_config.visibility = View.GONE
            prompt_driver_search.visibility = View.GONE

        }else{
            prompt_driver_trip_start.visibility = View.GONE
            prompt_driver_trip_track_passenger.visibility = View.GONE
            prompt_driver_trip_config.visibility = View.GONE
            prompt_driver_search.visibility = View.GONE
        }

    }

    var isTripStarted = 0

    override fun displayDriverUITrackUser(request: TripRequest) {

        sheetDriver.showBottomSheet(true,heightMedium)
        showDriverUI(UI_WAIT_START) //to track and start
        Log.d("MainRequests","trackuser: "+request.passengerName)



        var passName = request.passengerName
        var name = "PICK: $passName "


        // reverseGeocoding(locDes,label_request_track_passenger_subtitle)
        label_request_track_passenger_title.text = name

        button_driver_trip_join.isEnabled = false

        var passengerLocation = MapUtils.convertFromTripLocationToLocation(request.origin!!)

        TripManagementController().tripMgtRepo.isTripReached(request.tripId!!,passengerLocation, object :
            TripManagementRepository.TripReachedCallback {

            override fun onSuccess(info: String) {

                var pass = "Distance left: $info"
                label_trip_track_driver_wait_arrival_subtitle.text = pass

                progressBarRequestAccept.visibility = View.INVISIBLE
                layout_trip_details_driver_accept.visibility = View.GONE
                layout_trip_details_driver_start_wait.visibility = View.VISIBLE

                var run = Runnable {
                    button_driver_trip_join.isEnabled = true
                }
                MainThreadExecutor(6000).execute(run)

            }

            override fun onFailed(error: String) {

            }

        })


        button_driver_trip_join.setOnClickListener{

            isTripStarted = 1

            progressBarRequestAccept.visibility = View.VISIBLE
            button_driver_trip_join.isEnabled = false

            //Combine the locations object from passenger Origin to Destinatin
            var mapRoute  = MapRoute()

            mapRoute.tripId = request.tripId
            mapRoute.tripRequestId = request.requestId
            mapRoute.customerId = request.passengerName
            homeViewModel.setTrackDataForStartedTrip(mapRoute)

            //Observe and save the trip

        }

    }


    fun  observeDriverStartedTrip(){
        // IT COULD BE A DRIVER PASSENGER

        homeViewModel.startedTripRouteLiveData.observe(this, Observer {
            //   textView.text = it
            if(it!=null){

                // Log.d("HomeFragment-T", "OriginDestination true: ${it.driverName}")
                //set the destination received
                var locPassengerOrigin = MapUtils.convertFromLatLngTripLocation(it.origin!!)
                var locPassengerDes = MapUtils.convertFromLatLngTripLocation(it.destination!!)


                var startTrip = Trip.StartTrip()
                startTrip.tripId = it.tripId
                startTrip.distance = MapUtils.findDistance(it.origin!!, it.destination!!)

                startTrip.fare = "DEAFULT 3K"
                startTrip.tripStatus = Trip.StartTrip.TRIP_STARTED
                startTrip.tripOrigin = locPassengerOrigin
                startTrip.tripDestination = locPassengerDes
                startTrip.passengerName = it.customerId
                startTrip.passengerId = mAuth?.uid!!
                startTrip.requestId = it.tripRequestId

                //Get the Locations of the Trip
                MapUtils(this).getTripLocationsFromPolylines(it,object: MapUtils.TripRouteCallback{

                    override fun onSuccess(tripLocations: ArrayList<TripLocation>) {

                        startTrip.triproute = tripLocations
                        //First Start the trip then display the UI
                        TripManagementController().tripMgtRepo.startTrip(startTrip,object:
                            TripManagementRepository.StartTripCallback {

                            override fun onStarted(trip: Trip) {
                                if(isTripStarted == 1) {
                                    driverPresenter.displayDriverUIStartTrip(trip)
                                    isTripStarted = 0
                                }
                            }
                            override fun onFailed(error: String) {

                            }
                        })



                    }

                    override fun onFailed(error: String) {

                    }

                })

            }
        })


    }


    override fun displayDriverUIStartTrip(trip: Trip) {
        // update the ontrip:triproute, origin-destination

       // sheetDriver.showBottomSheet(true,heightMedium)
        showDriverUI(UI_TRIP) //to track and start
        showDriverUITripStarted(true,trip.tripId!!)


        isTripStarted = 0

        userProfileViewModel.mGetPassengerDetails.observe(this, Observer { passenger ->

            val imageUri = passenger.passengerPicUrl
            Picasso.with(baseContext).load(imageUri)
                .fit().centerCrop()
                .placeholder(
                    AvatarGenerator.avatarImage(
                        baseContext,
                        100,
                        AvatarConstants.RECTANGLE,
                        passenger.passengerFirstName!!
                    )
                )
                //.error(R.drawable.user_placeholder_error)
                .into(iv_manage_trip_passenger_avator)


            var locDes = Location(LocationManager.GPS_PROVIDER)
            locDes.longitude = trip.startedTrip?.tripDestination!!.longitude!!
            locDes.latitude = trip.startedTrip?.tripDestination!!.latitude!!

            var locOrigin = Location(LocationManager.GPS_PROVIDER)
            locOrigin.longitude =trip.startedTrip?.tripOrigin!!.longitude!!
            locOrigin.latitude = trip.startedTrip?.tripOrigin!!.latitude!!


            MapUtils(baseContext).reverseGeocoding(locDes,tv_driver_on_trip_details_dropoff,true)
            MapUtils(baseContext).reverseGeocoding(locOrigin,tv_driver_on_trip_details_pickup,true)

            tv_driver_on_trip_passenger_name.text =  trip.startedTrip?.passengerName
            // tv_driver_on_trip_passenger_subtitle.text = request.passengerId
            rating_bar_on_trip_passenger_rating.rating = 2F

            var shs = trip.startedTrip?.fare
            var km = trip.startedTrip?.distance.toString()+"KM - "+shs+"/="

            tv_driver_on_trip_distance.text = km

            button_driver_on_trip_end_trip.isEnabled = false
            button_driver_on_trip_end_trip.setOnClickListener{


              //  Toast.makeText(baseContext,"Trip End Presses",Toast.LENGTH_SHORT).show()

                //sheetDriver.showBottomSheet(true,heightLow)
                isTripEnded =1

                progressBarRequestEndTrip.visibility = View.VISIBLE
                button_driver_on_trip_end_trip.isEnabled = false
               // sheetDriver.showBottomSheet(false,heightLow)

                //endTrip(true,trip,trip.startedTrip?.requestId!!)

            }

            monitorEndedTrip(true,trip)


        })
    }

    var isTripEnded = 0


    fun showProcessPayment(isShow: Boolean){
        if(isShow) {
            layout_driver_on_trip_passenger_profile.visibility = View.GONE
            layout_started_trip_details_header.visibility = View.GONE
            progressBarEndTripByPassenger.visibility = View.VISIBLE
            val processP = "Processing payment"
            label_on_trip_pass.text = processP
        }else{
            layout_driver_on_trip_passenger_profile.visibility = View.VISIBLE
            layout_started_trip_details_header.visibility = View.VISIBLE

            layout_rate_trip_bypass.visibility = View.VISIBLE
            layout_end_trip_bypassenger.visibility = View.GONE
            progressBarEndTripByPassenger.visibility = View.GONE
            val thankyou = "Thank you for riding with us!"
            label_on_trip_pass.text = thankyou
        }
    }

    fun endTrip(isDriver: Boolean,trip:Trip, requestId: String){

       TripManagementController().tripMgtRepo.endTrip(trip,requestId,object: TripManagementRepository.endTripCallback {

            override fun onEnded() {

                //sheetDriver.showBottomSheet(false,heightLow)


                Toast.makeText(baseContext,"Thank you Ended",Toast.LENGTH_LONG).show()

                // PROCESS THE PAYMENT ///////////////////////////////////////////

                /////////////////////////////////////////////////////////////////



                showDriverUITripStarted(false,trip.tripId!!)


                    homeViewModel.mTripEnded.postValue(true)

                    Log.d("MainEnded","Ended")

                   // pref.putBoolPref(PreferenceHandler.KEY_IS_ON_TRIP,false)
                   // pref.clearPreference(PreferenceHandler.KEY_TRIP_ID)
                    pref.clear()
                    pref.setIntValue(PreferenceHandler.KEY_USER_SESSION,2)
                    MapUtils(baseContext).removePolylines()

                   // getTripSummaryDetails(trip.tripId!!)
            }

            override fun onFailed(error: String) {
                progressBarRequestEndTrip.visibility = View.GONE
                button_driver_on_trip_end_trip.isEnabled = true
            }

        })

    }


    private fun showDriverUITripStarted(isStarted: Boolean, tripId:String){
        if(isStarted){

            layout_trip_end_bar.visibility = View.VISIBLE
            layout_trip_complete_bar_rate.visibility = View.GONE

        }else{
            //sheetDriver.showBottomSheet(true,heightMedium)

            Log.d("MainEnded","Ended Show UI")


            //Show it again
            showDriverUI(UI_TRIP)
            progressBarRequestEndTrip.visibility = View.GONE

            layout_trip_complete_bar_rate.visibility = View.VISIBLE
            layout_trip_end_bar.visibility = View.GONE

            var thanks = "Thank you for riding with us"
            label_on_trip_.text = thanks

            btn_trip_submit_rating_driver.setOnClickListener{
                submitDriverRating(tripId, rating_bar_trip_receit_driver.rating.toString())
            }

        }

    }


    fun getTripSummaryDetails( tripId: String){
        TripManagementController().tripMgtRepo
            .getOnTripHistoryDetails(tripId,object: TripManagementRepository.OnTripHistoryDetailsCallback{

                override fun onTripHistoryReceived(tripHistory: TripHistory) {

                    displayDriverUIReceipt(tripHistory)

                }

                override fun onFailed(error: String) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }


            })

    }

    fun displayDriverUIReceipt(tripHistory: TripHistory) {
        // prompt_driver_trip_start.visibility = View.GONE
        // prompt_driver_trip_track_passenger.visibility = View.GONE
        // prompt_driver_trip_details.visibility = View.GONE
        // prompt_driver_search.visibility = View.GONE
        // prompt_driver_trip_receipt.visibility = View.VISIBLE

        //sheetDriver.toggleBottomSheet()
       // sheetDriver.showBottomSheet(false,heightLow)
       // sheetDriverReceipt.showBottomSheetReceit(true)



        //Fare
        var fare = "Sh "+tripHistory.fare
        tv_driver_ended_trip_amount.text = fare

        //Distance
        var distance = tripHistory.distance.toString()+" km"
        tv_driver_ended_trip_distance.text = distance

        //Rating label
        var passenger = "How was your trip with "+tripHistory.passengerName
        tv_driver_trip_receipt__rating_title.text = passenger

        //Date
        //"Sat Jul 17 11:16:20 GMT+03:00 2021"
        /*
        var dateSplit = tripHistory.dateOfTrip?.split(" ")
        var month = dateSplit?.get(1)
        var dateDay = dateSplit?.get(2)
        var year = dateSplit?.get(5)

        var mydate = "$month $dateDay, $year"
         */
        tv_driver_ended_trip_receipt_date.text = tripHistory.dateOfTrip


        //Locations
        var dest = Location(LocationManager.GPS_PROVIDER)
        dest.longitude = tripHistory.tripDestination!!.longitude!!
        dest.latitude = tripHistory.tripDestination!!.latitude!!

        var origin = Location(LocationManager.GPS_PROVIDER)
        origin.longitude = tripHistory.tripOrigin!!.longitude!!
        origin.latitude = tripHistory.tripOrigin!!.latitude!!

        MapUtils(this)
            .reverseGeocoding(dest,tv_driver_ended_trip_receipt_dropoff,true)
        MapUtils(this)
            .reverseGeocoding(origin,tv_driver_ended_trip_receipt_pickup,true)

        // btn_driver_on_trip_receipt_fare.text = fare

        btn_driver_ended_trip_submit_rating.setOnClickListener{

            submitDriverRating(tripHistory.tripId!!, rating_bar_driver_receipt.rating.toString())


        }
    }






    fun submitDriverRating(tripId: String,tripRating: String){

        btn_driver_ended_trip_submit_rating.isEnabled = false
        progressBarRequestEndTrip.visibility = View.VISIBLE


        var dMan = TripManagementController().tripMgtRepo

            dMan.submitRating(true,tripId, tripRating, object :
                TripManagementRepository.RatingTripCallback {

                override fun onSubmited() {


                    // homeViewModel.mTripEnded.postValue(true)
                    // prompt_driver_trip_receipt.visibility = View.GONE
                    // sheetDriver.toggleBottomSheet()

                  //  showDialogUI("Trip Details","Thank you for riding with us" )

                    /*
                   var label = "Thank you for riding with us!"
                   var snack = Snackbar.make(btn_driver_ended_trip_submit_rating, label, Snackbar.LENGTH_LONG)
                   //snack.anchorView = layoutBottomSheetDriver
                   snack.show()
                    */
                    MainThreadExecutor(2000).execute(Runnable {
                        progressBarRequestEndTrip.visibility = View.GONE
                        refreshUI(true)
                    })

                }

                override fun onFailed(error: String) {

                }
            })

    }



    private fun resetContent(sessionValue : Int){
        homeViewModel.mTripEnded.postValue(true)

        // pref.putBoolPref(PreferenceHandler.KEY_IS_ON_TRIP,false)
        // pref.clearPreference(PreferenceHandler.KEY_TRIP_ID)
        pref.clear()
        pref.setIntValue(PreferenceHandler.KEY_USER_SESSION,sessionValue)

        MapUtils(baseContext).removePolylines()
    }

    fun refreshUI(isDriver: Boolean){
       // var isReceipt = PreferenceHandler(baseContext).getBoolPref("isReceipt")
       // pref.isUserDriver()
        if(isDriver) {
            //sheetDriver.showBottomSheet(false,heightLow)

            resetContent(2)


            finish()

            val mainIntent = Intent(this,SplashActivity::class.java)
            val b = Bundle()
            b.putInt("key_session",2)
            mainIntent.putExtras(b)
            startActivity(mainIntent)

        }else{
            //Coming from the receipt

            resetContent(1)

            isOnTripStarted = 0 //hoping that when you finish this var is set back to zero

            finish()
            val mainIntent = Intent(this,SplashActivity::class.java)
            val b = Bundle()
            b.putInt("key_session",1)
            mainIntent.putExtras(b)
            startActivity(mainIntent)

        }

    }




    // var  slidingUpPanelLayout : SlidingUpPanelLayout  = findViewById(R.id.sliding_layout)
/*
    fun anchor(){
        if (sliding_layout.anchorPoint == 1.0f) {
            sliding_layout.anchorPoint = 0.7f;
            sliding_layout.panelState = SlidingUpPanelLayout.PanelState.EXPANDED;
           // item.setTitle(R.string.action_anchor_disable);
        } else {
            sliding_layout.anchorPoint = 1.0f;
            sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED;
            //item.setTitle(R.string.action_anchor_enable);
        }
    }


 */

    private fun onSlideListener(): SlidingUpPanelLayout.PanelSlideListener {


        return object : SlidingUpPanelLayout.PanelSlideListener {
            override fun onPanelSlide(view: View, v: Float) {
                // toast("panel is sliding")
            }

            override fun onPanelStateChanged(panel: View?, previousState: SlidingUpPanelLayout.PanelState?, newState: SlidingUpPanelLayout.PanelState?) {

            }


        }
    }






}








