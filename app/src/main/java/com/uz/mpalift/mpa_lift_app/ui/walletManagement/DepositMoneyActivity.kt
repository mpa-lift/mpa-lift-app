package com.uz.mpalift.mpa_lift_app.ui.walletManagement

import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.*
import android.text.style.StyleSpan
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import android.widget.Toast
import com.uz.mpalift.mpa_lift_app.module.walletManagement.ManageWalletRepository
import com.uzalz.mpa_lift_app.R
import kotlinx.android.synthetic.main.activity_wallet.*
import kotlinx.android.synthetic.main.content_wallet_deposit_money.*
import java.math.BigDecimal

class DepositMoneyActivity : AppCompatActivity() {


    val manageWalletRepository = ManageWalletRepository()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallet)

        var driverId = intent.extras?.getString("userId").toString()

        setUpToolBar("Wallet")
        textChange()

        liftsViewShowWalletBalance(driverId)


        button_start_deposit_money.setOnClickListener {

            layout_wallet_content_deposit_money.visibility = View.VISIBLE
            layout_wallet_content_transaction_history.visibility = View.GONE


        }

        button_deposit_money.setOnClickListener {
            var text = "In Progress"

            progressBarDepositMoney.visibility = View.VISIBLE
            button_deposit_money.text = text


            deposit(driverId)


        }



    }




    private fun setUpToolBar(channelName: String) {


        toolbarMoney.setNavigationOnClickListener{

            finish()

        }

        //    toolbar_main.logo = resources.getDrawable(R.drawable.ic_logo)
        // toolbar_main.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(toolbarMoney)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //  supportActionBar!!.setIcon(R.drawable.ic_logo)
        supportActionBar!!.setDisplayShowTitleEnabled(true)

        val channelNameSize = channelName.length

        val str = SpannableStringBuilder(channelName)
        str.setSpan(StyleSpan(Typeface.BOLD), 0, channelNameSize, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        toolbarMoney.title = str

        supportActionBar!!.title = str


        ///collapsingToolbarLayout.setExpandedTitleGravity(Gravity.BOTTOM);
        ///collapsingToolbarLayout.setExpandedTitleGravity(Gravity.BOTTOM);

        // collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
        // collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
        //  collapsingToolbarLayout.setCollapsedTitleGravity(Gravity.START)
        collapsing_toolbar_money.isTitleEnabled = false
        //   collapsingToolbarLayout.setTitle("VimsApp")

    }




    private fun liftsViewShowWalletBalance(id: String){

        manageWalletRepository.getWalletBalance(id,object: ManageWalletRepository.GetWalletBalanceCallback{

            override fun onGetWalletBalance(balance: BigDecimal) {

                var bal = balance.toString()

                tvAccountBalance.text = bal

            }

            override fun onFailed(error: String) {

            }

        })

    }


    fun refresh (){
        var text = "Deposit"

        progressBarDepositMoney.visibility = View.GONE
        button_deposit_money.text = text
        progressBarDepositMoney.visibility = View.GONE

        layout_wallet_content_deposit_money.visibility = View.GONE
        layout_wallet_content_transaction_history.visibility = View.VISIBLE

    }





    var operator : String ? = null
    fun textChange(){

        textInputRecipientName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.toString().trim { it <= ' ' }.isEmpty()) {
                    textInputRecipientName.error = "Field cannot be left blank."

                } else {
                    //    signUpViewModel.setName(charSequence)

                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })


        textInputRecipientNo.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.toString().trim { it <= ' ' }.isEmpty()) {
                    textInputRecipientNo.error = "Field cannot be left blank."

                } else {
                    //    signUpViewModel.setName(charSequence)

                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })


        textInputAmount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.toString().trim { it <= ' ' }.isEmpty()) {
                    textInputAmount.error = "Field cannot be left blank."

                } else {
                    //    signUpViewModel.setName(charSequence)

                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        var color = resources.getColor(R.color.grey_pallete_400)

        spinnerOperator.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val item = parent.selectedItem.toString()
                operator = item
                var spinnerCategoryText = parent.getChildAt(0) as TextView
                spinnerCategoryText.setTextColor(color)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }


    }


    private fun deposit(userId:String){

        // var name = textInputRecipientName.text.toString()
        //     val amount = textInputAmount.text.toString().toIntOrNull() ?: throw IllegalArgumentException("$textInputAmount.text.toString() is not a valid number")

        val balance = tvAccountBalance.text.toString().toDouble()
        val amount = textInputAmount.text.toString().toDouble()

        val total = balance + amount

        val amountBig = BigDecimal(total)

        manageWalletRepository.depositMoneyToAccount(userId,amountBig,object: ManageWalletRepository.DepositMoneyCallback{

            override fun onMoneyDeposited() {
                Toast.makeText(baseContext,"Your cash Deposit was successful",Toast.LENGTH_LONG).show()

               refresh()

            }

            override fun onFailed(error: String) {
                Toast.makeText(baseContext,error,Toast.LENGTH_LONG).show()
              //  finish()
            }

        })



    }



}
