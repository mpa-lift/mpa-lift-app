package com.uz.mpalift.mpa_lift_app.ui.userManagement

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.uz.mpalift.mpa_lift_app.module.entities.DrivingLicence
import com.uz.mpalift.mpa_lift_app.module.entities.MpaLiftUser
import com.uz.mpalift.mpa_lift_app.module.entities.TripVehicle

class UserProfileViewModel : ViewModel() {


    private var mError: MutableLiveData<String> = MutableLiveData()
    private var mProgess: MutableLiveData<Boolean> = MutableLiveData()
    private var mUserDetails: MutableLiveData<UserDetails> = MutableLiveData()

    val liveDataError: LiveData<String> = mError
    val liveDataProgress: LiveData<Boolean> = mProgess

    data class UserDetails(val userName:String,val userEmail:String, val userPhotoUrl:String)
    val liveDataUserDetails: LiveData<UserDetails> = mUserDetails


    fun setUserDetails(userName:String,userEmail:String,userPhotoUrl:String){
       var user = UserDetails(userName,userEmail,userPhotoUrl)
       mUserDetails.postValue(user)
    }

    fun setError(error:String){
        mError.postValue(error)
    }
    fun setProgress(isShow:Boolean){
        mProgess.postValue(isShow)
    }



    //USER DETAILS UI
    val mGetPassengerDetails = MutableLiveData<MpaLiftUser>()
    val mGetDriverDetails = MutableLiveData<DrivingLicence>()
    val mGetRegisteredVehicles = MutableLiveData<MutableList<TripVehicle>>()
    val mGetRegisteredVehiclesEmpty = MutableLiveData<String>()
    val mIsUserDriver = MutableLiveData<Boolean>()
    val mIsUserDriverError = MutableLiveData<String>()

    val mIsUserPasswordValid = MutableLiveData<Boolean>()
    val mIsUserPasswordValidError = MutableLiveData<String>()

    val mIsUserDriverLicenceRegistered = MutableLiveData<String>()
    val mIsUserDriverLicenceRegisteredError = MutableLiveData<String>()


    val mIsUserRegisteredForFCM = MutableLiveData<Boolean>()
    val mIsUserFCMError = MutableLiveData<String>()

}