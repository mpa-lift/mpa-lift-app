package com.uz.mpalift.mpa_lift_app.utils

import android.content.Context
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.uzalz.mpa_lift_app.R
import kotlinx.android.synthetic.main.item_request_driver.*

class DriverUISetup() {


    /////////////////// SETUP UI ELEMENTS //////////////////////////////////////////////////////////////



    companion object {

         lateinit var recycler : RecyclerView

         fun setUpRecyclerView(context: Context,recyclerView: RecyclerView) {
            recycler = recyclerView

            // val layoutManager = LinearLayoutManager(context)
            var gridColumns = 2

            val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            recycler.layoutManager = layoutManager


            //  decoration = DividerItemDecoration(getContext(), VERTICAL)

            var offset = 10
            // val end =  StartOffsetItemDecoration(offset)
            //recycler.addItemDecoration()

            //setting animation on scrolling
            recycler.setHasFixedSize(true); //enhance recycler view scroll
            recycler.isNestedScrollingEnabled = false;// enable smooth scrooll

        }

    }


}