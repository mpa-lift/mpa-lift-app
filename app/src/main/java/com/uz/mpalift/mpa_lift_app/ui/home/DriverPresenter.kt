package com.uz.mpalift.mpa_lift_app.ui.home

import android.location.Address
import com.google.android.gms.maps.model.LatLng
import com.uz.mpalift.mpa_lift_app.module.entities.Trip
import com.uz.mpalift.mpa_lift_app.module.entities.TripRequest
import com.uz.mpalift.mpa_lift_app.module.entities.TripVehicle
import com.uz.mpalift.mpa_lift_app.ui.home.DriverUI

class DriverPresenter(private var driverUI: DriverUI) {


    fun display1DriverConfigTrip(){
       // driverUI.display1ConfigTrip()
    }

    fun displayDriverUISearchDestination(){
       driverUI.displayDriverUISearchDestination()
    }
    fun displayDriverUICreateTrip(driverDestination: LatLng,address: String) {
      driverUI.displayDriverUICreateTrip(driverDestination,address)
    }
    fun displayUIDriverCars(liftResult: MutableList<TripVehicle>){
     //   driverUI.displayUIDriverCars(liftResult)
    }


    fun displayDriverUIManageRequest(request: TripRequest){
       // driverUI.displayDriverUIManageRequest(request)
    }

    fun displayDriverUITrackUser(request: TripRequest){
        driverUI.displayDriverUITrackUser(request)
    }

    fun displayDriverUIStartTrip(startTrip: Trip){
        driverUI.displayDriverUIStartTrip(startTrip)
    }


   // fun displayDriverUIReceipt(onTrip: OnTrip){

    ///    driverUI.displayDriverUIReceipt(onTrip)
    //}




}