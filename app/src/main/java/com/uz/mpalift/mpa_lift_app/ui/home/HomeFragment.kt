package com.uz.mpalift.mpa_lift_app.ui.home

import android.Manifest
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.firebase.geofire.GeoFire
import com.firebase.geofire.GeoLocation
import com.google.android.gms.common.api.Api
import com.google.android.gms.common.api.GoogleApi
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.uz.mpalift.mpa_lift_app.ui.main.MainThreadExecutor
import com.uz.mpalift.mpa_lift_app.module.DBConfig
import com.uz.mpalift.controllers.MapUtils
import com.uz.mpalift.controllers.userManagement.UserMgtController
import com.uz.mpalift.mpa_lift_app.module.AppExecutors
import com.uz.mpalift.mpa_lift_app.ui.tripRequests.TripRequestsPresenterImpl
import com.uz.mpalift.mpa_lift_app.ui.tripRequests.TripRequestsViewModel
import com.uz.mpalift.mpa_lift_app.ui.userManagement.UserProfilePresenter
import com.uz.mpalift.mpa_lift_app.ui.userManagement.UserProfileViewModel
import com.uz.mpalift.mpa_lift_app.utils.AppExecutorsUI
import com.uz.mpalift.mpa_lift_app.utils.PreferenceHandler
import com.uzalz.mpa_lift_app.R
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

//,OnConnectionFailedListener,LocationListener
class HomeFragment : Fragment(), OnMapReadyCallback,LocationListener{

    private lateinit var mGoogleMap: GoogleMap
    private lateinit var mGoogleApi: GoogleApi<Api.ApiOptions.NoOptions>
    private var mLocation : Location ? = null
    private lateinit var mLocationRequest: LocationRequest

    private val TAG = HomeFragment::class.java.simpleName
    private lateinit var appExecutors : AppExecutors



    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback



    private val REQUEST_LOCATION_PERMISSION = 1


    private lateinit var homeViewModel: HomeViewModel
    private lateinit var userProfileViewModel: UserProfileViewModel

    lateinit var userMgtController : UserMgtController
    private lateinit var userProfilePresenter: UserProfilePresenter




    lateinit var database : FirebaseDatabase
    var user : FirebaseUser? = null

    private var userValue  : Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        homeViewModel = ViewModelProvider(requireActivity()).get(HomeViewModel::class.java)
        userProfileViewModel = ViewModelProvider(requireActivity()).get(UserProfileViewModel::class.java)
        userProfilePresenter = UserProfilePresenter(userProfileViewModel)
        userMgtController = UserMgtController(userProfilePresenter)


       userValue = PreferenceHandler(requireContext()).getIntValue(PreferenceHandler.KEY_USER_SESSION)

       // observeToUpDateDriverLocationIfUserDriver()

        val root = inflater.inflate(R.layout.fragment_maps, container, false)

        user = FirebaseAuth.getInstance().currentUser
        database = FirebaseDatabase.getInstance()
        appExecutors = AppExecutors()


        // PART 2 - AFTER START OF LOCATION UPDATES - RECIEVE UPDATES - SAVE LOCATION TO VIEW MODEL
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations){
                    //// Update UI with location data ////
                    initMap(location)

                }
            }
        }


        setupMap()

        initFusedLocation()

        // PART 1 - GETTING LAST KNOWN LOCATION - START LOCATION UPDATES - SETTING CURRENT LOCATION
        getLastKnownLocation()


        //  PART 2 - GETTING PERIODIC UPDATES USING LAST KONWN LOCATION
        //  start periodic updates using the last location and check the callback
        startLocationUpdates()

        //we cannnot observe driver route unless its a driver
        observeRoutes()



        // ANIMATE LOCATION TRACKING
        // var mAndroidImageView : (ImageView) = root.findViewById(R.id.android_plain)
        // var mRotateAnim  =  AnimatorInflater.loadAnimator (requireContext(), R.animator.rotate);
        // mRotateAnim.setTarget(mAndroidImageView);


        // val mapFragment = requireFragmentManager().findFragmentById(R.id.map) as SupportMapFragment
        // mapFragment.getMapAsync(this)

        /*
        // val textView: TextView = root.findViewById(R.id.text_home)
           homeViewModel.text.observe(viewLifecycleOwner, Observer {
        // textView.text = it
        })
        */

        return root
    }





    private fun observeRoutes(){

        observerTripAndRefresh()

       if(userValue == 1){
           observeRouteFromPassengerOriginToDest()
           observeRouteFromDriverOriginToPassengerOrigin()
           observePassengerFinalTrip()

       }else if(userValue == 2){
           //setup trip observations
           observeDriverStartedTripRouteFromPointOfMeetingToDest()
           observeDriverTripCreationAndDrawPoylylines()

       }

    }

   // var isUserDriver = false

    private fun upDateDriverLocationIfUserDriver(isUserDriver:Int,location: Location){

        if(isUserDriver == 2){//driver
            var tripID = PreferenceHandler(requireContext()).getPref(PreferenceHandler.KEY_TRIP_ID)
            var isOnTrip = PreferenceHandler(requireContext()).getBoolPref(PreferenceHandler.KEY_IS_ON_TRIP)

            if (isOnTrip) {
                updateDriverLocation(tripID!!, location, DBConfig.refCarSupplyOnline)
            } else {
                if (tripID != null) { //Seeking the rider
                    updateDriverLocation(tripID, location, DBConfig.refCarSupplyOnline)
                }
            }


        }
    }

    private fun observeToUpDateDriverLocationIfUserDriver(){
        var pref = PreferenceHandler.getInstance(requireContext())
        userProfileViewModel.mIsUserDriver.observe(viewLifecycleOwner, Observer {isDriver->
           // isUserDriver = isDriver
        })


    }



    private fun setupMap() {
        (childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?)!!.getMapAsync(this)
    }

    private fun initFusedLocation(){

        fusedLocationClient =  LocationServices.getFusedLocationProviderClient(requireActivity())

    }


    private fun initMap(location: Location) {
        //After permission granted you initialise map

        val zoomLevel = 15f //how zoomed you want to be in the map//13
        val ltdlong = LatLng(location.latitude,location.longitude)

       // mGoogleMap.addMarker(MarkerOptions().position(ltdlong).title("Origin"))
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.latitude, location.longitude), zoomLevel))
        mGoogleMap.setPadding(0,0,0,64)//l,t,r,b
        setMapOverlay(mGoogleMap,ltdlong)
       // setMapDefaultMark(ltdlong,mGoogleMap)
        reverseGeocoding(location,mGoogleMap,BitmapDescriptorFactory.HUE_AZURE)


        Log.d("HomeFragment", "location UPDATES: "+location.latitude)
        val latLng = LatLng(location.latitude,location.longitude)


        Log.d("HomeFragment-NOW", "location UPDATES: "+location.latitude)
        homeViewModel.mPassengerOrigin.value = latLng
        Log.d("HomeFragment-NOW", "location UPDATES 1: "+homeViewModel.mPassengerOrigin.value)

        //update driver location if user driver

             var isTripActive = PreferenceHandler(requireContext()).getBoolPref(PreferenceHandler.KEY_IS_ON_TRIP)

        upDateDriverLocationIfUserDriver(userValue, location)

        //and set user is driver in preferences

    }

    private fun updateDriverLocation(tripID:String, location: Location, dbRef:String){

         var run = Runnable {

             var refCarSupply = database.reference.child(dbRef)
             //Store Driver Availability for tracking with the trip request id
             val geofireTripRequest = GeoFire(refCarSupply)
             geofireTripRequest.setLocation(tripID, GeoLocation(location.latitude,location.longitude))

         }
        appExecutors.networkIO().execute(run)

    }

    private fun getLastKnownLocation() {

        // PART 1 - GETTING LAST KNOWN LOCATION - START LOCATION UPDATES - SETTING CURRENT LOCATION
        // 0 it needs to get the location of the device at regular intervals.
        // As well as the geographical location (latitude and longitude), you may
        // want to give the user further information such as the bearing (horizontal direction of travel),
        // altitude, or velocity of the device. This information, and more, is available
        // in the Location object that your app can retrieve from the fused location provider

        val locationResult = fusedLocationClient.lastLocation



        locationResult.addOnCompleteListener(requireActivity()) { task ->
            if (task.isSuccessful) {
                // Set the map's camera position to the current location of the device.
                mLocation = task.result

                var run = Runnable {
                    if(mLocation!=null){

                        initMap(mLocation!!)

                    }else{
                        startLocationUpdates()
                    }
                }
                MainThreadExecutor(5000).execute(run)


               //  val ltdlong = LatLng(mLocation!!.latitude, mLocation!!.longitude)
               //  homeViewModel.mPassengerOrigin.value = ltdlong  //solve updates
               //  mGoogleMap.addMarker(MarkerOptions().position(ltdlong).title("Origin"))
               //  mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(mLocation!!.latitude, mLocation!!.longitude), zoomLevel))
                Log.d("HomeFragment-L", "Current location")
            } else {
                val zoomLevel = 13f //how zoomed you want to be in the map

                Log.d("HomeFragment-L", "Current location is null. Using defaults.")
                Log.e("HomeFragment-L", "Exception: %s", task.exception)
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mOrigin, zoomLevel))
                mGoogleMap.uiSettings?.isMyLocationButtonEnabled = false
            }
        }




        /*
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location : Location ->
                // Got last known location. In some rare situations this can be null.
                //loc turn off, factory reset, play services restart
                mLocation = location
                Log.d("HomeFragment-L", "Current location")
                initMap(mLocation!!)
                //reverseGeocoding(mLocation)
            }.addOnFailureListener {
                Log.d("HomeFragment-L", "Current location"+it.message)
            }

         */

        //PART 3 - Make the tracking state persistent



    }

    fun reverseGeocoding(location : Location,map:GoogleMap, bitmatFactoryMarkerColor: Float) {
        // Although latitude and longitude are useful for calculating distance or displaying a map position,
        // in many cases the address of the location is more useful.
        //but needs to be run in the background
        var runnable = Runnable {

            //Geocode and update on mainthread
            var geocoder = Geocoder(requireContext(), Locale.getDefault())

            var addresses : List<Address> ? = null
            var resultMessage = ""

            try {
                addresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    // In this sample, get just a single address
                    1)

                if (addresses == null || addresses.isEmpty()) {
                    if (resultMessage.isEmpty()) {
                        resultMessage = requireContext().getString(R.string.no_address_found);
                        Log.e(TAG, resultMessage)
                        Log.d("HomeFragment",resultMessage)
                    }
                }else {
                    Log.d("HomeFragment","result :no empty")
                    // If an address is found, read it into resultMessage
                    var address : Address  = addresses[0]
                    var addressParts = ArrayList<String>()
                    Log.d("HomeFragment","result: no empty: "+address.getAddressLine(0))

                    // Fetch the address lines using getAddressLine,
                    // join them, and send them to the thread
                    for (i in 0..address.maxAddressLineIndex) {
                        addressParts.add(address.getAddressLine(i))
                        Log.d("HomeFragment","result :address"+address.getAddressLine(i))
                    }
                    resultMessage = TextUtils.join("\n", addressParts)
                }


                appExecutors.mainThread().execute {
                    Log.d("HomeFragment","result :" +resultMessage)
                  //  Toast.makeText(requireContext(), resultMessage, Toast.LENGTH_LONG).show()

                    var ltdLong :LatLng = LatLng(location.latitude,location.longitude)
                    setMapDefaultMark(resultMessage,ltdLong,map,bitmatFactoryMarkerColor)

                }


            }catch (ioException: IOException) {
                // Catch network or other I/O problems
                resultMessage = requireContext().getString(R.string.service_not_available)
                Log.e(TAG, resultMessage, ioException)
            }
            catch (illegalArgumentException:IllegalArgumentException ) {
                // Catch invalid latitude or longitude values
                resultMessage = requireContext().getString(R.string.invalid_lat_long_used)
                Log.e(TAG, resultMessage + ". " + "Latitude = " + location.getLatitude() + ", Longitude = " + location.getLongitude(), illegalArgumentException);
            }

        }
        appExecutors.diskIO().execute(runnable)

    }

    private fun startLocationUpdates(){
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 1000  //millisecs
        mLocationRequest.fastestInterval = 1000
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        //start location updates
        fusedLocationClient.requestLocationUpdates(mLocationRequest, locationCallback,
            Looper.getMainLooper())

    }
    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }


    override fun onStart() {
        super.onStart()

     //   initFusedLocation()
        // PART 1 - GETTING LAST KNOWN LOCATION - START LOCATION UPDATES - SETTING CURRENT LOCATION
       // getLastKnownLocation()

    }


    override fun onResume() {
        super.onResume()
       // setupMap()
      //  super.onResume();
        startLocationUpdates()

        //if location updates are turned on
      //   if (requestingLocationUpdates) startLocationUpdates()
    }


    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }













    ///////////// GOOGLE MAP CALLBACKS AND SETTINGS  ///////////////////////////////////////////////
    var mOrigin =  LatLng(0.3506,32.6158)  //0.3506° N, 32.6158° E
    var mDestination = LatLng(0.3338,32.5684)  //0.3338° N, 32.5684° E



    override fun onMapReady(googleMap: GoogleMap) {
        mGoogleMap = googleMap


        val lat = 37.4219983
        val long = -122.084
        val latitude = 0.4479
        val longitude = 33.2026
        val latUg =  1.3733
        val lonUg = 32.2903

        // [0.4479° N, 33.2026° E]
        // [zoom levels]  1: World,  5: Landmass/continent, 10: City, 15: Streets, 20: Buildings
        val zoomLevel = 13f //how zoomed you want to be in the map
        // Add a marker in Sydney and move the camera
        val jinjaTown = LatLng(lat, long)

        //  mGoogleMap.addMarker(MarkerOptions().position(mOrigin).title("Origin"))
        //  mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel))
        //  Add a ground overlay 100 meters in width to the home location.



        val australiaBounds = LatLngBounds(
            LatLng((-44.0), 113.0),  // SW bounds
            LatLng((-10.0), 154.0) // NE bounds
        )
        //  mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(australiaBounds.center, 10f))
        //  Construct a CameraPosition focusing on Mountain View and animate the camera to that position.

        // SETTINGS
       // setMapLongClick(mGoogleMap)  //to pinch the map
       // setPoiClick(mGoogleMap)
        // setMapStyle(map)
        enableMyLocation() //enable loction layer
        mGoogleMap.uiSettings.isZoomControlsEnabled = true
        // mGoogleMap.isMyLocationEnabled = true
        mGoogleMap.uiSettings.isMyLocationButtonEnabled = true
    }


    override fun onLocationChanged(location: Location) {
        mGoogleMap.clear()

        val ltdlong = LatLng(location.latitude, location.longitude)

    }

    //ON MAP READY SETTINGS //////////////////////////////////////////////////////////////////

    private fun setMapDefaultMark(title:String,latLng : LatLng, map:GoogleMap, bitmapFactoryMarkerColor: Float){

        var arr = title.split(",")
        var snippetTItle = arr[0]+","+arr[1]

        val snippet = String.format(
            Locale.getDefault(),
            "Lat: %1$.5f, Long: %2$.5f",
            latLng.latitude,
            latLng.longitude
        )

        //BitmapDescriptorFactory.fromResource(R.drawable.ic_car)
        // icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
        //  .icon(BitmapDescriptorFactory.defaultMarker(bitmapFactoryMarkerColor))
        val poiMarker = map.addMarker(
            MarkerOptions()
                .position(latLng)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.defaultMarker(bitmapFactoryMarkerColor))
                .title(snippetTItle)
        )
        poiMarker.showInfoWindow()

    }

    private fun setMapLongClick(map:GoogleMap) {
        map.setOnMapLongClickListener {  latLng ->

            val snippet = String.format(
                Locale.getDefault(),
                "Lat: %1$.5f, Long: %2$.5f",
                latLng.latitude,
                latLng.longitude
            )

            //snippet adds desc of lat long
            map.addMarker(
                MarkerOptions()
                    .position(latLng)
                    .title(getString(R.string.dropped_pin))
                    .snippet(snippet)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))

            )

        }
    }

    /*
    By default, points of interest (POIs) appear on the map along with their corresponding icons.
    POIs include parks, schools, government buildings, and more. When the map type is set to normal, business
     POIs also appear on the map.
     Business POIs represent businesses such as shops, restaurants, and hotels.
     */

    private fun setPoiClick(map: GoogleMap) {
        map.setOnPoiClickListener { poi ->
            val poiMarker = map.addMarker(
                MarkerOptions()
                    .position(poi.latLng)
                    .title(poi.name)
            )
            poiMarker.showInfoWindow()
        }
    }


    private fun setMapOverlay(map: GoogleMap, home :LatLng ) {

        var float = 100.0F
        var homeOverlay = GroundOverlayOptions()
            .image(BitmapDescriptorFactory.fromResource(R.drawable.ic_sedan_car_front))
            .position(home, float)

      //  mGoogleMap.addGroundOverlay(homeOverlay);
    }

    ///////////////////////////////   LOCATION TRACKING  PERMISSIONS///////////////////////////////////

    private fun isPermissionGranted() : Boolean {
        return ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    private fun enableMyLocation() {

        if (isPermissionGranted()) {
            mGoogleMap.isMyLocationEnabled = true
            Log.d("HomeFragment-PERMISSION", "ENABLE true")
        }
        else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_LOCATION_PERMISSION
            )
            Log.d("HomeFragment-PERMISSION", "ENABLE try")
        } 

        if ( Build.VERSION.SDK_INT >= 23 &&
            ContextCompat.checkSelfPermission( requireContext(), android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission( requireContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
    }
    /*
    Override the onRequestPermissionsResult() method. If the requestCode is equal to REQUEST_LOCATION_PERMISSION
     permission is granted, and if the grantResults array is non-empty
    with PackageManager.PERMISSION_GRANTED in its first slot, call enableMyLocation():
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                enableMyLocation()
            }
        }
    }

    //////////////////////////  DIRECTIONS ?/////////////////////////////////////

    fun setupLocationSettings(){


        //  Track the device location using periodic location requests.
        //  Make the tracking state persistent.
        //  Show an animation to give a visual cue that the device location is being tracked.
        //  Check the device location settings. You need to know whether location services are turned on, and whether they are set to the accuracy that your app needs.

        //1. to use the Settings Client to check which settings are enabled, and present
        // the Location Settings dialog  for the user to update their settings with a single tap.

        //2. Configure location services
        //In order to use the location services provided by Google Play Services and the fused location
        // provider, connect your app using the Settings Client, then check the current location settings
        // and prompt the user to enable the required settings if needed. Apps whose features use
        // location services must request location permissions, depending on the use cases of those features.

        //3. Set up a location request
        // To store parameters for requests to the fused location provider, create a LocationRequest.

        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 1000  //millisecs
        mLocationRequest.fastestInterval = 1000
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        //4 Get current location settings
        // Once you have connected to Google Play services and the location services API,
        // you can get the current location settings of a user's device. To do this,
        // create a LocationSettingsRequest.Builder, and add one or more location requests.
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest)


        ///  Checking whether settings are satisfied   ////
        val client: SettingsClient = LocationServices.getSettingsClient(requireActivity())
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())


        // 6 When the Task completes, your app can check the location settings by looking
        // at the status code from the LocationSettingsResponse object. To get even more
        // details about the current state of the relevant location settings, your app
        // can call the LocationSettingsResponse object's getLocationSettingsStates() method.


        //7. Prompt user for location settings
        // To determine whether the location settings are appropriate for the location request,
        // add an OnFailureListener to the Task object that validates the location settings.
        // Then, check if the Exception object passed to the onFailure() method is an instance
        // of the ResolvableApiException class, which indicates that the settings must be changed.
        // Then, display a dialog that prompts the user for permission to modify the location settings
        // by calling startResolutionForResult().

        task.addOnSuccessListener { locationSettingsResponse ->
            // All location settings are satisfied. The client can initialize
            // location requests here.
            startLocationUpdates()


        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException){
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    exception.startResolutionForResult(requireActivity(), REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }


    }

    private var REQUEST_CHECK_SETTINGS = 1

    private lateinit var origin : String
    private  var dest : String ? = null















    /////////////  3.    OBSERVE ROUTES          ////////////////////////////////////////////////////

    fun decorateMapRoute(latLngOrigin: LatLng,latLngDest: LatLng){

        var locOrigin = MapUtils.convertFromLatLngToLocation(latLngOrigin)
        var locDestination = MapUtils.convertFromLatLngToLocation(latLngDest)

        //reverseGeocoding(locOrigin,mGoogleMap,BitmapDescriptorFactory.HUE_AZURE)
        reverseGeocoding(locDestination,mGoogleMap,BitmapDescriptorFactory.HUE_RED)

        /*
        val cameraPosition = CameraPosition.Builder()
            .target(latLngDest) // Sets the center of the map to Mountain View
            .zoom(15f)            // Sets the zoom
            .bearing(90f)         // Sets the orientation of the camera to east
            .tilt(30f)            // Sets the tilt of the camera to 30 degrees
            .build()              // Creates a CameraPosition from the builder
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

         */

        MapUtils(requireContext()).drawDirections(latLngOrigin,latLngDest,mGoogleMap, object : MapUtils.DirectionsCallback{

            override fun onDirectionsFound() {
               PreferenceHandler(requireContext()).putBoolPref(PreferenceHandler.KEY_IS_DIRECTIONS_FOUND,true)
            }

            override fun onDirectionsNotFound(msg: String) {
                PreferenceHandler(requireContext()).putBoolPref(PreferenceHandler.KEY_IS_DIRECTIONS_FOUND,false)
                Toast.makeText(requireContext(),msg,Toast.LENGTH_LONG).show()
            }

        })

    }


    fun observeRouteFromPassengerOriginToDest(){
        // IT COULD BE A DRIVER PASSENGER
        homeViewModel.passengerOrigin.observe(viewLifecycleOwner, Observer {
            //   textView.text = it
            origin = it.toString()
            Log.d("HomeFragment-O","Observe-Location-origin: "+origin)

        })
        homeViewModel.passengerDestination.observe(viewLifecycleOwner, Observer {
            //   textView.text = it
            dest = it.toString()
            Log.d("HomeFragment-O", "Observe-Location-dest: $dest")
            homeViewModel.setPassengerTripRoute()

        })

        homeViewModel.passengerMapRouteLiveData.observe(viewLifecycleOwner, Observer {
            //   textView.text = it
             if(it!=null){
                 Log.d("HomeFragment-T", "OriginDestination true: ${it.origin} :  ${it.destination}")
                 //set the destination received
                 mGoogleMap.clear()

                 decorateMapRoute(it.origin!!,it.destination!!)

                 stopLocationUpdates()

             }
        })


    }

    fun observeRouteFromDriverOriginToPassengerOrigin(){

        homeViewModel.driverToPassengerRouteLiveData.observe(viewLifecycleOwner, Observer {
            //   textView.text = it
            //  origin = it.toString()
           /// Log.d("HomeFragment-O","Observe-Location-origin: "+origin)

            //Draw trip route

            if(it!=null){

                Log.d("HomeFragment-T", "Passenger origin: ${it.origin}"+"Driver origin: ${it.destination}")
                mGoogleMap.clear()
                //set the destination received
                decorateMapRoute(it.origin!!,it.destination!!)


            }


        })


    }

    fun observePassengerFinalTrip(){

        homeViewModel.finalMapRouteLiveData.observe(viewLifecycleOwner, Observer {
            //   textView.text = it
            //   origin = it.toString()
            //   Log.d("HomeFragment-O","Observe-Location-origin: "+origin)

            //Draw trip route

            if(it!=null){

                Log.d("HomeFragment-T", "Passenger origin: ${it.origin}"+"Driver origin: ${it.destination}")
                mGoogleMap.clear()
                //set the destination received
                decorateMapRoute(it.origin!!,it.destination!!)

            }

        })

    }



    /////   OBSERVER DRIVER ROUTES //////////////////////////////////
    //observer driver Route
    fun observeDriverTripCreationAndDrawPoylylines(){
        // IT COULD BE A DRIVER PAASENGER

        homeViewModel.passengerOrigin.observe(viewLifecycleOwner, Observer {
            //   textView.text = it
            origin = it.toString()
        })

        homeViewModel.passengerDestination.observe(viewLifecycleOwner, Observer {
            //   textView.text = it
            dest = it.toString()

            // var car = DriverTrip.TripVehicle()
            //car.vehicleSeats = "3"
            //car.vehicleNoPlate = "UAB 123"
            //car.vehicleName = "Subaru"
            Log.d("HomeFragment-T", "Pass Destination observed")

         //   homeViewModel.setDriverTripRoute("2000",user!!.uid,user!!.phoneNumber!!)
        })

        homeViewModel.tripLiveData.observe(viewLifecycleOwner, Observer {
            //   textView.text = it
            if(it!=null){

                Log.d("HomeFragment-T", "OriginDestination true: ${it.driverName}")
                //set the destination received
                var tripOriginLat = it.tripOrigin!!.latitude
                var tripOriginLon = it.tripOrigin!!.longitude

                var tripDesLat = it.tripDestination!!.latitude
                var tripDestLon = it.tripDestination!!.longitude


                //convert the double to latlng for maps
                var locationOrigin =  LatLng(tripOriginLat!!,tripOriginLon!!)
                var locationDestination =  LatLng(tripDesLat!!,tripDestLon!!)


                Log.d("HomeFragment-T-Final", "Origin-Destination: ${locationOrigin} / ${locationDestination}")

                decorateMapRoute(locationOrigin,locationDestination)

                /*
                mGoogleMap.addMarker(MarkerOptions().position(locationOrigin).title("Origin"))
                mGoogleMap.addMarker(MarkerOptions().position(locationDestination).title("Destination"))
                //  mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel))
                val cameraPosition = CameraPosition.Builder()
                    .target(locationDestination) // Sets the center of the map to Mountain View
                    .zoom(25f)            // Sets the zoom
                    .bearing(90f)         // Sets the orientation of the camera to east
                    .tilt(30f)            // Sets the tilt of the camera to 30 degrees
                    .build()              // Creates a CameraPosition from the builder
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

                Log.d("HomeFragment-T", "AFTER GOOGLE MAPS")

                MapUtils(requireContext()).drawDirections(locationOrigin,locationDestination,mGoogleMap)

                 */



            }else{

            }
        })
    }

    //observer driver Route
    fun  observeDriverStartedTripRouteFromPointOfMeetingToDest(){
        // IT COULD BE A DRIVER PAASENGER

        homeViewModel.startedTripRouteLiveData.observe(viewLifecycleOwner, Observer {mapRoute ->
            //   textView.text = it
            if(mapRoute!=null){
              //  homeViewModel.startedTripRouteLiveData.removeObservers(this)

                decorateMapRoute(mapRoute.origin!!,mapRoute.destination!!)

                /*
                mGoogleMap.addMarker(MarkerOptions().position(mapRoute.origin!!).title("Origin"))
                mGoogleMap.addMarker(MarkerOptions().position(mapRoute.destination!!).title("Destination"))
                //  mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel))
                val cameraPosition = CameraPosition.Builder()
                    .target(mapRoute.destination!!) // Sets the center of the map to Mountain View
                    .zoom(25f)            // Sets the zoom
                    .bearing(90f)         // Sets the orientation of the camera to east
                    .tilt(30f)            // Sets the tilt of the camera to 30 degrees
                    .build()              // Creates a CameraPosition from the builder
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

                MapUtils(requireContext()).drawDirections(mapRoute.origin!!,mapRoute.destination!!,mGoogleMap)

                 */



            }
        })


    }

    //observer driver Route
    fun observerTripAndRefresh(){
        // IT COULD BE A DRIVER PASSENGER
        Log.d("HomeFragment-R"," REFRESH")

        homeViewModel.mTripEnded.observe(viewLifecycleOwner, Observer {isEnded->
            //   textView.text = it
            if (isEnded) {

                //mGoogleMap.clear()


                startLocationUpdates()

               // homeViewModel.startedTripRouteLiveData.removeObservers(viewLifecycleOwner)
               // homeViewModel.tripLiveData.removeObservers(viewLifecycleOwner)

                // clear the trip Id
                // PreferenceHandler(this).putPref(PreferenceHandler.KEY_TRIP_ID)
            }

        })

    }





}
