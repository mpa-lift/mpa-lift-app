package com.uz.mpalift.mpa_lift_app.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.text.TextUtils
import com.securepreferences.SecurePreferences

class PreferenceHandler(private val mContext: Context) {


        val sharedPreferencesInstance: SharedPreferences get() = SecurePreferences(mContext)

        fun putPref(key: String?, value: String?) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(mContext)
            val editor = prefs.edit()
            editor.putString(key, value)
            editor.apply()
        }

        fun getPref(key: String?): String? {
            val preferences =
                PreferenceManager.getDefaultSharedPreferences(mContext)
            return preferences.getString(key, null)
        }

        fun putLongPref(key: String?, value: Long) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(mContext)
            val editor = prefs.edit()
            editor.putLong(key, value)
            editor.apply()
        }

        fun getLongPref(key: String?): Long {
            val preferences =
                PreferenceManager.getDefaultSharedPreferences(mContext)
            return preferences.getLong(key, 0)
        }

        fun putBoolPref(key: String?, value: Boolean) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(mContext)
            val editor = prefs.edit()
            editor.putBoolean(key, value)
            editor.apply()
        }

        fun getBoolPref(key: String?): Boolean {
            val preferences =
                PreferenceManager.getDefaultSharedPreferences(mContext)
            return preferences.getBoolean(key, false)
        }



        fun setIntValue(key: String?, value: Int) {
            val prefs = PreferenceManager.getDefaultSharedPreferences(mContext)
            val editor = prefs.edit()
            editor.putInt(key, value)
            editor.apply()
        }

        fun getIntValue(key: String?): Int {
            val preferences =
                PreferenceManager.getDefaultSharedPreferences(mContext)
            return preferences.getInt(key, 0)
        }



    fun clear(){
        val preferences =
            PreferenceManager.getDefaultSharedPreferences(mContext)
        preferences.edit().clear().apply()
    }

    fun clearPreference(key:String){
        val preferences =
            PreferenceManager.getDefaultSharedPreferences(mContext)
        preferences.edit().remove(key).apply()
    }


    fun isUserDriver():Boolean{
       // var session = getPref(KEY_USER_SESSION)
        var isDriver = getIntValue(PreferenceHandler.KEY_USER_SESSION)
       // var isPass = PreferenceHandler.VALUE_USER_SESSION_TYPE_PASSENGER == session
        if(isDriver == 2) {
            return true
        }
        return false
    }
    fun isUserPassenger():Boolean{
        var session = getPref(KEY_USER_SESSION)
        var isPass = getIntValue(PreferenceHandler.KEY_USER_SESSION)
        if(isPass == 1) {
            return true
        }
        return false
    }

    fun isUserNull():Boolean{
        var session = getIntValue(KEY_USER_SESSION)
        if(session == 0) {

                return true
        }
        return false
        }

        companion object {
            const val KEY_USER_SESSION = "user_session"

            const val VALUE_USER_SESSION_TYPE_DRIVER = "driver"
            const val VALUE_USER_SESSION_TYPE_PASSENGER = "passenger"

            const val KEY_TRIP_ID = "trip_id"
            const val KEY_IS_ON_TRIP = "is_on_trip"
            const val KEY_IS_REGISTERED = "registered"

            const val KEY_IS_DIRECTIONS_FOUND = "isFoundDirections"

            const val KEY_MONITOR_REQUEST = "monitor_request"
            const val KEY_MONITOR_REQUEST_TRIP_ID = "monitor_request_trip_id"


            const val FCM_TOKEN = "fcm_token"



            private var INSTANCE: PreferenceHandler? = null
            fun getInstance(context: Context): PreferenceHandler? {
                if (INSTANCE == null) {
                    INSTANCE = PreferenceHandler(context)
                }
                return INSTANCE
            }

    }


}