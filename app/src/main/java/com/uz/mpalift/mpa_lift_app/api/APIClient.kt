package com.uz.mpalift.mpa_lift_app.api

import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

class APIClient {


    // private val ENDPOINT = "https://bpo-chatapp-api.herokuapp.com/api/";

    //  private val ENDPOINT = "http://10.0.2.2:8080/api/";
    private val ENDPOINT = "http://10.42.0.1:8183/"
    private val ENDPOINT_FCM = "https://fcm.googleapis.com/"


    private var retrofit: Retrofit? = null

    fun getRetrofitInstance(): Retrofit? {
        if (retrofit == null) { //to fake responses

            val okHttpClient = OkHttpClient().newBuilder()
                .connectTimeout(100, TimeUnit.SECONDS) //40
                .readTimeout(200, TimeUnit.SECONDS) //60
                .writeTimeout(200, TimeUnit.SECONDS) //90

                .addInterceptor(object : Interceptor { @Throws(IOException::class)
                override fun intercept(chain: Interceptor.Chain): Response? {
                    val request: Request = chain.request().newBuilder().addHeader("parameter", "value").build()
                    return chain.proceed(request)
                }
                })
                .build()


            val gson = GsonBuilder()
                .setLenient()
                .create()

            retrofit = Retrofit.Builder()
                .baseUrl(ENDPOINT_FCM)
                .client(okHttpClient)
                //.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        }
        return retrofit
    }

}