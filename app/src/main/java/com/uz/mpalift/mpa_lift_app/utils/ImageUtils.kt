package com.uz.mpalift.mpa_lift_app.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import androidx.core.graphics.drawable.RoundedBitmapDrawable
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory

class ImageUtils {


    companion object {
        fun makeDrawableCircular(context: Context, drawable: Int) {

            //R.drawable.ic_person_light_white_24dp
            var myBitmap =
                BitmapFactory.decodeResource(context.resources, drawable)

            var drawable: RoundedBitmapDrawable =
                RoundedBitmapDrawableFactory.create(context.resources, myBitmap)

            drawable.isCircular = true
            // detailBinding.materialupProfileImage.setImageDrawable(drawable);
        }

        fun makeBitmapCircular(context: Context, bitmap: Bitmap): Drawable {

            var drawable: RoundedBitmapDrawable =
                RoundedBitmapDrawableFactory.create(context.resources, bitmap)

            drawable.isCircular = true
            return drawable
            // detailBinding.materialupProfileImage.setImageDrawable(drawable);

        }


    }



}