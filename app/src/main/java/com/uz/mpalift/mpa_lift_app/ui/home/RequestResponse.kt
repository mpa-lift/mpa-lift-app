package com.uz.mpalift.mpa_lift_app.ui.home

import android.location.Location
import android.os.Parcel
import android.os.Parcelable
import com.uz.mpalift.mpa_lift_app.module.entities.TripLocation

class RequestResponse() :Parcelable {


    var responseType : String ? = null

    var tripId : String ? = null
    var userId : String ? = null
    var requestId : String ? = null
    var userName : String ? = null
    var calculatedFare : String? = null

    // For the Findermodel
    var vehicleName : String ? = null
    var vehicleSeats : String ? = null

    //For the request acceptance
    var requestDate : String ? = null
    var requestStatus : String ? = null


    var tripDriverOrigin : TripLocation? = null
    var tripDriverDestination : TripLocation? = null
    var tripDriverDropOff : TripLocation? = null

    constructor(parcel: Parcel) : this() {
        responseType = parcel.readString()
        tripId = parcel.readString()
        userId = parcel.readString()
        requestId = parcel.readString()
        userName = parcel.readString()
        calculatedFare = parcel.readString()
        vehicleName = parcel.readString()
        vehicleSeats = parcel.readString()
        requestDate = parcel.readString()
        requestStatus = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(responseType)
        parcel.writeString(tripId)
        parcel.writeString(userId)
        parcel.writeString(requestId)
        parcel.writeString(userName)
        parcel.writeString(calculatedFare)
        parcel.writeString(vehicleName)
        parcel.writeString(vehicleSeats)
        parcel.writeString(requestDate)
        parcel.writeString(requestStatus)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RequestResponse> {
        override fun createFromParcel(parcel: Parcel): RequestResponse {
            return RequestResponse(parcel)
        }

        override fun newArray(size: Int): Array<RequestResponse?> {
            return arrayOfNulls(size)
        }
    }


}