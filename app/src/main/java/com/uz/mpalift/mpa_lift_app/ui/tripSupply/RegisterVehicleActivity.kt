package com.uz.mpalift.mpa_lift_app.ui.tripSupply

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.widget.Toolbar
import com.uz.mpalift.mpa_lift_app.module.userManagement.UserRepository
import com.uz.mpalift.mpa_lift_app.module.entities.TripVehicle
import com.uzalz.mpa_lift_app.R
import kotlinx.android.synthetic.main.activity_register_vehicle.*

class RegisterVehicleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_vehicle)
        val toolbar: Toolbar = findViewById(R.id.toolbarRegisterCar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)



        textChange()

        button_add_vehicle.setOnClickListener {

            var driverId = intent.extras?.getString("driverId").toString()

            register(driverId)

        }


    }



    fun textChange(){

        vehicleNameInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.toString().trim { it <= ' ' }.isEmpty()) {
                    vehicleNameInput.error = "Field cannot be left blank."

                } else {
                //    signUpViewModel.setName(charSequence)

                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })


        noPlateInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.toString().trim { it <= ' ' }.isEmpty()) {
                    noPlateInput.error = "Field cannot be left blank."

                } else {
                    //    signUpViewModel.setName(charSequence)

                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })



    }


    private fun register(driverId: String){

        var vehicle = TripVehicle()
        vehicle.driverId = driverId
        vehicle.vehicleName = vehicleNameInput.text.toString()
        vehicle.vehicleNoPlate = noPlateInput.text.toString()
        vehicle.vehiclePhoto = "photo"
        vehicle.vehicleSeats = "5"

       var userRepository = UserRepository()


        userRepository.registerTripVehicle(vehicle, object: UserRepository.RegisterTripVehicleCallback{

            override fun onSuccess() {
              finish()
            }

            override fun onError(error: String) {


            }

        })

    }







}
