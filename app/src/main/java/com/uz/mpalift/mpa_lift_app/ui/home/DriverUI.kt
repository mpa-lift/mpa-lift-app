package com.uz.mpalift.mpa_lift_app.ui.home

import android.location.Address
import com.google.android.gms.maps.model.LatLng
import com.uz.mpalift.mpa_lift_app.module.entities.Trip
import com.uz.mpalift.mpa_lift_app.module.entities.TripRequest
import com.uz.mpalift.mpa_lift_app.module.entities.TripVehicle

interface DriverUI {

    //This interface is just for function organisation
    //The UI is to be updated using  livedata

    //fun display1DriverConfigTrip()
    //fun displayUIDriverCars(liftResult: MutableList<TripVehicle>)
    fun displayDriverUISearchDestination()
    fun displayDriverUICreateTrip(driverDestination: LatLng,address: String)



   // fun displayDriverUIManageRequest(request: TripRequest)  //receive,accept
    fun displayDriverUITrackUser(request: TripRequest)
    fun displayDriverUIStartTrip(trip: Trip)


    //fun displayDriverUIReceipt(onTrip: OnTrip)



    //fun displayDriverEventCreateTrip()
    //fun displayEventDriverArrival(arrival:String)


}