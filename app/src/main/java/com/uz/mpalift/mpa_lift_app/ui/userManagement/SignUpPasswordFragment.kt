package com.uz.mpalift.mpa_lift_app.ui.userManagement

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.ktx.Firebase
import com.uz.mpalift.mpa_lift_app.ui.home.MainActivity
import com.uzalz.mpa_lift_app.R
import kotlinx.android.synthetic.main.activity_sign_up.*


/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */

/*
class SignUpPasswordFragment : Fragment() {


    private lateinit var auth: FirebaseAuth


    lateinit var signUpViewModel: SignUpViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_signup_password, container, false)
        signUpViewModel = ViewModelProviders.of(requireActivity()).get(SignUpViewModel::class.java)
        // Initialize Firebase Auth
        auth = Firebase.auth
        return  v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        activity?.toolbar?.setNavigationOnClickListener{
           // findNavController().navigate(R.id.action_signUpPasswordFragment_to_EmailFragment)
        }


        passwordTextChange()
        observeFields()

        button_password_next.setOnClickListener{

            register()

        }

    }



    fun passwordTextChange(){

        passInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.toString().trim { it <= ' ' }.isEmpty()) {
                    passInput.error = "Field cannot be left blank."

                }else if(charSequence.toString().trim().length < 6){
                    passInput.error = "Password too short."

                } else {
                    signUpViewModel.setPassword(charSequence)
                }
            }

            override fun afterTextChanged(editable: Editable) {

            }
        })
    }

    var myName : String ? = null
    var myEmail: String ? = null
    var myPassword : String ? = null

    private fun observeFields(){
        signUpViewModel.emailM.observe(requireActivity(), Observer { email ->
          myEmail = email.toString()
            Toast.makeText(context,email,Toast.LENGTH_LONG).show()

            Log.d("Password",""+email)
        })
        signUpViewModel.nameM.observe(requireActivity(), Observer { name ->
          myName = name.toString()
        })
        signUpViewModel.passwordM.observe(requireActivity(), Observer { password ->
          myPassword = password.toString()

        })

    }





    private lateinit var functions: FirebaseFunctions


    private fun registerAPi(){
        //APIClient().getRetrofitInstance()!!.create(SignUpApi::class.java)
        //functions = Firebase.functions

    }

    private fun register() {

        if (myEmail != null  && myPassword != null) {

            auth.createUserWithEmailAndPassword(myEmail!!, myPassword!!)
                .addOnCompleteListener(requireActivity()) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        //    Log.d(TAG, "createUserWithEmail:success")


                        // Toast.makeText(this,"createUserWithEmail:success",Toast.LENGTH_LONG).show()

                        val user = auth.currentUser
                        //   updateUI(user)

                        val intent = Intent(context, MainActivity::class.java)
                        startActivity(intent)


                    } else {
                        // If sign in fails, display a message to the user.
                        //  Log.w(TAG, "createUserWithEmail:failure", task.exception)
                        Toast.makeText(context, "Authentication failed."+task.exception, Toast.LENGTH_SHORT).show()
                        // updateUI(null)
                    }

                }



        }
    }





}
*/
