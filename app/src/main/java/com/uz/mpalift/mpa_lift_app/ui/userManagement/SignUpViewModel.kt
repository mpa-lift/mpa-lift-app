package com.uz.mpalift.mpa_lift_app.ui.userManagement

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SignUpViewModel : ViewModel() {



    var emailM: MutableLiveData<CharSequence> =  MutableLiveData()
    var nameM: MutableLiveData<CharSequence> =  MutableLiveData()
    var lastNameM: MutableLiveData<CharSequence> = MutableLiveData()
    var cityM: MutableLiveData<CharSequence> = MutableLiveData()
    var passwordM: MutableLiveData<CharSequence> =  MutableLiveData()
    var phoneM: MutableLiveData<CharSequence> = MutableLiveData()




    fun setEmail(email:CharSequence){
       emailM.value = email
    }
    fun setPhone(phone:CharSequence){
        phoneM.value = phone
    }
    fun setName(name:CharSequence){
        nameM.value = name
    }
    fun setLastName(name:CharSequence){
        lastNameM.value = name
    }
    fun setCity(name:CharSequence){
        cityM.value = name
    }
    fun setPassword(pass:CharSequence){
        passwordM.value = pass
    }



    //SignUp
    var mSignUpIsShowProgressBar: MutableLiveData<Boolean> = MutableLiveData()
    var mSignUpWelcomeUser: MutableLiveData<String> = MutableLiveData()
    var mSignUpError: MutableLiveData<String> = MutableLiveData()





}