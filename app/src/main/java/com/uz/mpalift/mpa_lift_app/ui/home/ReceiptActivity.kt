package com.uz.mpalift.mpa_lift_app.ui.home

import android.location.Location
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.android.material.snackbar.Snackbar
import com.uz.mpalift.controllers.MapUtils
import com.uz.mpalift.controllers.TripManagementController
import com.uz.mpalift.mpa_lift_app.module.entities.TripHistory
import com.uz.mpalift.mpa_lift_app.module.tripMgt.TripManagementRepository
import com.uz.mpalift.mpa_lift_app.ui.main.MainThreadExecutor
import com.uz.mpalift.mpa_lift_app.utils.PreferenceHandler
import com.uzalz.mpa_lift_app.R
import kotlinx.android.synthetic.main.activity_get_started.*
import kotlinx.android.synthetic.main.activity_receipt.*
import kotlinx.android.synthetic.main.prompt_driver_ui_receipt.*

class ReceiptActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receipt)

        setUpToolBar()

        var tripId = intent.extras?.getString("tripId")
        getTripSummaryDetails(tripId!!)

    }




    private fun setUpToolBar() {

        //    toolbar_main.logo = resources.getDrawable(R.drawable.ic_logo)
        // toolbar_main.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(toolbar_trip_receipt)
        // supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //  supportActionBar!!.setIcon(R.drawable.ic_logo)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        toolbar_trip_receipt.setNavigationOnClickListener {
            finish()
        }

        /*
        val str = SpannableStringBuilder("Vacuum")
        str.setSpan(StyleSpan(Typeface.BOLD), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        toolbar.title = str
        supportActionBar!!.title = str
         */
    }



    fun getTripSummaryDetails( tripId: String){
        TripManagementController().tripMgtRepo
            .getOnTripHistoryDetails(tripId,object: TripManagementRepository.OnTripHistoryDetailsCallback{
                override fun onTripHistoryReceived(tripHistory: TripHistory) {
                    displayDriverUIReceipt(tripHistory)
                }
                override fun onFailed(error: String) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
            })
    }


    var x = 0
    fun displayDriverUIReceipt(tripHistory: TripHistory) {
        x++
        Log.d("CounterReceipt","$x")
        // prompt_driver_trip_start.visibility = View.GONE
        // prompt_driver_trip_track_passenger.visibility = View.GONE
        // prompt_driver_trip_details.visibility = View.GONE
        // prompt_driver_search.visibility = View.GONE
        // prompt_driver_trip_receipt.visibility = View.VISIBLE

        //  sheetDriver.toggleBottomSheet()
        //  sheetDriver.showBottomSheet(false,heightMedium)
        //  sheetDriverReceipt.showBottomSheetReceit(true)

        //Fare
        var fare = "Sh "+tripHistory.fare
        tv_driver_ended_trip_amount.text = fare

        //Distance
        var distance = tripHistory.distance.toString()+" km"
        tv_driver_ended_trip_distance.text = distance

        //Rating label
        var passenger = "How was your trip with "+tripHistory.passengerName
        tv_driver_trip_receipt__rating_title.text = passenger

        //Date
        //"Sat Jul 17 11:16:20 GMT+03:00 2021"
        /*
        var dateSplit = tripHistory.dateOfTrip?.split(" ")
        var month = dateSplit?.get(1)
        var dateDay = dateSplit?.get(2)
        var year = dateSplit?.get(5)

        var mydate = "$month $dateDay, $year"
         */
        tv_driver_ended_trip_receipt_date.text = tripHistory.dateOfTrip


        //Locations
        var dest = Location(LocationManager.GPS_PROVIDER)
        dest.longitude = tripHistory.tripDestination!!.longitude!!
        dest.latitude = tripHistory.tripDestination!!.latitude!!

        var origin = Location(LocationManager.GPS_PROVIDER)
        origin.longitude = tripHistory.tripOrigin!!.longitude!!
        origin.latitude = tripHistory.tripOrigin!!.latitude!!

        MapUtils(this)
            .reverseGeocoding(dest,tv_driver_ended_trip_receipt_dropoff,true)
        MapUtils(this)
            .reverseGeocoding(origin,tv_driver_ended_trip_receipt_pickup,true)

        // btn_driver_on_trip_receipt_fare.text = fare

        btn_driver_ended_trip_submit_rating.setOnClickListener{

            submitDriverRating(tripHistory.tripId!!, rating_bar_driver_receipt.rating.toString())

        }
    }




    fun submitDriverRating(tripId: String,tripRating: String){


        var dMan = TripManagementController().tripMgtRepo

        dMan.submitRating(true,tripId, tripRating, object :
            TripManagementRepository.RatingTripCallback {

            override fun onSubmited() {
                // homeViewModel.mTripEnded.postValue(true)
                // prompt_driver_trip_receipt.visibility = View.GONE
                // sheetDriver.toggleBottomSheet()

                var label = "Thank you for riding with us!"
                var snack = Snackbar.make(btn_driver_ended_trip_submit_rating, label, Snackbar.LENGTH_LONG)
                //snack.anchorView = layoutBottomSheetDriver
                snack.show()

                PreferenceHandler(baseContext).putBoolPref("isSubmitted",true)
                finish()


            }

            override fun onFailed(error: String) {

            }
        })

    }




}
