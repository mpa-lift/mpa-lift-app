package com.uz.mpalift.mpa_lift_app.ui.main

import android.app.Application
import com.google.firebase.FirebaseApp

@Suppress("unused")
class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        // KalturaOvpPlayer.initialize(this, PARTNER_ID, OVP_SERVER_URL)
        // KalturaOttPlayer.initialize(this, MainActivity.PARTNER_ID, MainActivity.SERVER_URL);

        FirebaseApp.initializeApp(this)
    }
}