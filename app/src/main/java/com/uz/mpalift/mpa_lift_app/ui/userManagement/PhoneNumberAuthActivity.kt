package com.uz.mpalift.mpa_lift_app.ui.userManagement

import androidx.appcompat.app.AppCompatActivity

class PhoneNumberAuthActivity : AppCompatActivity() {

    /*
        //get formatted number i.e "+1 469-664-1766"
        ccp.getFormattedFullNumber();

        //get unformatted number i.e. "14696641766"
        ccp,.getFullNumber();

        //get unformatted number with prefix "+" i.e "+14696641766"
        ccp.getFullNumberWithPlus();
        */


    /*

    private var verificationInProgress = false
    private var storedVerificationId: String? = ""
    private lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks


     private lateinit var auth: FirebaseAuth



    val phoneNum = "+16505554567"
    val testVerificationCode = "123456"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phone_number_auth)
        setUpToolBar()
        // Initialize Firebase Auth

        //auth = FirebaseAuth.getInstance()

         auth = Firebase.auth
        //  auth = FirebaseAuth.getInstance()

        toolbar_phone.setNavigationOnClickListener{
            finish()
        }

        if(auth.currentUser!= null){



        }


        buttonVerifyPhone.setOnClickListener{
            verifyPhoneNumber()

        }

        pin_view.addTextChangedListener(){
            if(pin_view.text.toString().length == 6 ){
                val code = pin_view.text.toString().trim()
                verifyPhoneNumberWithCode(code)
            }

        }
        buttonVerifyCode.setOnClickListener{

            val code = pin_view.text.toString().trim()
            if (code.isEmpty() || code.length < 6) {
             Toast.makeText(this,"Enter valid code", Toast.LENGTH_LONG).show()
            }

            verifyPhoneNumberWithCode(code)

        }



        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:$credential")


                showUI(false)


                //sometime the code is not detected automatically
                //in this case the code will be null
                //so user has to manually enter the code
                if (credential.smsCode != null) {
                    val code = credential.smsCode
                 //   editTextCode.setText(code)
                    //verifying the code
                    verifyPhoneNumberWithCode(code!!)
                }

            }

            override fun onVerificationFailed(e: FirebaseException) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e)

                Toast.makeText(baseContext, e.message, Toast.LENGTH_SHORT).show()

                if (e is FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // ...
                } else if (e is FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // ...
                }



                // Show a message and update the UI
                // ...
            }

            override fun onCodeSent(verificationId: String, token: PhoneAuthProvider.ForceResendingToken) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:$verificationId")
                Toast.makeText(baseContext, verificationId, Toast.LENGTH_SHORT).show()



                // Save verification ID and resending token so we can use them later
                storedVerificationId = verificationId
                resendToken = token

                showProgress(false)
                showUI(false)



                // ...
            }
        }
    }



    private fun setUpToolBar() {

        //    toolbar_main.logo = resources.getDrawable(R.drawable.ic_logo)
        // toolbar_main.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(toolbar_phone)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //  supportActionBar!!.setIcon(R.drawable.ic_logo)
        supportActionBar!!.setDisplayShowTitleEnabled(false)


        //val str = SpannableStringBuilder("Vacuum")
        //str.setSpan(StyleSpan(Typeface.BOLD), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        //toolbar.title = str

        //supportActionBar!!.title = str


    }



    private fun showProgress(isShowProgress:Boolean){
        if(isShowProgress){
            progressBar.visibility = View.VISIBLE
        }else{
            progressBar.visibility = View.INVISIBLE
        }
    }

    private fun verifyPhoneNumberWithCode(code: String) {
        showProgress(true)
        //called after code sent
        // [START verify_with_code]
          val credential = PhoneAuthProvider.getCredential(storedVerificationId!!, code)
         // [END verify_with_code]
          signInWithPhoneAuthCredential(credential)


        //// TESTING ///////////////////////////////////
       /// val intent = Intent(this, SignUpActivity::class.java)
        //startActivity(intent)
        //// TESTING ///////////////////////////////////

     //   finish()


    }




    private fun verifyPhoneNumber(){
        showProgress(true)
        //attach
        countryCodeHolder.registerCarrierNumberEditText(editText_carrierNumber)

       if(countryCodeHolder.isValidFullNumber) {
           ///////////////TESTING//////////////////////
           ////////////////////////////////////////////
           Toast.makeText(this,countryCodeHolder.formattedFullNumber, Toast.LENGTH_LONG).show()

           PhoneAuthProvider.getInstance().verifyPhoneNumber(
               countryCodeHolder.formattedFullNumber, // Phone number to verify
               60, // Timeout duration
               TimeUnit.SECONDS, // Unit of timeout
               this, // Activity (for callback binding)
               callbacks
           ) // OnVerificationStateChangedCallbacks

       }

    }



    private fun showUI(isShowPhoneView: Boolean){

         if(isShowPhoneView){
            layoutPhone.visibility = View.VISIBLE
             layoutCode.visibility = View.INVISIBLE
         }else{
             layoutPhone.visibility = View.INVISIBLE
             layoutCode.visibility = View.VISIBLE
         }

    }


    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")
                    showProgress(false)

                    val user = task.result?.user
                    Toast.makeText(this,user?.phoneNumber, Toast.LENGTH_LONG).show()

                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)

                    finish()
                    // ...
                } else {
                    // Sign in failed, display a message and update the UI
                    showProgress(false)
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        // The verification code entered was invalid
                        Toast.makeText(this,"Invalid code", Toast.LENGTH_LONG).show()
                    }
                }
            }
    }


    private fun signout(){

        Firebase.auth.signOut()
    }

    private fun createSignInIntent() {
        // [START auth_fui_create_intent]
        // Choose authentication providers
        val providers = arrayListOf(
            //AuthUI.IdpConfig.EmailBuilder().build(),
            AuthUI.IdpConfig.PhoneBuilder().build(),
            //  AuthUI.IdpConfig.FacebookBuilder().build(),
            //    AuthUI.IdpConfig.TwitterBuilder().build() ,
            AuthUI.IdpConfig.GoogleBuilder().build())


        // Create and launch sign-in intent
        startActivityForResult(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .setTheme(R.style.FullscreenTheme)
                .build(),
            RC_SIGN_IN
        )

        // [END auth_fui_create_intent]
    }


    companion object {
        private const val TAG = "PhoneAuthActivity"
        private const val RC_SIGN_IN = 123

        private const val KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress"
        private const val STATE_INITIALIZED = 1
        private const val STATE_VERIFY_FAILED = 3
        private const val STATE_VERIFY_SUCCESS = 4
        private const val STATE_CODE_SENT = 2
        private const val STATE_SIGNIN_FAILED = 5
        private const val STATE_SIGNIN_SUCCESS = 6
    }


*/
}
