package com.uz.mpalift.mpa_lift_app.ui.userManagement

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.avatarfirst.avatargenlib.AvatarConstants
import com.avatarfirst.avatargenlib.AvatarGenerator
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.makeramen.roundedimageview.RoundedTransformationBuilder
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation
import com.uz.mpalift.controllers.userManagement.UserMgtController
import com.uz.mpalift.mpa_lift_app.module.DBConfig
import com.uz.mpalift.mpa_lift_app.module.entities.DrivingLicence
import com.uz.mpalift.mpa_lift_app.module.userManagement.UserRepository
import com.uz.mpalift.mpa_lift_app.ui.home.MainActivity
import com.uz.mpalift.mpa_lift_app.ui.main.MainThreadExecutor
import com.uz.mpalift.mpa_lift_app.utils.DateCalendarConverter
import com.uz.mpalift.mpa_lift_app.utils.PreferenceHandler
import com.uz.mpalift.mpa_lift_app.utils.SecurityUtils
import com.uzalz.mpa_lift_app.R
import kotlinx.android.synthetic.main.content_sign_in_welcome_back.*
import kotlinx.android.synthetic.main.prompt_driver_ui_enter_destination.*
import java.util.*

class WelcomeBackActivity : AppCompatActivity() {




    private lateinit var userProfileViewModel: UserProfileViewModel
    private lateinit var userProfilePresenter: UserProfilePresenter
    lateinit var userMgtController : UserMgtController


    var userId : String ? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_back)

        //USER PROFILE
        userProfileViewModel = ViewModelProviders.of(this).get(UserProfileViewModel::class.java)
        userProfilePresenter = UserProfilePresenter(userProfileViewModel)
        userMgtController = UserMgtController(userProfilePresenter)


        userId = intent.extras?.getString("userId")


        passwordTextChange()
        button_welcome_back_complete.setOnClickListener {
            button_welcome_back_complete.startAnimation()
           // progressBarSignUpPassengerWelcome.visibility = View.VISIBLE

          //  frame_welcome_signup_pass.visibility = View.GONE
           userMgtController.isUserPasswordValid(DBConfig.user!!.uid,password!!)

        }
        observePassword()
        displayProfileDetailsForDriver()


    }




    var password : String ? = null
    fun passwordTextChange(){
        edittext_input_password_welcome_back.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.toString().trim { it <= ' ' }.isEmpty()) {
                    edittext_input_password_welcome_back.error = "Field cannot be left blank."
                }else if(charSequence.toString().trim().length < 6){
                    edittext_input_password_welcome_back.error = "Password too short."
                } else {
                    var pass= SecurityUtils.encryptPassword(charSequence.toString())
                    password = charSequence.toString()
                }
            }
            override fun afterTextChanged(editable: Editable) {
            }
        })
    }


    private fun displayError(title: String, msg: String) {
        val mAlertDialog: AlertDialog =
            AlertDialog.Builder(this, R.style.Theme_AppCompat_Dialog_Alert)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton("Adjust Date",
                    DialogInterface.OnClickListener { dialog, which ->

                       // startActivityForResult(Intent(Settings.ACTION_DATE_SETTINGS), 0)
                       // finish()

                    })
                .setNegativeButton("Cancel",
                    DialogInterface.OnClickListener { dialog, which -> finish() })
                .show()
    }


    fun displayChooseRole(title: String){

        val items = arrayOf("Driver", "Passenger")

        //is persin already a driver


        MaterialAlertDialogBuilder(this)
            .setTitle(title)
            .setIcon(resources.getDrawable(R.drawable.ic_person_black_24dp))
            .setItems(items) { dialog, which ->
                // Respond to item chosen
                when (which) {
                    0 -> { /* horse */

                        UserRepository().isUserDriver(userId!!,object: UserRepository.isUserDriverCallback{

                            override fun isDriver(boolean: Boolean) {
                                if(boolean){
                                    //   frame_welcome_signup_pass.visibility = View.GONE
                                       frame_welcome_continue.visibility = View.GONE
                                    openHomeScreen(true)


                                }else{
                                    // register driver
                                    //  openHomeScreen(false)
                                   // Toast.makeText(baseContext,"You need to register as driver",Toast.LENGTH_LONG).show()

                                    var reg = DrivingLicence()
                                    reg.dateIssued = DateCalendarConverter.dateToString(Date())
                                    reg.userId = DBConfig.user!!.uid
                                    reg.licenceId = UUID.randomUUID().toString()

                                    userMgtController.registerAsDriver(reg)

                                    var snack = Snackbar.make(frame_welcome_signup_pass, "Activating your licence...", Snackbar.LENGTH_LONG)
                                    snack.show()
                                    observeLicenceRegistration()

                                }
                            }

                            override fun onError(error: String) {
                                var label = "net: "+error
                                var snack = Snackbar.make(layout_text_driver_create_trip, label, Snackbar.LENGTH_LONG)
                                // snack.anchorView = layoutBottomSheetDriver
                                snack.show()


                            }

                        })

                    }
                    1 -> { /* cow   */

                        openHomeScreen(false)
                    }
                }
            }
            .show()

    }



    fun observePassword(){
        userProfileViewModel.mIsUserPasswordValid.observe(this, Observer {isValid->
            Log.d("UserPass","observed")
            if(isValid){

                button_welcome_back_complete.stopAnimation()
                button_welcome_back_complete.revertAnimation()
                var continueAs = "Continue as"
                tv_welcome_continue.text = continueAs

               // displayError("ContinueAs","Driver")
                displayChooseRole("Continue As")

               //   frame_welcome_signup_pass.visibility = View.GONE
               //   frame_welcome_continue.visibility = View.VISIBLE

            }
            else{
                button_welcome_back_complete.stopAnimation()
                frame_welcome_signup_pass.visibility = View.VISIBLE
                button_welcome_back_complete.stopAnimation()
                button_welcome_back_complete.revertAnimation()

                var label = "Incorrect password!!"
                var snack = Snackbar.make(frame_welcome_signup_pass, label, Snackbar.LENGTH_LONG)
               // snack.anchorView = layoutBottomSheetDriver
                snack.show()
            }
        })
        userProfileViewModel.mIsUserPasswordValidError.observe(this, Observer {error->
                Toast.makeText(this,error,Toast.LENGTH_LONG).show()
        })
    }




    private fun openHomeScreen(isDriver: Boolean){

        button_welcome_back_complete.stopAnimation()

        var pref = PreferenceHandler(this)

        if(isDriver){
            pref.setIntValue(PreferenceHandler.KEY_USER_SESSION, 2)
        }else{
            pref.setIntValue(PreferenceHandler.KEY_USER_SESSION, 1)
        }

        Log.d("USER_LIFT_TYPE_WEL","user?"+pref.isUserDriver())

        val intent = Intent(baseContext, MainActivity::class.java)
        startActivity(intent)
        finish()

    }







    private fun observeLicenceRegistration(){
        userProfileViewModel.mIsUserDriverLicenceRegistered.observe(this, Observer{

            MainThreadExecutor(2000).execute(Runnable {

                var label = it
                var snack = Snackbar.make(frame_welcome_signup_pass, label, Snackbar.LENGTH_LONG)
                snack.show()
                openHomeScreen(true)

            })


        })

    }

    private fun displayProfileDetailsForDriver(){

        userMgtController.getUserProfile(userId!!)

            userProfileViewModel.liveDataUserDetails.observe(this,Observer{userDetails->

                val transformation: Transformation = RoundedTransformationBuilder()
                    .borderColor(Color.BLACK)
                    .borderWidthDp(0.5f)
                    .cornerRadiusDp(5f)
                    .oval(false)
                    .build()

                Picasso.with(baseContext).load(userDetails.userPhotoUrl)
                    .fit().centerInside()
                    .transform(transformation)
                    .placeholder(AvatarGenerator.avatarImage(baseContext, 100, AvatarConstants.CIRCLE, userDetails.userName))
                    //.error(R.drawable.user_placeholder_error)
                    .into(iv_pic_welcome_back)

                tv_welcome_name.text = userDetails.userEmail
                //tv_profile_username.text = userDetails.userName

            })

        }

    }



