package com.uz.mpalift.mpa_lift_app.ui.tripFinder

import com.uz.mpalift.controllers.tripFinder.TripFinderPort
import com.uz.mpalift.controllers.tripFinder.TripFinderViewModel

import com.uz.mpalift.mpa_lift_app.module.entities.Trip

class TripFinderPresenter(var tripFinderVModel: TripFinderVModel) : TripFinderPort{

    override fun presentAvailableTrips(trips: ArrayList<TripFinderViewModel>) {

       tripFinderVModel.listOfLifts.postValue(trips)

    }

    override fun showLoadError(error: String) {
        tripFinderVModel.listOfLiftsError.postValue(error)

    }

    override fun isShowProgress(isShow: Boolean) {

    }


}