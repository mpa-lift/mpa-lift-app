package com.uz.mpalift.mpa_lift_app.ui.home

import android.util.Log
import androidx.lifecycle.*
import com.google.android.gms.maps.model.LatLng
import com.uz.mpalift.controllers.MapRoute
import com.uz.mpalift.controllers.MapUtils
import com.uz.mpalift.mpa_lift_app.module.entities.*

class HomeViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply { value = "This is home Fragment" }
    val text: LiveData<String> = _text


    //Location Data for passenger can change
    val mPassengerOrigin = MutableLiveData<LatLng>()
    val passengerOrigin : LiveData<LatLng> = mPassengerOrigin

    val mPassengerDestination = MutableLiveData<LatLng>()
    val mPassengerDestinationNewData = MutableLiveData<LatLng>()
    //val passengerDestination : LiveData<LatLng> = mPassengerDestination


    var passengerDestination = Transformations.switchMap(mPassengerDestination){ destination ->
        //destination is the new value inside the mutable livedata
         getData(destination)

    }  // Returns LiveData
    fun getData(x: LatLng): LiveData<LatLng>{
        mPassengerDestinationNewData.postValue(x)
         return mPassengerDestinationNewData
    }




    //This is the data of origin and destination for the passenger
    private val mPassengerTripData = MutableLiveData<MapRoute>()
     fun setPassengerTripRoute(){
         if(mPassengerOrigin.value!= null && mPassengerDestination.value!=null){
             var trip = MapRoute()
             trip.origin = mPassengerOrigin.value
             trip.destination = mPassengerDestination.value

             mPassengerTripData.value = trip
         }

     }

     val passengerMapRouteLiveData : LiveData<MapRoute> = mPassengerTripData





    // var mTripVehicle = MutableLiveData<DriverTrip.TripVehicle>()

    var mTripEnded = MutableLiveData<Boolean>()


    // This is the data of origin and destination for the passenger
    private val mDriverTripData = MutableLiveData<Trip>()
    fun setDriverTripRoute(fare: String, driverId: String, driverDetailsName: String, driverDetailsPhotoUrl:String, vehichle: TripVehicle){

        if(mPassengerOrigin.value!= null && mPassengerDestination.value!=null){

            Log.d("HomeFragment-T-NotNull", "OriginANDDestination D NULL: "+mPassengerDestination.value)
            Log.d("HomeFragment-T-NotNull", "OriginANDDestination O NULL: "+mPassengerOrigin.value)

            var trip = Trip()

            var locPassengerOrigin = TripLocation()
            locPassengerOrigin.latitude = mPassengerOrigin.value!!.latitude
            locPassengerOrigin.longitude = mPassengerOrigin.value!!.longitude
            //  locPassengerOrigin.setLatLng(mPassengerOrigin.value)

            var locPassengerDes = TripLocation()
            locPassengerDes.latitude = mPassengerDestination.value!!.latitude
            locPassengerDes.longitude = mPassengerDestination.value!!.longitude
            //locPassengerDes.setLatLng(mPassengerDestination.value)

            trip.tripOrigin = locPassengerOrigin
            trip.tripDestination = locPassengerDes
            /*
            trip.fare = MapUtils.findFareByDistance(
                mPassengerOrigin.value!!,
                mPassengerDestination.value!!
            ).toString()

             */

            var distanceKm = MapUtils.findDistance(MapUtils.convertFromTripLocationToLatLng(locPassengerOrigin), MapUtils.convertFromTripLocationToLatLng(
                locPassengerDes
            ))

            /*
            var distance = MapUtils.convertFromTripLocationToLocation(locPassengerOrigin).distanceTo(MapUtils.convertFromTripLocationToLocation(
                locPassengerDes
            ))
             */

            trip.distance = distanceKm.toFloat()
            /*
            trip.distance = MapUtils.findDistance(MapUtils.convertFromTripLocationToLocation(locPassengerOrigin),
                    MapUtils.convertFromTripLocationToLocation(
                        locPassengerDes
                    )
                )
             */

            //  trip.tripvehicle = mTripVehicle.value
            trip.tripvehicle = vehichle

            trip.driverName = driverDetailsName
            trip.driverPicUrl = driverDetailsPhotoUrl
            trip.driverId = driverId

            mDriverTripData.postValue(trip)
        }else{

            Log.d("HomeFragment-T-Er", "OriginANDDestination NULL: ")
            Log.d("HomeFragment-T-Er", "OriginANDDestination D NULL: "+mPassengerDestination.value)
            Log.d("HomeFragment-T-Er", "OriginANDDestination O NULL: "+mPassengerOrigin.value)

        }
    }
    val tripLiveData : LiveData<Trip> = mDriverTripData









    // This is the data of route from origin of driver to origin for the passenger
    // it is used to draw a line of path frm the driver to the passenger
    var mDriverOrigin = MutableLiveData<LatLng>()
    private var driverToPassengerRoute = MutableLiveData<MapRoute>()

    fun setTrackDataFromDriverToPassenger(){
        if(mDriverOrigin.value!= null && mPassengerOrigin.value!=null){
            var tracktrip = MapRoute()
            tracktrip.origin = mDriverOrigin.value
            tracktrip.destination =  mPassengerOrigin.value
            Log.d("ORIGNS1", "NOT null: "+mDriverOrigin.value)
            Log.d("ORIGNS2", "NOT null: "+mPassengerOrigin.value)
            driverToPassengerRoute.value = tracktrip
        }else{
            Log.d("ORIGNS1", "null: "+mDriverOrigin.value)
            Log.d("ORIGNS2", "null: "+mPassengerOrigin.value)
        }
    }
    val driverToPassengerRouteLiveData : LiveData<MapRoute> = driverToPassengerRoute



    // This is the data of route from origin of passenger to destination for the driver
    // it is used to draw a line of path frm the passenger's location to where the driver is going(destination)
    var mPassengerTripOrigin = MutableLiveData<LatLng>()
    var mDriverTripDestination = MutableLiveData<LatLng>()
    private var finalTripRoute = MutableLiveData<MapRoute>()

    fun setTrackDataForFinalTrip(mPassengerTripOrigin : LatLng, mDriverTripDestination:LatLng){
        //  if(mPassengerTripOrigin!= null && mDriverTripDestination.value!=null){
            Log.d("TrackData","x:"+mPassengerTripOrigin+" y:"+mDriverTripDestination)
            var tracktrip = MapRoute()
            tracktrip.origin = mPassengerTripOrigin
            tracktrip.destination =  mDriverTripDestination

            finalTripRoute.value = tracktrip
        // }
    }
    val finalMapRouteLiveData : LiveData<MapRoute> = finalTripRoute






    //Taking the passenger to his/her dest
    // This is the data of route from origin of passenger to destination for the passenger
    // it is used to draw a line of path frm the pass to pass
    private val startedTripRoute = MutableLiveData<MapRoute>()

    fun setTrackDataForStartedTrip(mapRoute: MapRoute){
        if(mPassengerOrigin.value!= null && mPassengerDestination.value!=null){

            mapRoute.origin = mPassengerOrigin.value
            mapRoute.destination = mPassengerDestination.value

            startedTripRoute.value = mapRoute
        }

    }
    val startedTripRouteLiveData : LiveData<MapRoute> = startedTripRoute





}