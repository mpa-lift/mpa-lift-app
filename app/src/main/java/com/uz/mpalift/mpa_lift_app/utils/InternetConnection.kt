package com.uz.mpalift.mpa_lift_app.utils

import java.net.InetAddress
import java.net.UnknownHostException
import java.util.concurrent.*


class InternetConnection {


    private fun internetConnectionAvailable(timeOut: Int): Boolean {
        var inetAddress: InetAddress? = null
        try {
            val future: Future<InetAddress?> =
                Executors.newSingleThreadExecutor().submit(Callable<InetAddress?> {
                    try {
                        InetAddress.getByName("google.com")
                    } catch (e: UnknownHostException) {
                        null
                    }
                })
            inetAddress = future.get(timeOut.toLong(), TimeUnit.MILLISECONDS)
            future.cancel(true)
        } catch (e: InterruptedException) {
        } catch (e: ExecutionException) {
        } catch (e: TimeoutException) {
        }
        return inetAddress != null && !inetAddress.equals("")
    }



}