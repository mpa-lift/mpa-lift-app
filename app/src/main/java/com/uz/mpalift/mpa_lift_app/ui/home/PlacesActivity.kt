package com.uz.mpalift.mpa_lift_app.ui.home



import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.RectangularBounds
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.uzalz.mpa_lift_app.R
import java.util.*


class PlacesActivity : AppCompatActivity() {

    private lateinit var mPlacesClient: PlacesClient


    ///////////////////////////////////////////////
       //-- programatically
    private lateinit var queryText: EditText
    private lateinit var mSearchButton: Button
    private lateinit var mSearchResult: TextView
    private lateinit var mResult: StringBuilder

    ////////////////////////////////////////////////

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_places)

        // Initialize the Places client
        val apiKey = getString(R.string.maps_api_key)
        Places.initialize(applicationContext, apiKey)
        mPlacesClient = Places.createClient(this)

        fragmentPlaces()



    }


    private fun customPlaces(){

        /////////////////    CUSTOM  ///////////////////////////////////////
       //  queryText = findViewById(R.id.inputEditText)
        // mSearchButton = findViewById(R.id.searchButton)
        // mSearchResult = findViewById(R.id.searchResult)


        mSearchButton.setOnClickListener {
            Log.d("PLACES_ACTIVITY", "onclick")
            //    Toast.makeText(PlacePredictionProgrammatically.this, queryText.getText().toString(), Toast.LENGTH_SHORT).show();
            // Create a new token for the autocomplete session. Pass this to FindAutocompletePredictionsRequest,
            // and once again when the user makes a selection (for example when calling fetchPlace()).
            var token = AutocompleteSessionToken.newInstance()
            // Create a RectangularBounds object.
            //dummy lat/lng
            var bounds = RectangularBounds.newInstance(
                LatLng(-33.880490, 151.184363),
                LatLng(-33.858754, 151.229596)
            )
            // Use the builder to create a FindAutocompletePredictionsRequest.
            var request = FindAutocompletePredictionsRequest.builder()
                // Call either setLocationBias() OR setLocationRestriction().
                .setLocationBias(bounds)
                //.setLocationRestriction(bounds)
                .setCountry("us")//Nigeria
                .setTypeFilter(TypeFilter.ADDRESS)
                .setSessionToken(token)
                .setQuery(queryText.text.toString())
                .build()


            mPlacesClient.findAutocompletePredictions(request)
                .addOnSuccessListener { response: FindAutocompletePredictionsResponse ->
                    Log.d("PLACES_ACTIVITY", "onSuccess")
                    mResult = StringBuilder()

                    for (prediction in response.autocompletePredictions) {
                        Log.d("PLACES_ACTIVITY", prediction.placeId)
                        //  Log.i(TAG, prediction.getPrimaryText(null).toString())
                    }
                    mSearchResult.setText(mResult.toString())

                }.addOnFailureListener { exception: Exception? ->
                    Log.d("PLACES_ACTIVITY", "onError")
                    if (exception is ApiException) {
                        // Log.e(TAG, "Place not found: " + exception.statusCode)
                        Log.d("PLACES_ACTIVITY", "Place not found:" + exception.message)
                    }
                }
            ///////////////////////////////////////////////////////////////


        }
        }

    fun fragmentPlaces(){

        // Initialize the AutocompleteSupportFragment.
        val autocompleteFragment =
            supportFragmentManager.findFragmentById(R.id.autocomplete_fragment)
                    as AutocompleteSupportFragment

        // Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(listOf(Place.Field.ID, Place.Field.NAME))

        // Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                // TODO: Get info about the selected place.
                Log.d("PLACES_ACTIVITY", "Place: ${place.name}, ${place.id}")
            }

            override fun onError(status: Status) {
                // TODO: Handle the error.
                Log.d("PLACES_ACTIVITY", "An error occurred: $status")
            }
        })



    }

















    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(com.uzalz.mpa_lift_app.R.menu.menu_places, menu)
        return true
    }





    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.getItemId()) {
            R.id.search -> {
                onSearchCalled()
                true
            }
            R.id.home -> {
               // finish()
                true
            }
            else -> false
        }
    }

    fun onSearchCalled() { // Set the fields to specify which types of place data to return.
        val fields: List<Place.Field> = Arrays.asList(
            Place.Field.ID,
            Place.Field.NAME,
            Place.Field.ADDRESS,
            Place.Field.LAT_LNG
        )
        // Start the autocomplete intent.
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).setCountry("UG") //NIGERIA
         .build(this)
        startActivityForResult(intent,
            AUTOCOMPLETE_REQUEST_CODE
        )
    }


    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int, @Nullable data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val place = Autocomplete.getPlaceFromIntent(data!!)
              //  Log.i(FragmentActivity.TAG, "Place: " + place.name + ", " + place.id + ", " + place.address)
                Toast.makeText(this, "ID: " + place.id + "address:" + place.address + "Name:" + place.name + " latlong: " + place.latLng, Toast.LENGTH_LONG).show()
                val address = place.address
                // do query with address
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) { // TODO: Handle the error.
                val status = Autocomplete.getStatusFromIntent(data!!)
                Toast.makeText(this, "Error: " + status.getStatusMessage(), Toast.LENGTH_LONG ).show()
               // Log.i(FragmentActivity.TAG, status.getStatusMessage())
            } else if (resultCode == Activity.RESULT_CANCELED) { // The user canceled the operation.
            }
        }
    }



    companion object {
      var  AUTOCOMPLETE_REQUEST_CODE = 123
    }


}
