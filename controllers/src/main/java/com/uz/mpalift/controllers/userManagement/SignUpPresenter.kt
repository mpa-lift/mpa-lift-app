package com.uz.mpalift.controllers.userManagement





interface SignUpPresenter {

    fun showSignUpProgress(isShow: Boolean)
    fun showSignUpSuccess(driverName: String)
    fun showSignUpError(error:String)


}