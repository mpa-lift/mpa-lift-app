package com.uz.mpalift.controllers

import android.content.ContentValues.TAG
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.text.TextUtils
import android.util.Log
import android.widget.TextView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.maps.android.SphericalUtil
import com.uz.mpalift.mpa_lift_app.module.AppExecutors
import com.uz.mpalift.mpa_lift_app.module.entities.TripLocation
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.math.BigDecimal
import java.net.HttpURLConnection
import java.net.URL
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList


class MapUtils(var context: Context) {


    //////////////////////////  DIRECTIONS ?/////////////////////////////////////

    private fun getRequestedUrl(origin: LatLng, destination: LatLng): String {
        val strOrigin = "origin=" + origin.latitude + "," + origin.longitude
        val strDestination = "destination=" + destination.latitude + "," + destination.longitude
        val sensor = "sensor=false"
        val mode = "mode=driving"

        val parameters = "$strOrigin&$strDestination&$sensor&$mode";
        val output = "json";
        val apiKEY = "&key="+context.resources.getString(R.string.api_key_debug)

        val url = "https://maps.googleapis.com/maps/api/directions/$output?$parameters$apiKEY";

       // Log.d("HomeFragment-P", "URL$url")

        return url;
    }

    //2.  Request direction from Google Direction API
    // @param requestedUrl see {@link #buildRequestUrl(LatLng, LatLng)}
    //  @return JSON data routes/direction

    private fun requestDirection(requestedUrl:String):String {
        var responseString = ""
        var inputStream : InputStream? = null
        var httpURLConnection: HttpURLConnection? =null
        try {
            val url =  URL(requestedUrl)
            httpURLConnection = url.openConnection() as HttpURLConnection?
            httpURLConnection?.connect()

            inputStream = httpURLConnection?.inputStream
            val reader =  InputStreamReader(inputStream!!)
            val bufferedReader = BufferedReader(reader);

            val stringBuffer = StringBuffer()

            reader.forEachLine {
                stringBuffer.append(it)
                Log.d("HomeFragment-LINES: ","LINE: "+it)

            }

            responseString = stringBuffer.toString()
            bufferedReader.close();
            reader.close();
        } catch (e:Exception ) {
            e.printStackTrace()
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (e: IOException) {
                    e.printStackTrace();
                }
            }
        }
        httpURLConnection?.disconnect();
        return responseString
    }


    //fun getMyPoints():ArrayList<LatLng>{
     // return points!!
    // }


    interface DirectionsCallback{

        fun onDirectionsFound()
        fun onDirectionsNotFound(msg: String)

    }


    var points :  ArrayList<LatLng> ? = null
    var polylineOptions : PolylineOptions? = null

    fun removePolylines(){

      polylineOptions?.addAll(null)
    }

    fun drawDirections(origin: LatLng, dest: LatLng, mGoogleMap: GoogleMap, directionsCallback: DirectionsCallback){

        Log.d("Cordinates", "o: "+origin+" d: "+dest)

        //This methhod should run in athread
        var appExecutors = AppExecutors()

        var runnable1 = Runnable {

            var direction =  requestDirection(getRequestedUrl(origin,dest))
            Log.d("MapUtils:d","routes: "+direction)

            //Json object parsing on another thread
            var runnable2 = Runnable {

                var routes : List<List<HashMap<String, String>>>  ? = null
                var jsonObject : JSONObject? = null

                try {
                    jsonObject = JSONObject(direction)
                    var parser = DirectionsJSONParser()

                    routes = parser.parse(jsonObject)

                    Log.d("MapUtils:x","routes: "+routes)

                } catch (e: JSONException) {
                    e.printStackTrace();
                    Log.d("MapUtils:x","error: "+e.message)
                }

                //var points :  ArrayList<LatLng> ? = null
               // var  polylineOptions : PolylineOptions? = null

                for (path in routes!!) {
                    points = ArrayList()
                    if(polylineOptions==null){
                        polylineOptions =  PolylineOptions()
                        }

                    polylineOptions?.startCap(SquareCap())

                          for (point in path) {
                             var lat = point["lat"]?.toDouble()
                             var lon = point["lng"]?.toDouble()
                               points!!.add(LatLng(lat!!, lon!!))
                          }
                           polylineOptions?.addAll(points)
                           polylineOptions?.width(8f);//15
                           polylineOptions?.color(context.resources.getColor(R.color.colorAccent))
                           polylineOptions?.geodesic(true)
                    }


                if (polylineOptions != null) {

                    var runnableMain = Runnable {

                        directionsCallback.onDirectionsFound()
                        mGoogleMap.addPolyline(polylineOptions)
                        if (points != null) {
                            centerIncidentRouteOnMap(points!!,mGoogleMap,dest)
                          //  addOriginDestinationMarkerAndGet()
                        }

                    }
                    appExecutors.mainThread().execute(runnableMain)

                } else {
                    var runnableMain = Runnable {
                        directionsCallback.onDirectionsNotFound("Directions not found! try changing your location")
                    }
                    appExecutors.mainThread().execute(runnableMain)
                }

            }
            appExecutors.diskIO().execute(runnable2)


        }
        appExecutors.diskIO().execute(runnable1)
    }

    //////////  Markers      /////////////////////////////////////////////////////////////////////

     fun addOriginDestinationMarkerAndGet(latLng: LatLng, mGoogleMap: GoogleMap): Marker {
        val bitmapDescriptor =
            BitmapDescriptorFactory.fromBitmap(
                MapUtils(
                    context
                ).getOriginDestinationMarkerBitmap())
        return mGoogleMap.addMarker(
            MarkerOptions().position(latLng).flat(true).icon(bitmapDescriptor)
        )
    }




    fun getOriginDestinationMarkerBitmap(): Bitmap {
        val height = 20
        val width = 20
        val bitmap = Bitmap.createBitmap(height, width, Bitmap.Config.RGB_565)
        val canvas = Canvas(bitmap)
        val paint = Paint()
        paint.color = Color.BLACK
        paint.style = Paint.Style.FILL
        paint.isAntiAlias = true
        canvas.drawRect(0F, 0F, width.toFloat(), height.toFloat(), paint)
        return bitmap
    }


    fun centerIncidentRouteOnMap(copiedPoints: List<LatLng>,  mGoogleMap: GoogleMap, dest: LatLng) {
        var minLat = Int.MAX_VALUE.toDouble()
        var maxLat = Int.MIN_VALUE.toDouble()
        var minLon = Int.MAX_VALUE.toDouble()
        var maxLon = Int.MIN_VALUE.toDouble()

        for (point in copiedPoints) {
            maxLat = point.latitude.coerceAtLeast(maxLat)
            minLat = Math.min(point.latitude, minLat)
            maxLon = Math.max(point.longitude, maxLon)
            minLon = Math.min(point.longitude, minLon)
        }

      val bounds = LatLngBounds.Builder().include(LatLng(maxLat, maxLon)).include(LatLng(minLat, minLon)).build()

        var cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds,  10)
        mGoogleMap.animateCamera(cameraUpdate,object: GoogleMap.CancelableCallback{

            override fun onFinish() {

                val cameraPosition = CameraPosition.Builder()
                    .target(dest) // Sets the center of the map to Mountain View
                    .zoom(13f)            // Sets the zoom
                    .bearing(90f)         // Sets the orientation of the camera to east 90f
                    .tilt(65f)            // Sets the tilt of the camera to 30 degrees - how flat the view is
                    .build()              // Creates a CameraPosition from the builder
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

            }

            override fun onCancel() {


            }

        })

        //mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 300))





        val zoomLevel = 13f
      //  mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(zoomLevel))
    }



    ///////////////////////    STORE ROUTE IN DATABASE  //////////////////////////////////////



    interface TripRouteCallback{

        fun onSuccess(tripLocations: ArrayList<TripLocation>)
        fun onFailed(error: String)
    }


    fun getTripLocationsFromPolylines(trip: MapRoute, tripRouteCallback: TripRouteCallback){
        var pointsLocation = ArrayList<TripLocation>()
        //This method should run in a thread
        var appExecutors= AppExecutors()

        var runnable1 = Runnable {

            var locationOriginDriver =  LatLng(trip.origin!!.latitude,trip.origin!!.longitude)
            var locationDestDriver =  LatLng(trip.destination!!.latitude,trip.destination!!.longitude)


            var direction =  requestDirection(getRequestedUrl(locationOriginDriver,locationDestDriver))
            //Json object parsing on another thread
            var runnable2 = Runnable {

                var routes : List<List<HashMap<String, String>>>  ? = null
                var jsonObject : JSONObject? = null

                try {
                    jsonObject = JSONObject(direction)
                    var parser = DirectionsJSONParser()

                    routes = parser.parse(jsonObject)

                } catch (e: JSONException) {
                    e.printStackTrace();
                }

                Log.d("HomeFragment-P","ROUTES"+routes?.size)

                //var points :  ArrayList<LatLng> ? = null
                var  polylineOptions : PolylineOptions? = null

                for (path in routes!!) {
                    points = ArrayList()
                    pointsLocation = ArrayList()
                    polylineOptions =  PolylineOptions()
                    polylineOptions.startCap(SquareCap())


                    for (point in path) {

                        var lat = point["lat"]?.toDouble()
                        var lon = point["lng"]?.toDouble()
                        ///  Log.d("HomeFragment-P","POINTS- "+lat+" "+lon)
                        points!!.add(LatLng(lat!!, lon!!))

                        //for saving in db
                        var tripLoc =
                            TripLocation()
                        tripLoc.longitude = lon
                        tripLoc.latitude = lat
                        pointsLocation.add(tripLoc)

                    }
                    polylineOptions.addAll(points)
                    polylineOptions.width(15f);
                    polylineOptions.color(Color.BLUE)
                    polylineOptions.geodesic(true)
                }

                if (polylineOptions != null) {

                   // trip.triproute = pointsLocation

                    var run = Runnable {
                        tripRouteCallback.onSuccess(pointsLocation)
                    }
                    appExecutors.mainThread().execute(run)

                } else {
                    //  Toast.makeText(context, "Direction not found", Toast.LENGTH_LONG).show();
                    var run = Runnable {
                        tripRouteCallback.onFailed("Polylines empty")
                    }
                    appExecutors.mainThread().execute(run)
                }

            }
            appExecutors.diskIO().execute(runnable2)

        }
        appExecutors.diskIO().execute(runnable1)

    }


    var appExecutors = AppExecutors()

    fun reverseGeocoding(location : Location, tv:TextView, isShowFullAddress:Boolean) {
        // Although latitude and longitude are useful for calculating distance or displaying a map position,
        // in many cases the address of the location is more useful.
        //but needs to be run in the background
        var runnable = Runnable {

            //Geocode and update on mainthread
            var geocoder = Geocoder(context, Locale.getDefault())

            var addresses : List<Address> ? = null
            var resultMessage = ""

            try {
                addresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    // In this sample, get just a single address
                    1)

                if (addresses == null || addresses.isEmpty()) {
                    if (resultMessage.isEmpty()) {
                        resultMessage = context.getString(R.string.no_address_found);
                        Log.e(TAG, resultMessage)
                        Log.d("HomeFragment",resultMessage)
                    }
                }else {
                    Log.d("HomeFragment","result :no empty")
                    // If an address is found, read it into resultMessage
                    var address : Address = addresses[0]
                    var addressParts = ArrayList<String>()
                    Log.d("HomeFragment","result: no empty: "+address.getAddressLine(0))

                    // Fetch the address lines using getAddressLine,
                    // join them, and send them to the thread
                    for (i in 0..address.maxAddressLineIndex) {
                        addressParts.add(address.getAddressLine(i))
                        Log.d("HomeFragment","result :address"+address.getAddressLine(i))
                    }
                    resultMessage = TextUtils.join("\n", addressParts)
                }


                appExecutors.mainThread().execute {
                    Log.d("HomeFragment","result :" +resultMessage)
                    //  Toast.makeText(requireContext(), resultMessage, Toast.LENGTH_LONG).show()

                    //var ltdLong :LatLng = LatLng(location.latitude,location.longitude)
                   // setMapDefaultMark(resultMessage,ltdLong,map)
                    if(isShowFullAddress){
                        tv.text = resultMessage
                    }else{
                        var arr = resultMessage.split(",")
                        var mylocation = arr[0] +" "+arr[1]
                        tv.text = mylocation
                    }



                }


            }catch (ioException: IOException) {
                // Catch network or other I/O problems
                resultMessage = context.getString(R.string.service_not_available)
                Log.e(TAG, resultMessage, ioException)
            }
            catch (illegalArgumentException:IllegalArgumentException ) {
                // Catch invalid latitude or longitude values
                resultMessage = context.getString(R.string.invalid_lat_long_used)
                Log.e(TAG, resultMessage + ". " + "Latitude = " + location.getLatitude() + ", Longitude = " + location.getLongitude(), illegalArgumentException);
            }

        }
        appExecutors.diskIO().execute(runnable)

    }








   companion object{


       fun convertFromLatLngToLocation(latLng: LatLng):Location{

           var locOrigin = Location(LocationManager.GPS_PROVIDER)
           locOrigin.latitude = latLng.latitude
           locOrigin.longitude = latLng.longitude

           return locOrigin
       }

       fun convertFromLocationToLatLng(location: Location):LatLng{

           var latLng = LatLng(location.latitude, location.longitude)

           return latLng
       }


       fun convertFromTripLocationToLatLng(tripLocation: TripLocation):LatLng{

           var lat = tripLocation.latitude
           var lon = tripLocation.longitude

           var tripOriginLatLng = LatLng(lat!!,lon!!)

           return tripOriginLatLng
       }

       fun convertFromLatLngTripLocation(latLng: LatLng): TripLocation {

           var locPassengerOrigin =
               TripLocation()
           locPassengerOrigin.latitude = latLng.latitude
           locPassengerOrigin.longitude = latLng.longitude

           return locPassengerOrigin
       }

       fun convertFromTripLocationToLocation(tripLocation: TripLocation):Location{

           var locationDes = Location(LocationManager.GPS_PROVIDER)
           locationDes.longitude = tripLocation.longitude!!
           locationDes.latitude = tripLocation.latitude!!

           return locationDes
       }
       fun convertFromLocationToTripLocation(location: Location):TripLocation{

           var tripLocation = TripLocation()
           tripLocation.latitude = location.latitude
           tripLocation.longitude = location.longitude

           return tripLocation
       }



       fun findDistance(origin: LatLng, destination: LatLng): Float{

           //Calculating the distance in meters
           //Calculating the distance in meters
           var distance: Double = SphericalUtil.computeDistanceBetween(origin, destination)

           var d = distance/1000

           return d.toFloat()
       }



       fun findFareByDistance(origin: LatLng, destination: LatLng): BigDecimal{

           var distance: Double = SphericalUtil.computeDistanceBetween(origin, destination)

           var distanceInMeters = distance/1000

           var rateOfCharge = 0.075 //based on jinja - kla 80km by 6000/=



           var fare = rateOfCharge * distanceInMeters
           var bigDecimalVal = BigDecimal(fare)

           return bigDecimalVal

       }

       fun formatToDollars(bigDecimalVal: BigDecimal):String{
           val n: NumberFormat = NumberFormat.getCurrencyInstance(Locale.US)
           val doublePayment: Double = bigDecimalVal.toDouble()
           val s: String = n.format(doublePayment)
           return s
       }

       fun convertFareByDistance(origin: LatLng, destination: LatLng): String{

           var distance: Double = SphericalUtil.computeDistanceBetween(origin, destination)

           var distanceInMeters = distance/1000

           var rateOfCharge = 0.075 //based on jinja - kla 80km by 6000/=



           var fare = rateOfCharge * distanceInMeters
           var bigDecimalVal = BigDecimal(fare)


           val n: NumberFormat = NumberFormat.getCurrencyInstance(Locale.US)
           val doublePayment: Double = bigDecimalVal.toDouble()
           val s: String = n.format(doublePayment)

           return s

       }





       fun convertMetersTOKM(meters : Float){




       }






   }



}