package com.uz.mpalift.controllers.userManagement

//import com.uz.mpalift.mpa_lift_app.ui.home.HomeViewModel
import com.uz.mpalift.mpa_lift_app.module.entities.*
import com.uz.mpalift.mpa_lift_app.module.userManagement.UserRepository

class SignUpController(var signUpPresenter: SignUpPresenter) {

    private var userRepository = UserRepository()


    fun getPassengerDetails(userId:String){

        userRepository.getPassengerDetails(userId,object:UserRepository.GetPassengerDetailsCallback{

            override fun onGetPassengerDetails(user: MpaLiftUser) {
             // homeViewModel.mGetPassengerDetails.postValue(passenger)
            }

            override fun onFailed(error: String) {

            }

        })

    }



    fun getRegisteredVehicles(driverId:String){

        userRepository.getRegisteredCars(driverId, object: UserRepository.GetDriverVehiclesCallback {

            override fun onResponded(vehicles: MutableList<TripVehicle>) {

            //   homeViewModel.mGetRegisteredVehicles.postValue(vehicles)

            }
            override fun onResponseEmpty() {
           //   homeViewModel.mGetRegisteredVehiclesEmpty.postValue("No Vehicles Registered")
            }
            override fun onResponseFailed(error: String) {

            }
        })

    }


    fun registerDriver( driverId: String, driverFirstName:String, driverCity:String,driverEmail:String,
    driverPassword:String, driverPicUrl:String){


        var driver = DrivingLicence()

       // driver.driverId = driverId
       // driver.driverFirstName = driverFirstName
      //  driver.driverCity = driverCity
      //  driver.driverEmail = driverEmail
       // driver.driverPassword = driverPassword
       // driver.driverPicUrl = driverPicUrl

        signUpPresenter.showSignUpProgress(true)

        userRepository.registerAsDriver(driver, object: UserRepository.RegisterDriverCallback{

            override fun onSuccess() {

                //we are meant to use the ResponseBody of the UseCase

              //  signUpPresenter.showSignUpProgress(false)
             //   signUpPresenter.showSignUpSuccess(driver.driverFirstName!!)

            }

            override fun onError(error: String) {
                // var msg = "Thank you "+myName+" welcome!"
                signUpPresenter.showSignUpError(error)
            }

        })




    }



}