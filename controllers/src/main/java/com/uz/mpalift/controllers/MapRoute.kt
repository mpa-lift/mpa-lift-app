package com.uz.mpalift.controllers

import com.google.android.gms.maps.model.LatLng

class MapRoute {

    var origin : LatLng ? = null
    var destination : LatLng ? = null

    var tripId : String ? = null
    var customerId : String ? = null
    var tripRequestId : String ? = null

    var tripRoute : ArrayList<LatLng> ? = null

    init {
        tripRoute = ArrayList()
    }

}