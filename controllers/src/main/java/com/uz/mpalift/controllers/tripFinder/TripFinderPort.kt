package com.uz.mpalift.controllers.tripFinder


import com.uz.mpalift.mpa_lift_app.module.entities.*

interface TripFinderPort {


    fun isShowProgress(isShow:Boolean)
    fun presentAvailableTrips(trips: ArrayList<TripFinderViewModel>)
    fun showLoadError(error:String)


}