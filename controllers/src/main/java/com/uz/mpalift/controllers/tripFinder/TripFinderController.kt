package com.uz.mpalift.controllers.tripFinder

import android.location.Location
import android.location.LocationManager
import com.google.android.gms.maps.model.LatLng
//import com.uz.mpalift.mpa_lift_app. ui.home.HomeViewModel
import com.uz.mpalift.mpa_lift_app.module.entities.Trip
import com.uz.mpalift.mpa_lift_app.module.tripFinder.TripFinderRepository

class TripFinderController(var tripFinderPresenter: TripFinderPort) {


    //A presenter gets the data and prepares it into strings and presents

    var tripFinder = TripFinderRepository()

   fun findLifts(pickupLocation: LatLng){

       tripFinder.liftFinder(pickupLocation, object: TripFinderRepository.GetTripsCallback{

           override fun onTripsReceived(list: MutableList<Trip>) {
               //feed to the ui from here, the view model

               var mylifts = ArrayList<TripFinderViewModel>()
               for (trip in list){
                 var tripViewModel = TripFinderViewModel()
                   tripViewModel.driverId = trip.driverId
                   tripViewModel.tripId = trip.tripId
                   tripViewModel.driverName = trip.driverName
                   tripViewModel.driverPicUrl =trip.driverPicUrl
                   tripViewModel.tripAmount = trip.fare
                   tripViewModel.tripDistance =trip.distance

                   //convert to location
                   var locOrigin = Location(LocationManager.GPS_PROVIDER)
                   locOrigin.latitude = trip.tripOrigin!!.latitude!!
                   locOrigin.longitude = trip.tripOrigin!!.longitude!!

                   var locDes = Location(LocationManager.GPS_PROVIDER)
                   locDes.latitude = trip.tripDestination!!.latitude!!
                   locDes.longitude = trip.tripDestination!!.longitude!!



                   tripViewModel.tripDriverDestination =locDes
                   tripViewModel.tripDriverOrigin =locOrigin

                   var seats = trip.tripvehicle!!.vehicleSeats!!
                   tripViewModel.vehicleSeats = seats
                   tripViewModel.vehicleName = trip.tripvehicle!!.vehicleName!!

                   mylifts.add(tripViewModel)

               }

                tripFinderPresenter.presentAvailableTrips(mylifts)
                //  homeViewModel.listOfLifts.postValue(mylifts)

           }

           override fun onTripsEmpty(msg: String) {
               tripFinderPresenter.showLoadError(msg)
           }

           override fun onTripsCancelled(error: String) {
               tripFinderPresenter.showLoadError(error)
           }

       })

    }


    class TripFinderPresenter(){


    }




}