package com.uz.mpalift.controllers.tripFinder

import android.location.Location
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.uz.mpalift.controllers.tripFinder.TripFinderViewModel

class TripFinderViewModel : ViewModel(){

    //the viewmodel is here, instead of the presenters coz the viewmodels are reactive
    var tripId : String ? = null
    var driverId : String ? = null
    var driverName : String ? = null
    var vehicleName : String ? = null
    var vehicleSeats : String ? = null
    var driverPicUrl : String ? = null


    var tripDriverOrigin : Location ? = null
    var tripDriverDestination : Location? = null
    var tripDriverDropOff : Location ? = null

    var tripDistance : Float ? = null
    var tripAmount: String ? = null

    var driverWalletAmount : String ? = null

    //LIFTS
    val listOfLifts = MutableLiveData<List<TripFinderViewModel>>()




}