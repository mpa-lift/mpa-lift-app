package com.uz.mpalift.controllers.tripRequests

import android.location.Location
import android.util.Log
import android.widget.Toast
import com.uz.mpalift.controllers.MapUtils
import com.uz.mpalift.controllers.tripFinder.TripFinderViewModel
import com.uz.mpalift.mpa_lift_app.module.entities.*
import com.uz.mpalift.mpa_lift_app.module.tripMgt.TripManagementRepository
import com.uz.mpalift.mpa_lift_app.module.tripRequests.TripRequestsRepository
import com.uz.mpalift.mpa_lift_app.module.userManagement.UserRepository
import java.util.*


class TripRequestsController(var tripRequestsPresenter: TripRequestsPresenter) {

    private var tripRequestsRepository = TripRequestsRepository()
    private var userRepository = UserRepository()
    private var tripManagementRepository = TripManagementRepository()



    fun presentRequestSent(res: TripResponse){

        tripRequestsPresenter.showRequestResponseSent(res)

    }


    // UseCase RequestForRide
    fun sendDriverRequest(userId:String, trip: TripFinderViewModel){

        userRepository.getPassengerDetails(userId,object:UserRepository.GetPassengerDetailsCallback{

            override fun onGetPassengerDetails(user: MpaLiftUser) {

                //  Check the driver id is set to prevent kotlin null

                var tripRequest = TripRequest()
                tripRequest.tripId = trip.tripId
                tripRequest.passengerId = user.passengerId //driver.driverId
                tripRequest.passengerName = user.passengerFirstName

                var locO = MapUtils.convertFromLocationToTripLocation(trip.tripDriverOrigin!!)
                var locD = MapUtils.convertFromLocationToTripLocation(trip.tripDriverDestination!!)

                tripRequest.destination = locD
                tripRequest.origin = locO

                tripRequestsRepository.sendDriverARequest(tripRequest,trip.vehicleSeats!!,object: TripRequestsRepository.SendRequestCallback{

                    override fun onRequestSent(res: TripResponse) {
                        res.driverName = trip.driverName
                        res.vehicleName = trip.vehicleName


                        // homeViewModel.mSendRequestResponse.postValue(res)
                        tripRequestsPresenter.showRequestResponseSent(res)

                    }

                    override fun onRequestSeatsUnAvailable(msg: String) {
                      //  homeViewModel.mSendRequestSeatsUnAvailable.postValue(msg)
                        tripRequestsPresenter.showRequestSentSeatsAvailiableResponse(msg)
                    }

                    override fun onRequestFailed(error: String) {
                     //   homeViewModel.mSendRequestError.postValue(error)
                        tripRequestsPresenter.showRequestSentError(error)

                    }

                })

            }

            override fun onFailed(error: String) {

            }


        })


    }




    fun showRequestTracking(request: TripRequest){
        tripRequestsPresenter.showRequestResponseSentTracking(request)
    }

    fun monitorRequestAcceptance(tripId:String){

        tripRequestsRepository.monitorSentRequestAcceptance(tripId,object : TripRequestsRepository.MonitorRequestStatusCallback{

            override fun onRequestResponded(request: TripRequest) {

                if (request.requestStatus == TripRequest.ACCEPTED){
                    //  sheetPassengerMap.toggleBottomSheet()
                    // 1. start tracking the driver
                       Log.d("TripRequestController", "Response:accepted")

                    // homeViewModel.mTrackRequestResponse.postValue(request)
                    // homeViewModel.setTrackDataFromDriverToPassenger()
                    tripRequestsPresenter.showRequestResponseSentTracking(request)
                   // tripRequestsPresenter.setTrackDataFromDriverToPassenger()



                }
            }
            override fun onRequestFailed(error: String) {

            }
        })


    }

    fun getRequestedDriverComingDetails(tripId: String){

        tripManagementRepository.getOnTripDetails(tripId,object:TripManagementRepository.OnTripDetailsCallback{

            override fun onTripReceived(onTrip: Trip) {
                // care details
                var carName = ""+onTrip.tripvehicle!!.vehicleName
                var carSeats = " | "+onTrip.tripvehicle!!.vehicleSeats +" seats"

                //var driverDetails = com.uz.mpalift.mpa_lift_app.ui.home.HomeViewModel.DriverComingViewModel(onTrip.driverPicUrl!!,onTrip.driverName!!,carName,carSeats)

                //homeViewModel.mTrackRequestDriverComing.postValue(driverDetails)
                tripRequestsPresenter.showTrackingDriverComing(onTrip.driverPicUrl!!,onTrip.driverName!!,carName,carSeats)

            }

            override fun onFailed(error: String) {

            }

        })
    }

    fun monitorRequestDriverArrival(tripId: String, myLocation: Location){

        tripRequestsRepository.trackDriverByLocation(tripId,myLocation,object: TripRequestsRepository.TrackRequestedDriverCallback{

            override fun onSuccess(info:String) {


                tripManagementRepository.getOnTripDetails(tripId,object:TripManagementRepository.OnTripDetailsCallback{

                    override fun onTripReceived(onTrip: Trip) {


                        var trackArrivalNotice = "Driver has arrived"
                        //2. show enjoy ride info
                        var trackArrivalTitle = "ENJOY YOUR RIDE"
                        var trackArrivalSubtitle = "$info left: Driver will start trip"
                        //3. show plate and destination
                        var isShowNoPlate = false
                        //layout_trip_tracking_show_plate.visibility = View.VISIBLE
                        var trackArrivalNoPlate = onTrip.tripvehicle!!.vehicleNoPlate
                        // label_passenger_trip_track_car_plate.text = carPlate

                        var locationDes =
                            MapUtils.convertFromTripLocationToLocation(
                                onTrip.tripDestination!!
                            )
                        //  MapUtils(baseContext).reverseGeocoding(locationDes,label_passenger_trip_track_car_destination)

                      //  var arrivingViewModel = com.uz.mpalift.mpa_lift_app.ui.home.HomeViewModel.DriverArrivingViewModel(trackArrivalNotice,trackArrivalTitle,
                        //    trackArrivalSubtitle,isShowNoPlate,trackArrivalNoPlate!!,locationDes)

                     //   homeViewModel.mTrackRequestDriverArriving.postValue(arrivingViewModel)
                        tripRequestsPresenter.showTrackingDriverArriving(trackArrivalNotice,trackArrivalTitle,
                            trackArrivalSubtitle,isShowNoPlate,trackArrivalNoPlate!!,onTrip.tripDestination!!)



                    }

                    override fun onFailed(error: String) {

                    }

                })


            }

            override fun onFailed(error: String) {
               // homeViewModel.mTrackRequestDriverArrivingError.postValue(error)
                tripRequestsPresenter.showDriverArrivingError(error)
            }

        })

    }



    fun waitOnRequests(tripId: String){

        tripRequestsRepository.waitForRequest(tripId,object:TripRequestsRepository.WaitForRequestCallback{

            override fun onRequestReceived(requests: MutableList<TripRequest>) {

                //homeViewModel.mWaitOnRequests.postValue(requests)
                tripRequestsPresenter.showRequestWaiting(requests)

            }

            override fun onRequestFailed(error: String) {
              //  homeViewModel.mWaitOnRequestsError.postValue(error)
                tripRequestsPresenter.showRequestsWaitingError(error)

            }


        })

    }




    fun showRespondToRequest(isAccepted: Boolean){
        tripRequestsPresenter.showRequestAccepted(isAccepted)
    }

    //This is replaced
    // UseCase RespondToRideRequest
    fun respondToRequest(driverId:String, request: TripRequest, tripRequestResponse: String){

        tripRequestsRepository.respondToRequest(driverId,request,tripRequestResponse, object:TripRequestsRepository.RespondToRequestCallback{

            override fun onResponded() {

                if(tripRequestResponse == TripRequest.ACCEPTED) {
                  //  homeViewModel.mIsRequestAccepted.postValue(true)
                    tripRequestsPresenter.showRequestAccepted(true)


                }else {//Request is Rejected
                   // homeViewModel.mIsRequestAccepted.postValue(false)
                    tripRequestsPresenter.showRequestAccepted(false)
                }

            }

            override fun onResponseFailed(error: String) {


            }

        })

    }




}