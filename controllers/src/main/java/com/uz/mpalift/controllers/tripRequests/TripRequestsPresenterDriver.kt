package com.uz.mpalift.controllers.tripRequests

import com.uz.mpalift.mpa_lift_app.module.entities.TripLocation
import com.uz.mpalift.mpa_lift_app.module.entities.TripRequest
import com.uz.mpalift.mpa_lift_app.module.entities.TripResponse


interface TripRequestsPresenterDriver {


    fun showRequestAccepted(isRequestAccepted: Boolean)
    fun showRequestWaiting(requests: MutableList<TripRequest>)
    fun showRequestsWaitingError(error:String)
    fun showRequestResponseSentTracking(request: TripRequest)
    fun showRequestResponseSent(res: TripResponse)




}