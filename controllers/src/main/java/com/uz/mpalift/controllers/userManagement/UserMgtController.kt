package com.uz.mpalift.controllers.userManagement

import android.util.Log
import com.uz.mpalift.mpa_lift_app.module.AppExecutors
import com.uz.mpalift.mpa_lift_app.module.entities.*
import com.uz.mpalift.mpa_lift_app.module.userManagement.UserRepository

class UserMgtController(var userProfilePort: UserProfilePort) {

    private var userRepository = UserRepository()


    fun getUserProfile(userId:String){

            userRepository.getPassengerDetails(userId,object:UserRepository.GetPassengerDetailsCallback{

                override fun onGetPassengerDetails(user: MpaLiftUser) {
                    userProfilePort.presentUserDetails(user.passengerFirstName!!,user.passengerEmail!!,user.passengerPicUrl!!)
                }

                override fun onFailed(error: String) {
                    userProfilePort.showLoadError(error)
                }
            })

    }

    fun getPassengerDetails(userId:String){

        userRepository.getPassengerDetails(userId,object:UserRepository.GetPassengerDetailsCallback{

            override fun onGetPassengerDetails(user: MpaLiftUser) {

           //   homeViewModel.mGetPassengerDetails.postValue(passenger)
                userProfilePort.showPassengerDetails(user)

            }

            override fun onFailed(error: String) {

            }
        })

    }

    fun getRegisteredVehicles(driverId:String){

        userRepository.getRegisteredCars(driverId, object: UserRepository.GetDriverVehiclesCallback {

            override fun onResponded(vehicles: MutableList<TripVehicle>) {

              // homeViewModel.mGetRegisteredVehicles.postValue(vehicles)
                userProfilePort.showRegisteredVehicles(vehicles)

            }

            override fun onResponseEmpty() {
             // homeViewModel.mGetRegisteredVehiclesEmpty.postValue("No Vehicles Registered")
                userProfilePort.showRegisteredVehiclesError("No Vehicles Registered")
            }

            override fun onResponseFailed(error: String) {

            }
        })

    }

    fun isUserDriver(userId: String){

        userRepository.isUserDriver(userId,object : UserRepository.isUserDriverCallback{

            override fun isDriver(boolean: Boolean) {

                userProfilePort.isShowUserDriver(boolean)

            }

            override fun onError(error: String) {

              userProfilePort.errorOnCheckIsUserDriver(error)
            }

        })

    }

    fun isUserPasswordValid(userId: String,userPassword:String){

        userRepository.isUserPasswordValid(userId,userPassword,object : UserRepository.IsUserPasswordValidCallback{

            override fun isValid(boolean: Boolean) {
                Log.d("USerPass","callback"+boolean)

                var uithread = Runnable {
                    userProfilePort.isShowUserPasswordValid(boolean)
                }
               AppExecutors().mainThread().execute(uithread)

            }

            override fun onError(error: String) {

                userProfilePort.errorOnCheckIsUserPasswordValid(error)
            }

        })

    }

    //register as driver
    fun registerAsDriver(driver: DrivingLicence){

         userRepository.registerAsDriver(driver,object:UserRepository.RegisterDriverCallback{

             override fun onSuccess() {

                 var  msg = driver.userId.toString()
                 userProfilePort.showUserDriverLicenceRegistered(msg)
             }

             override fun onError(error: String) {
                 userProfilePort.showUserDriverLicenceRegisteredError(error)
             }


         })


    }



    fun registerUserForFCM(userId:String, token:String){

        userRepository.registerUserForFCM(userId,token,object:UserRepository.RegisterUserForFCMCallback{

            override fun onRegister() {

                userProfilePort.isUserRegisterForFcm(true)

            }

            override fun onError(error: String) {
                userProfilePort.showUserRegistrationErrorForFcm(error)
            }


        })



    }







}
