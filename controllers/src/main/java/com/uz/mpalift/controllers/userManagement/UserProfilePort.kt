package com.uz.mpalift.controllers.userManagement

import com.uz.mpalift.mpa_lift_app.module.entities.DrivingLicence
import com.uz.mpalift.mpa_lift_app.module.entities.MpaLiftUser
import com.uz.mpalift.mpa_lift_app.module.entities.TripVehicle

//import com.uz.mpalift.mpa_lift_app.ui.home.HomeViewModel

interface UserProfilePort {


    //profile presenter
    //request presenter
    //on trip presenter

    fun isShowProgress(isShow:Boolean)
    fun presentUserDetails(userName:String, userEmail:String, userPhotoUrl:String)
    fun showLoadError(error:String)

    fun showDriverDetails(driver: DrivingLicence)
    fun showPassengerDetails(user: MpaLiftUser)

    fun showRegisteredVehicles(vehicles: MutableList<TripVehicle>)
    fun showRegisteredVehiclesError(error:String)

    fun isShowUserDriver(isDriver: Boolean)
    fun errorOnCheckIsUserDriver(error: String)


    fun isShowUserPasswordValid(isValid: Boolean)
    fun errorOnCheckIsUserPasswordValid(error: String)

    fun showUserRegistered(isRegisteredMsg:String)
    fun showUserRegisteredError(error:String)


    fun showUserDriverLicenceRegistered(isRegisteredMsg:String)
    fun showUserDriverLicenceRegisteredError(error:String)


    fun isUserRegisterForFcm(boolean: Boolean)
    fun showUserRegistrationErrorForFcm(error: String)


}