package com.uz.mpalift.controllers.tripRequests

import com.uz.mpalift.mpa_lift_app.module.entities.TripLocation
import com.uz.mpalift.mpa_lift_app.module.entities.TripRequest
import com.uz.mpalift.mpa_lift_app.module.entities.TripResponse


interface TripRequestsPresenter {

    /*
    homeViewModel.mIsRequestAccepted.postValue(false)
    homeViewModel.mWaitOnRequests.postValue(requests)
    homeViewModel.mWaitOnRequestsError.postValue(error)
    homeViewModel.mTrackRequestDriverArriving.postValue(arrivingViewModel) ++error
    homeViewModel.mTrackRequestDriverComing.postValue(driverDetails)

    homeViewModel.mTrackRequestResponse.postValue(request)
    homeViewModel.setTrackDataFromDriverToPassenger()
    homeViewModel.mSendRequestResponse.postValue(res)
    homeViewModel.mSendRequestSeatsUnAvailable.postValue(msg)
    homeViewModel.mSendRequestError.postValue(error)

     */

    fun showRequestAccepted(isRequestAccepted: Boolean)

    fun showRequestWaiting(requests: MutableList<TripRequest>)
    fun showRequestsWaitingError(error:String)

    fun showTrackingDriverArriving(trackArrivalNotice:String,trackArrivalTitle:String,
                                   trackArrivalSubtitle:String,isShowNoPlate:Boolean,trackArrivalNoPlate:String,locationDes:TripLocation)
    fun showDriverArrivingError(error: String)

    fun showTrackingDriverComing(driverPicUrl:String,driverName:String,carName:String,carSeats:String)

    // fun showRequestResponse()
    // fun setTrackDataFromDriverToPassenger()
    fun showRequestResponseSentTracking(request: TripRequest)

    fun showRequestResponseSent(res: TripResponse)
    fun showRequestSentSeatsAvailiableResponse(msg:String)
    fun showRequestSentError(error: String)




}