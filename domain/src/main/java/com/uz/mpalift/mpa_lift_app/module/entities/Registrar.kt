package com.uz.mpalift.mpa_lift_app.module.entities

class Registrar {

    var userId : String ? = null
    var userName : String ? = null
    var registrationDate : String ? = null

}