package com.uz.mpalift.mpa_lift_app.module.entities

import com.firebase.geofire.GeoFire
import com.google.android.gms.maps.model.LatLng

class TripResponse {

    var requestId : String ? =  null   //the request has an id
    var tripId : String ? = null       //the request is for a certain trip

    var isRequestSent : Boolean = false
    var responseMessage : String ? = null
    var responseMessageError : String ? = null

    var driverOrigin : LatLng ? = null


    var vehicleName : String ? = null
    var driverName : String ? = null

    // as we track the geo loc of drivers
    // once a request is accepted we show the movement on the map


}