package com.uz.mpalift.mpa_lift_app.module.entities

import com.uz.mpalift.mpa_lift_app.module.entities.TripVehicle
import kotlin.collections.ArrayList

class DrivingLicence {

    var userId : String ? = null
    var licenceId : String ? = null
    var dateIssued : String ? = null
    var licencePhotoUrl : String ? = null


     var vehicles : MutableList<TripVehicle> ? = null


    init {
        vehicles = ArrayList()
    }

}