package com.uz.mpalift.mpa_lift_app.module.billManagement

import android.location.Location
import android.location.LocationManager
import android.util.Log
import com.firebase.geofire.GeoFire
import com.firebase.geofire.GeoLocation
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.*
import com.google.maps.android.SphericalUtil
import com.uz.mpalift.mpa_lift_app.module.AppExecutors
import com.uz.mpalift.mpa_lift_app.module.DBConfig
import com.uz.mpalift.mpa_lift_app.module.DBConfig.Companion.reference
import com.uz.mpalift.mpa_lift_app.module.entities.Bill
import com.uz.mpalift.mpa_lift_app.module.entities.Trip
import com.uz.mpalift.mpa_lift_app.module.entities.TripHistory
import com.uz.mpalift.mpa_lift_app.module.tripRequests.TripRequestsRepository


class BillManagementRepository {

    var app = AppExecutors()

    var tripRequestRepository = TripRequestsRepository()


    interface SubmitBillCallback{

        fun onBillSubmitted()
        fun onFail(error:String)
    }


    interface GetBillCallback{

        fun onGetBill(bill: Bill)
        fun onFail(error:String)
    }


    interface RemoveBillCallback{

        fun onRemoveBill()
        fun onFail(error:String)
    }


    fun getBillDetails(tripId : String, onGetBillCallback: GetBillCallback){

        var run = Runnable {

            val childEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    Log.d("DRIVER-REQUEST-WAIT", "onChildChanged:")
                    // onChildAdded() will be called for each node at the first time
                    val bill = snapshot.getValue(Bill::class.java)
                    // Log.d("DRIVER-REQUEST-LISTNER", "onChildChanged:" + request?.passengerId +" tripid: "+request?.tripId +" status: "+request?.isAccepted)
                    var runMain = Runnable {

                        if(bill!=null) {
                            onGetBillCallback.onGetBill(bill)

                        }
                    }
                    app.mainThread().execute(runMain)
                    // MainThreadExecutor(500).execute(runMain)
                }
                override fun onCancelled(error: DatabaseError) {

                }
            }
            // databaseReference.child(DBConfig.refRequests).equalTo(tripId).addListenerForSingleValueEvent(childEventListener)
            reference.child(DBConfig.refBill).child(tripId).addListenerForSingleValueEvent(childEventListener)
        }
        app.diskIO().execute(run)
    }


    fun submitBill(bill: Bill, submitBillCallback: SubmitBillCallback){

        var run = Runnable {
           var  refSubmitRating =    reference.child(DBConfig.refBill).child(bill.userId!!)
           refSubmitRating.setValue(bill)

           var listenerSubmitRating = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    //   remove the tracking database
                    //   databaseReference.child("onTripOnline").onDisconnect()
                    var uithread = Runnable {
                        submitBillCallback.onBillSubmitted()
                    }
                    app.mainThread().execute(uithread)

                }

                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {
                        submitBillCallback.onFail(databaseError.message)
                    }
                    app.mainThread().execute(uithread)
                }
            }
            refSubmitRating.addListenerForSingleValueEvent(listenerSubmitRating)

        }
        app.diskIO().execute(run)

            //  refSubmitRating.removeEventListener(listenerSubmitRating!!)
    }


    fun removeBill(userId: String, removeBillCallback: RemoveBillCallback){

        var run = Runnable {
            var  refSubmitRating =    reference.child(DBConfig.refBill).child(userId)
            refSubmitRating.removeValue()

            var listenerSubmitRating = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    //   remove the tracking database
                    //   databaseReference.child("onTripOnline").onDisconnect()
                    var uithread = Runnable {
                        removeBillCallback.onRemoveBill()
                    }
                    app.mainThread().execute(uithread)

                }

                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {
                        removeBillCallback.onFail(databaseError.message)
                    }
                    app.mainThread().execute(uithread)
                }
            }
            refSubmitRating.addListenerForSingleValueEvent(listenerSubmitRating)

        }
        app.diskIO().execute(run)

        //  refSubmitRating.removeEventListener(listenerSubmitRating!!)
    }



}