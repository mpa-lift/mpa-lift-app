package com.uz.mpalift.mpa_lift_app.module.entities

import com.uz.mpalift.mpa_lift_app.module.userManagement.DateConverter
import java.util.*
import kotlin.collections.ArrayList

class Trip {

    var tripId : String ? = null
    var driverId : String ? = null
    var driverName : String ? = null
    var driverPicUrl : String ? = null

    var tripOrigin : TripLocation? = null
    var tripDestination : TripLocation? = null

    var requestId : String ? = null

    var distance  : Float = 0F
    var fare : String ? = null
    var dateOfTrip : String ? = null


    var tripvehicle : TripVehicle? = null
    var startedTrip : StartTrip? = null

    var triproute : ArrayList<TripLocation> ? = null
    // var triplocations : MutableList<TripStatus> ? = null
    var tripRequests : MutableList<TripRequest> ? = null


    init {
        tripId = UUID.randomUUID().toString()
        // driverId = UUID.randomUUID().toString()
        startedTrip = StartTrip()
        triproute = ArrayList()
        dateOfTrip = DateConverter.dateToStringWithoutTime(Date())
    }



    class StartTrip{
        var tripId : String ? = null
        var requestId : String ? = null
        var passengerId : String ? = null

        var passengerName : String ? = null

        var tripRating : String ? = null

        var distance  : Float = 0F
        var fare : String ? = null
        var dateOfTrip : String ? = null

        var tripOrigin : TripLocation? = null
        var tripDestination : TripLocation? = null
        var triproute : ArrayList<TripLocation> ? = null

        var tripStatus : String ? = null



        init {

            //  driverId = UUID.randomUUID().toString()
            triproute = ArrayList()
            dateOfTrip = DateConverter.dateToStringWithoutTime(Date())
            tripStatus =
                TRIP_INACTIVE
            tripRating = "0.0"
        }


        companion object{

            var TRIP_STARTED = "STARTED"
            var TRIP_ENDED = "ENDED"
            var TRIP_CANCELED = "CANCELED"
            var TRIP_INACTIVE = "INACTIVE"

        }


    }




    /*
    fun isOnTrip( triproute : TripRoute, riplocations : MutableList<TripStatus>){

    }

    class TripStatus{

        var origin : TripLocation? = null
        var destination : TripLocation? = null
        var isOnTrip : Boolean = false

    }

     */




}