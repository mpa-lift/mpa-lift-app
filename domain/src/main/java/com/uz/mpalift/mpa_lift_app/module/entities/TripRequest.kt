package com.uz.mpalift.mpa_lift_app.module.entities

import java.util.*

class TripRequest {

    public var requestId : String ? =  null   //the request has an id
    public var tripId : String ? = null       //the request is for a certain trip
    public var passengerId : String ? =  null  //the request is sent by the passenger
    public var passengerName : String ? =  null  //the request is sent by the passenger
    var calculatedFare : String? = null
    var requestDate : String? = null
    var requestVoiceMsg : String? = null
    var passengerRating : String ? = null

                                        // the trip is set to go some where
    public var origin : TripLocation? = null
    public var destination : TripLocation? = null

    public var requestStatus : String? = null




    init {
        requestId = UUID.randomUUID().toString()
        requestStatus =
            PENDING
    }

    data class RequestStatus(var status:String) {

    }


    companion object{
        var PENDING = "PENDING"
        var ACCEPTED = "ACCEPTED"
        var REJECTED = "REJECTED"



    }



}