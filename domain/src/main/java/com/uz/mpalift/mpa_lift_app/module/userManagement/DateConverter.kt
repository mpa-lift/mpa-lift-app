package com.uz.mpalift.mpa_lift_app.module.userManagement

import java.text.SimpleDateFormat
import java.util.*

class DateConverter {


    companion object{

        fun dateToStringWithoutTime(date: Date?): String? {
            val justDateNoTime: String
            val dateFormatter =
                SimpleDateFormat("MMMM d, yyyy", Locale.US)
            dateFormatter.timeZone = TimeZone.getTimeZone("UTC")
            //SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            justDateNoTime = dateFormatter.format(date!!)
            return justDateNoTime
        }



    }
}