package com.uz.mpalift.mpa_lift_app.module.entities

import com.google.android.gms.maps.model.LatLng
import java.util.*
import kotlin.collections.ArrayList

class MpaLiftUser {

    var passengerId : String ? = null
    var passengerFirstName : String ? = null
    var passengerEmail : String ? = null
    var passengerPassword : String ? = null
    var passengerCity : String ? = null
    var passengerPicUrl : String ? = null
    var passengerRating : String ? = null

    var  fcmToken : String ? = null

    var isDrivingLicenceActivated = false
    var drivingLicence : DrivingLicence ? = null


}