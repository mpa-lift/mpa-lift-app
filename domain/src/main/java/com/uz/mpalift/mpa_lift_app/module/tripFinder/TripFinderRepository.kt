package com.uz.mpalift.mpa_lift_app.module.tripFinder

import android.location.Location
import android.location.LocationManager
import android.util.Log
import com.firebase.geofire.GeoFire
import com.firebase.geofire.GeoLocation
import com.firebase.geofire.GeoQueryEventListener
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolylineOptions
import com.google.android.gms.maps.model.SquareCap
import com.google.firebase.database.*
import com.google.maps.android.PolyUtil
import com.uz.mpalift.mpa_lift_app.module.AppExecutors
import com.uz.mpalift.mpa_lift_app.module.DBConfig
import com.uz.mpalift.mpa_lift_app.module.DBConfig.Companion.refCarSupply
import com.uz.mpalift.mpa_lift_app.module.DBConfig.Companion.reference
import com.uz.mpalift.mpa_lift_app.module.entities.Trip
import com.uz.mpalift.mpa_lift_app.module.entities.TripLocation
import com.uz.mpalift.mpa_lift_app.module.entities.TripRequest


class TripFinderRepository {


    var app= AppExecutors()


    var trips = ArrayList<Trip>()


    var tripLocationsFoundOnPath = ArrayList<Trip>()


    fun isLocationOnPath(pickupLocation : LatLng, trip: Trip):Boolean{
        //1. find the routes on your pathway
        var polylineOptions =  PolylineOptions()
        polylineOptions.startCap(SquareCap())

        var points = ArrayList<LatLng>()


            for(tripRoute in trip.triproute!!){
                points.add(LatLng(tripRoute.latitude!!, tripRoute.longitude!!))
            }
            polylineOptions.addAll(points)

          var isOnPath =  PolyUtil.isLocationOnPath(pickupLocation,polylineOptions.points,true)

           Log.d("PolyPath","MyPathAfter: "+pickupLocation)
           Log.d("PolyPath","path: "+polylineOptions.points.toString())
           Log.d("PolyPath","isOn: "+isOnPath)

        return isOnPath

    }







    fun liftFinder(pickupLocation : LatLng, repoCallback: GetTripsCallback){
        //1. find the routes on your pathway

        // ATTACK THE REPO DATABASE AND SEARCH AND COMPARE THE AVAILABLE DRIVERS
        //getBestTrip(trip)
         Log.d("TripRepo3", "return:")

         // Get the closest driver
         pickup = pickupLocation
         //this method uses the pickup latlng attributes
         //getClosestDriver()

         var run = Runnable {

             val childEventListener = object :
                 ValueEventListener {
                 override fun onDataChange(snapshot: DataSnapshot) {

                     Log.d(
                         "TripRepo2",
                         "return:" + snapshot.childrenCount
                     )
                     for (postSnapshot in snapshot.children) {
                         // TODO: handle the post
                         val trip = postSnapshot.getValue(Trip::class.java)

                         trips.add(trip!!)



                         if(isLocationOnPath(pickupLocation,trip)){
                             tripLocationsFoundOnPath.add(trip)
                         }

                     }


                     //2. Find the closest driver on those ones
                     // after have the trip locations on path

                     //a. create a geo fire for the locations found
                     //b. use your pickup location and find the closest using getCLosestDriver()



                   //  dbRefDriverTrips.removeEventListener(this)
                     if(tripLocationsFoundOnPath.isNotEmpty()){
                         var runMain = Runnable {
                             repoCallback.onTripsReceived(tripLocationsFoundOnPath)
                         }
                         app.mainThread().execute(runMain)

                     }else{
                         var runMain = Runnable {
                             repoCallback.onTripsEmpty("No Vehicle at the moment")
                         }
                         app.mainThread().execute(runMain)

                     }



                 }

                 override fun onCancelled(error: DatabaseError) {
                     repoCallback.onTripsCancelled(error.message)
                     Log.d(
                         "TripRepo1",
                         "onChildChanged:" + error.message
                     )

                 }
             }

          DBConfig.reference.child(refCarSupply).addListenerForSingleValueEvent(childEventListener)

         }
         app.diskIO().execute(run)



     }



    var radius = 0.0
    var driverFound = false
    var driverFoundId  = ""


    var pickup : LatLng? = null

    fun getClosestDriver(){//Aroound the request locations

        // Kyanmbogo road
        //0.3439262,32.5917388 0.343541, 32.590849
        var lat = 0.3439262
        var lon =  32.5917387
        var sampleLatLng = LatLng(lat, lon)//pickup

        var driverLocation = reference.child(DBConfig.refCarSupplyOnline)
        var geofire = GeoFire(driverLocation)

        var geoquery = geofire.queryAtLocation(
            GeoLocation(
                sampleLatLng.latitude,
                sampleLatLng.longitude
            ), radius)
        geoquery.removeAllListeners() // when driver moves thes listners can be called
        geoquery.addGeoQueryEventListener(object:
            GeoQueryEventListener {

            //this method is called whe driver has been found
            override fun onKeyEntered(key: String?, location: GeoLocation?) {
                //Everytime the driver found within the radius the on key is called
                //the key is the trip id
                if(!driverFound){ //change identity of driver
                    driverFound = true
                    driverFoundId = key!!   //this is the key we choose
                    Log.d("TripRepo-K", "Key: " + key)
                }


            }
            //called when drver has not been found
            override fun onGeoQueryReady() {
                if(!driverFound){
                    radius++ // move to next km radius
                    Log.d("TripRepo-R", "Radius: " + radius)
                   getClosestDriver()
                    //after all drivers found this won't be triggerd
                }

            }


            override fun onGeoQueryError(error: DatabaseError?) {

            }




            override fun onKeyExited(key: String?) {

            }

            override fun onKeyMoved(key: String?, location: GeoLocation?) {

            }

        })



    }


    private fun getLocationValue(dataSnapshot: DataSnapshot): GeoLocation? {
        return try {
            val typeIndicator: GenericTypeIndicator<Map<String?, Any?>?> =
                object :
                    GenericTypeIndicator<Map<String?, Any?>?>() {}
            val data =
                dataSnapshot.getValue(
                    typeIndicator
                )!!
            val location = data["l"] as List<*>?
            val latitudeObj = location!![0] as Number
            val longitudeObj = location[1] as Number
            val latitude: Double = latitudeObj.toDouble()
            val longitude: Double = longitudeObj.toDouble()
            if (location.size == 2 && GeoLocation.coordinatesValid(
                    latitude,
                    longitude
                )
            ) {
                GeoLocation(latitude, longitude)
            } else {
                null
            }
        } catch (e: NullPointerException) {
            null
        } catch (e: ClassCastException) {
            null
        }
    }





    interface GetTripsCallback{

        fun onTripsReceived(list:MutableList<Trip>)
        fun onTripsEmpty(msg:String)
        fun onTripsCancelled(error:String)
    }




}