package com.uz.mpalift.mpa_lift_app.module.entities

import com.uz.mpalift.mpa_lift_app.module.userManagement.DateConverter
import java.util.*
import kotlin.collections.ArrayList

class TripHistory {

    var tripId : String ? = null
    var driverId : String ? = null
    var driverName : String ? = null
    var driverPicUrl : String ? = null

    var passengerId : String ? = null
    var passengerName : String ? = null


    var tripOrigin : TripLocation? = null
    var tripDestination : TripLocation? = null


    var distance  : Float = 0F
    var fare : String ? = null


    var passengerTripRating : String = "0.0"
    var driverTripRating : String = "0.0"




    var dateOfTrip : String ? = null


    var tripvehicle : TripVehicle? = null
    var triproute : ArrayList<TripLocation> ? = null



    init {
        triproute = ArrayList()
        dateOfTrip = DateConverter.dateToStringWithoutTime(Date())
    }










}