package com.uz.mpalift.mpa_lift_app.module.tripMgt

import android.location.Location
import android.location.LocationManager
import android.util.Log
import com.firebase.geofire.GeoFire
import com.firebase.geofire.GeoLocation
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.*
import com.google.maps.android.SphericalUtil
import com.uz.mpalift.mpa_lift_app.module.AppExecutors
import com.uz.mpalift.mpa_lift_app.module.DBConfig
import com.uz.mpalift.mpa_lift_app.module.DBConfig.Companion.reference
import com.uz.mpalift.mpa_lift_app.module.entities.Bill
import com.uz.mpalift.mpa_lift_app.module.entities.PendingTrip
import com.uz.mpalift.mpa_lift_app.module.entities.Trip
import com.uz.mpalift.mpa_lift_app.module.entities.TripHistory
import com.uz.mpalift.mpa_lift_app.module.tripRequests.TripRequestsRepository
import com.uz.mpalift.mpa_lift_app.module.tripSupply.TripSupplyRepository


class TripManagementRepository {

    var app = AppExecutors()

    var tripRequestRepository = TripRequestsRepository()





    interface StartTripCallback{

        fun onStarted(trip: Trip)
        fun onFailed(error:String)
    }
    interface endTripCallback{

        fun onEnded()
        fun onFailed(error:String)
    }

    interface RatingTripCallback{

        fun onSubmited()
        fun onFailed(error:String)
    }


    interface OnTripDetailsCallback{

        fun onTripReceived(onTrip: Trip)
        fun onFailed(error:String)
    }

    interface OnTripHistoryDetailsCallback{

        fun onTripHistoryReceived(tripHistory: TripHistory)
        fun onFailed(error:String)
    }




    interface TripReachedCallback{

        fun onSuccess(info:String)
        fun onFailed(error:String)
    }



    interface MonitorTripEndedCallback{

        fun onTripEnded()
        fun onTripEndedFailed(error:String)
    }

    interface RecordTripEndedCallback{

        fun onTripEndedRecorded()
        fun onTripEndedFailed(error:String)
    }







    var listenerSubmitRating :ValueEventListener ? =null
    var refSubmitRating : DatabaseReference ? = null
    fun submitRating(isDriver:Boolean,tripId : String, tripRating:String, callback: RatingTripCallback){

        var run = Runnable {
            var fieldTripRating : String ? = null
            val fieldDriverRating = "driverTripRating"
            val fieldPassengerRating = "passengerTripRating"
            fieldTripRating = if(isDriver){
                fieldDriverRating
            }else{
                fieldPassengerRating
            }

            refSubmitRating =    reference.child(DBConfig.refTripHistory).child(tripId).child(fieldTripRating)
            refSubmitRating?.setValue(tripRating)

            listenerSubmitRating = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {


                    //   remove the tracking database
                    //   databaseReference.child("onTripOnline").onDisconnect()
                    var uithread = Runnable {

                        callback.onSubmited()
                    }
                    app.mainThread().execute(uithread)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {
                        callback.onFailed(databaseError.message)
                    }
                    app.mainThread().execute(uithread)

                }
            }
            refSubmitRating?.addListenerForSingleValueEvent(listenerSubmitRating!!)

        }
        app.diskIO().execute(run)

        refSubmitRating?.removeEventListener(listenerSubmitRating!!)
    }

    fun getOnTripDetails(tripId : String, onTripCallback: OnTripDetailsCallback){

        var run = Runnable {

            val childEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    Log.d("DRIVER-REQUEST-WAIT", "onChildChanged:")
                    // onChildAdded() will be called for each node at the first time
                    val onTrip = snapshot.getValue(Trip::class.java)
                    // Log.d("DRIVER-REQUEST-LISTNER", "onChildChanged:" + request?.passengerId +" tripid: "+request?.tripId +" status: "+request?.isAccepted)
                    var runMain = Runnable {

                        if(onTrip!=null) {
                            onTripCallback.onTripReceived(onTrip)
                        }
                    }
                    app.mainThread().execute(runMain)
                    // MainThreadExecutor(500).execute(runMain)
                }
                override fun onCancelled(error: DatabaseError) {

                }
            }
            // databaseReference.child(DBConfig.refRequests).equalTo(tripId).addListenerForSingleValueEvent(childEventListener)
            reference.child(DBConfig.refCarSupply).child(tripId).addListenerForSingleValueEvent(childEventListener)
            //I had an out of memory error
        }
        app.diskIO().execute(run)
    }

    fun getOnTripHistoryDetails(tripId : String, onTripCallback: OnTripHistoryDetailsCallback){

        var run = Runnable {

            val childEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    Log.d("DRIVER-REQUEST-WAIT", "onChildChanged:")
                    // onChildAdded() will be called for each node at the first time
                    val onTrip = snapshot.getValue(TripHistory::class.java)
                    // Log.d("DRIVER-REQUEST-LISTNER", "onChildChanged:" + request?.passengerId +" tripid: "+request?.tripId +" status: "+request?.isAccepted)
                    var runMain = Runnable {

                        if(onTrip!=null) {
                            onTripCallback.onTripHistoryReceived(onTrip)
                        }
                    }
                    app.mainThread().execute(runMain)
                    // MainThreadExecutor(500).execute(runMain)
                }
                override fun onCancelled(error: DatabaseError) {

                }
            }
            // databaseReference.child(DBConfig.refRequests).equalTo(tripId).addListenerForSingleValueEvent(childEventListener)
            reference.child(DBConfig.refTripHistory).child(tripId).addListenerForSingleValueEvent(childEventListener)
        }
        app.diskIO().execute(run)
    }




    var listenerToStartTrip :ValueEventListener ? =null
    var refStartTrip : DatabaseReference ? = null
    fun startTrip(trip: Trip.StartTrip, startTripCallback: StartTripCallback){

        var run = Runnable {

            refStartTrip =    reference.child(DBConfig.refCarSupply).child(trip.tripId!!).child("startedTrip")
            refStartTrip?.setValue(trip)

            listenerToStartTrip =  object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    var tripOriginLat = trip.tripOrigin!!.latitude
                    var tripOriginLon = trip.tripOrigin!!.longitude

                    var tripDesLat = trip.tripDestination!!.latitude
                    var tripDestLon = trip.tripDestination!!.longitude

                    //convert the double to latlng for maps
                    var locationOrigin =  LatLng(tripOriginLat!!,tripOriginLon!!)
                    var locationDestination =  LatLng(tripDesLat!!,tripDestLon!!)


                    //Store Driver Availability for tracking with the trip id
                    var refOnTripOnline = reference.child(DBConfig.refCarSupplyOnline)
                    val geofireDriverOrigin = GeoFire(refOnTripOnline)
                    geofireDriverOrigin.setLocation(trip.tripId, GeoLocation(locationOrigin.latitude,locationDestination.longitude))


                    //bring back the entire trip
                    getOnTripDetails(trip.tripId!!, object:OnTripDetailsCallback{

                        override fun onTripReceived(onTrip: Trip) {
                            refStartTrip?.removeEventListener(listenerToStartTrip!!)

                            var uithread = Runnable {
                                startTripCallback.onStarted(onTrip)
                            }
                            app.mainThread().execute(uithread)

                        }

                        override fun onFailed(error: String) {

                            var uithread = Runnable {
                                startTripCallback.onFailed(error)
                            }
                            app.mainThread().execute(uithread)

                        }



                    })



                }

                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {
                        startTripCallback.onFailed(databaseError.message)
                    }
                    app.mainThread().execute(uithread)

                }
            }

            refStartTrip?.addListenerForSingleValueEvent(listenerToStartTrip!!)

        }
        app.diskIO().execute(run)

    }


    var listenerToEndTrip :ValueEventListener ? =null
    var refEndTrip : DatabaseReference ? = null
    fun endTrip(trip : Trip, tripRequestId:String, callback: endTripCallback){

        recordEndedTrip(trip.tripId!!,object:RecordTripEndedCallback{

            override fun onTripEndedRecorded() {

                //1. remove request by trip id
                app.diskIO().execute(Runnable {
                    removeRequest(tripRequestId, trip, callback)
                })

            }
            override fun onTripEndedFailed(error: String) {
            }
        })


        //2. remove trip supply online by trip id
        app.diskIO().execute(Runnable {
            removeTrip(trip, callback)
        })

        app.diskIO().execute(Runnable {
            removeTripTrackingOnline(trip,callback)
        })

            //refEndTrip =    reference.child(DBConfig.refCarSupply).child(trip.tripId!!).child("startedTrip").child("tripStatus")
            //Log.d("TripID","ID "+trip.tripId)
            //refEndTrip?.setValue(Trip.StartTrip.TRIP_ENDED)

    }

    private fun removeRequest(tripRequestId:String,trip : Trip,callback: endTripCallback){

        tripRequestRepository.removeTripRequest(tripRequestId,object :TripRequestsRepository.RemoveTripRequestCallback{

            override fun onRequestRemoved() {
                //2. remove trip supply online by trip id
               /*
                app.diskIO().execute(Runnable {
                  //  removeTrip(trip, callback)
                })
                */

            }

            override fun onFailed(error: String) {

            }

        })


    }

    private fun removeTrip(trip : Trip, callback: endTripCallback){


        var reftaks =    reference.child(DBConfig.refCarSupply).child(trip.tripId!!)
        reftaks.removeValue()
        reftaks.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                app.mainThread().execute(Runnable {
                    callback.onEnded()
                })
                /*
                app.diskIO().execute(Runnable {
                    removeTripTrackingOnline(trip,callback)
                })

                 */
            }

            override fun onCancelled(databaseError: DatabaseError) {
                var uithread = Runnable {
                    callback.onFailed(databaseError.message)
                }
                app.mainThread().execute(uithread)

            }
        })

    }

    private fun removeTripTrackingOnline(trip : Trip, callback: endTripCallback){

            var reftaks =    reference.child(DBConfig.refCarSupplyOnline).child(trip.tripId!!)

            reftaks.removeValue()
            reftaks.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                   // refEndTrip?.removeEventListener(listenerToEndTrip!!)

                    //add trip history
                    var ref =    reference.child(DBConfig.refTripHistory).child(trip.tripId!!)

                    val tripHistory = TripHistory()

                    tripHistory.tripId = trip.tripId
                    tripHistory.driverId = trip.driverId
                    tripHistory.driverName = trip.driverName
                    tripHistory.driverPicUrl = trip.driverPicUrl
                    tripHistory.fare = trip.fare

                    tripHistory.dateOfTrip = trip.startedTrip?.dateOfTrip
                    tripHistory.distance = trip.startedTrip?.distance!!
                    tripHistory.passengerName = trip.startedTrip?.passengerName
                    tripHistory.passengerId = trip.startedTrip?.passengerId
                    tripHistory.tripDestination = trip.startedTrip?.tripDestination
                    tripHistory.tripOrigin = trip.startedTrip?.tripOrigin
                    tripHistory.triproute = trip.startedTrip?.triproute
                    tripHistory.tripvehicle = trip.tripvehicle

                    ref.setValue(tripHistory)

                    ref.addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onDataChange(dataSnapshot: DataSnapshot) {

                            Log.d("TripMgtRepo","callback: removeTripOnline")
                            Log.d("TripMgtRepo","tripEndedSuccess")


                        }

                        override fun onCancelled(databaseError: DatabaseError) {

                            Log.d("TripMgtRepo","tripEndedError")

                            var uithread = Runnable {
                                callback.onFailed(databaseError.message)
                            }
                            app.mainThread().execute(uithread)

                        }
                    })


                }

                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {
                        callback.onFailed(databaseError.message)
                    }
                    app.mainThread().execute(uithread)

                }
            })

    }



    private fun recordEndedTrip(tripId: String, callback:RecordTripEndedCallback){

        var myrun = Runnable {

            //add a request and its Id
          var  refDb = reference.child(DBConfig.refEndedTrips).child(tripId)
            refDb.setValue(tripId)

            refDb.addListenerForSingleValueEvent(object :
                ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    callback.onTripEndedRecorded()
                }

                override fun onCancelled(databaseError: DatabaseError) {

                }
            })
        }
        app.diskIO().execute(myrun)

    }








    fun isTripReached(tripId: String, tripDestination: Location, tripReachedCallback: TripReachedCallback){

        //Ill go to carSupplyOnline cars that are comming
        //and i will get the tripId of the request that was accepted
        // and track the car supply online

        //remember the loc is always changing so the method will be called all the time

        var run = Runnable {

            val childEventListener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    // Log.d("DRIVER-REQUEST-WAIT", "onChildChanged:")

                    //val request = snapshot.getValue(TripRequest::class.java)

                    if(dataSnapshot.exists()){

                        val location = GeoFire.getLocationValue(dataSnapshot)


                        var driverLocation = Location(LocationManager.GPS_PROVIDER)
                        driverLocation.latitude = location.latitude
                        driverLocation.longitude = location.longitude

                        Log.d("TripRepo-D","Data Exists; distance")

                        var latLngOrigin = LatLng(location.latitude, location.longitude)
                        var latLngDes = LatLng(tripDestination.latitude, tripDestination.longitude)

                        // var distance =driverLocation.distanceTo(tripDestination)
                        var distance = findDistance(latLngOrigin,latLngDes)

                        if(distance < 20){
                            var moreD = "less KM left: "+distance
                            var runMain = Runnable {
                                tripReachedCallback.onSuccess(moreD)
                            }
                            app.mainThread().execute(runMain)
                        }else{
                            var moreD = "more KM left: "+distance
                            var runMain = Runnable {
                                tripReachedCallback.onSuccess(moreD)
                            }
                            app.mainThread().execute(runMain)
                        }
                    }

                }

                override fun onCancelled(error: DatabaseError) {
                    tripReachedCallback.onFailed(error.message)
                }
            }

            // databaseReference.child(DBConfig.refRequests).equalTo(tripId).addListenerForSingleValueEvent(childEventListener)

            reference.child(DBConfig.refCarSupplyOnline).child(tripId).addValueEventListener(childEventListener)

        }
        app.diskIO().execute(run)

    }


    fun findDistance(origin: LatLng, destination: LatLng): Float{

        //Calculating the distance in meters
        //Calculating the distance in meters
        var distance: Double = SphericalUtil.computeDistanceBetween(origin, destination)

        var d = distance/1000

        return d.toFloat()
    }



    fun monitorTripEnded(tripId:String, monitorTripEndedCallback: MonitorTripEndedCallback) {

        // The driver will track a req db by his trip Id
        // So the request child ref should be headed by a trip Id
        var run = Runnable {

            val childEventListener = object :
                ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {

                    // onChildAdded() will be called for each node at the first time
                   // val ontrip = snapshot.getValue(Trip::class.java)

                    //   Log.d("DRIVER-REQUEST-LISTNER", "onChildChanged:" + request?.passengerId +" tripid: "+request?.tripId +" status: "+request?.isAccepted)
                    var runMain = Runnable {

                        if (snapshot.value != null) {
                            monitorTripEndedCallback.onTripEnded()
                        }

                    }
                    app.mainThread().execute(runMain)
                    // MainThreadExecutor(500).execute(runMain)


                }

                override fun onCancelled(error: DatabaseError) {

                }
            }

            // database.getReference(DBConfig.refOnTrip)
            reference.child(DBConfig.refEndedTrips).child(tripId)
                .addValueEventListener(childEventListener)

        }
        app.diskIO().execute(run)


    }



    /////////////////   CALLBACKS ///////////////////////////////////////////////////////////







}