package com.uz.mpalift.mpa_lift_app.module

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.uz.mpalift.mpa_lift_app.module.userManagement.UserRepository


class DBConfig {

    companion object{

        private var database : FirebaseDatabase = FirebaseDatabase.getInstance()
        var user  = FirebaseAuth.getInstance().currentUser

        var  firebaseStorage = FirebaseStorage.getInstance()

        var reference = database.reference


        //Trip Databases
        var refRequests = "tripRequests"
        var refCarSupply = "tripSupply"
        var refCarSupplyOnline = "tripSupplyOnline"

        //Databases
        var vehiclesdB = "vehicles"


        var usersDb = "users"
        var registrar = "registrar"
        var driverLicenceDb = "userDriverLicence"

        var refPendingTrips = "pendingTrips"

        var refEndedTrips = "endedTrips"



        var refTripHistory = "tripHistory"

        var refWallet = "wallet"
        var refWalletTransaction = "walletTransaction"

        var refBill = "bill"




    }


}