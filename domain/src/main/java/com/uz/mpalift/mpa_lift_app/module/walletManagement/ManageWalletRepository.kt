package com.uz.mpalift.mpa_lift_app.module.walletManagement

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.uz.mpalift.mpa_lift_app.module.AppExecutors
import com.uz.mpalift.mpa_lift_app.module.DBConfig
import com.uz.mpalift.mpa_lift_app.module.entities.WalletAccount
import java.math.BigDecimal

class ManageWalletRepository {



    var app = AppExecutors()


   // var hashMap : HashMap<String, Int> = HashMap<String, Int> ()

    interface CreateWalletCallback{

        fun onWalletCreated()
        fun onFailed(error:String)
    }
    interface GetWalletBalanceCallback{

        fun onGetWalletBalance(balance:BigDecimal)
        fun onFailed(error:String)
    }
    interface DepositMoneyCallback{

        fun onMoneyDeposited()
        fun onFailed(error:String)
    }

    interface TransactPaymentCallback{

        fun onPaymentTransacted(transaction: WalletAccount.AccountTransaction)
        fun onFailed(error:String)
    }



    fun getWalletBalance(accountUserId: String,callback: GetWalletBalanceCallback){

        var listenerWallet : ValueEventListener? =null
        var refWallet : DatabaseReference? = null

        var run = Runnable {

            refWallet =    DBConfig.reference.child(DBConfig.refWallet).child(accountUserId)

            listenerWallet = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    var wallet = dataSnapshot.getValue(WalletAccount::class.java)
                    if(wallet!=null){
                        var uithread = Runnable {

                            var big = BigDecimal(wallet.accBalance)
                            callback.onGetWalletBalance(big)
                        }
                        app.mainThread().execute(uithread)

                    }

                }

                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {
                        callback.onFailed(databaseError.message)
                    }
                    app.mainThread().execute(uithread)

                }
            }
            refWallet?.addListenerForSingleValueEvent(listenerWallet!!)
        }
        app.diskIO().execute(run)

       // refWallet?.removeEventListener(listenerWallet!!)
    }


    fun depositMoneyToAccount(accountUserId: String,amount: BigDecimal, callback: DepositMoneyCallback){
        var listenerWallet : ValueEventListener? =null
        var refWallet : DatabaseReference? = null
        var run = Runnable {

            refWallet =    DBConfig.reference.child(DBConfig.refWallet).child(accountUserId).child("accBalance")
            refWallet?.setValue(amount.toString())

            listenerWallet = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                   refWallet?.removeEventListener(listenerWallet!!)
                    //   remove the tracking database
                    //   databaseReference.child("onTripOnline").onDisconnect()
                    var uithread = Runnable {
                        callback.onMoneyDeposited()
                        refWallet?.removeEventListener(listenerWallet!!)
                    }
                    app.mainThread().execute(uithread)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {
                        callback.onFailed(databaseError.message)
                    }
                    app.mainThread().execute(uithread)

                }
            }
            refWallet?.addListenerForSingleValueEvent(listenerWallet!!)
        }
        app.diskIO().execute(run)


    }




    //var amountToDriver =  BigDecimal(amountToDriver.toDouble())
    //depositMoneyToAccount(driverUserId,amountToDriver)


    fun transactPayment(passengerUserId: String, driverUserId: String,amountToDriver: String, callback: TransactPaymentCallback){

        Log.d("PassId","IdPass: "+passengerUserId)
        Log.d("PassId","IdDriver: "+driverUserId)
        Log.d("PassAmount","IdAmount: "+amountToDriver)

        //1. transact from passenger
        getWalletBalance(passengerUserId,object:  GetWalletBalanceCallback{

            override fun onGetWalletBalance(balance: BigDecimal) {

                // Send this money to pass account
                var amountA = balance - BigDecimal(amountToDriver.toDouble())
                depositMoneyToAccount(passengerUserId,amountA, object: DepositMoneyCallback{

                    override fun onMoneyDeposited() {
                        //2. transact to passenger
                        transactToDriver(passengerUserId,driverUserId,amountToDriver,callback)
                    }

                    override fun onFailed(error: String) {

                    }

                })

            }

            override fun onFailed(error: String) {

            }
        })

    }

    private fun transactToDriver(payerId:String,driverUserId: String,amountToDriver:String,callback: TransactPaymentCallback){

        getWalletBalance(driverUserId,object:  GetWalletBalanceCallback{

            override fun onGetWalletBalance(balance: BigDecimal) {

                // Send this money to pass account
                var amountA = balance + BigDecimal(amountToDriver.toDouble())
                depositMoneyToAccount(driverUserId,amountA, object: DepositMoneyCallback{

                    override fun onMoneyDeposited() {

                        var walletTransact = WalletAccount.AccountTransaction()
                        walletTransact.payerId = payerId
                        walletTransact.receiverId = driverUserId
                        walletTransact.amount = amountToDriver

                        recordWalletTransaction(walletTransact,callback)


                    }

                    override fun onFailed(error: String) {

                    }

                })

            }

            override fun onFailed(error: String) {

            }

        })
    }


     fun removeWalletDatabase(){

        var refWalletT =    DBConfig.reference.child(DBConfig.refWalletTransaction)

        var run = Runnable {


            refWalletT.removeValue()

            val listenerWallet = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                }

                override fun onCancelled(databaseError: DatabaseError) {

                }
            }
            refWalletT.addListenerForSingleValueEvent(listenerWallet)
        }
        app.diskIO().execute(run)

       // refWalletT.removeEventListener(listenerWallet!!)


    }


    var listenerWalletRecord : ValueEventListener? =null
    var  refWalletT : DatabaseReference? = null

    fun removeRecordWalletTransactionListener(){
        if(refWalletT!=null && listenerWalletRecord!=null) {
            refWalletT!!.removeEventListener(listenerWalletRecord!!)
        }
    }
    fun resetX(){
        x = 1
    }

    var x = 1
    private fun recordWalletTransaction(walletTransaction:WalletAccount.AccountTransaction,callback: TransactPaymentCallback){

        refWalletT =    DBConfig.reference.child(DBConfig.refWalletTransaction).child(walletTransaction.transactionId)

        var run = Runnable {
            refWalletT!!.setValue(walletTransaction)
            listenerWalletRecord = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                //    removeRecordWalletTransactionListener()
                    if(x==1) {
                        x++
                        var uithread = Runnable {
                            callback.onPaymentTransacted(walletTransaction)
                        }
                        app.mainThread().execute(uithread)
                    }

                }
                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {
                        callback.onFailed(databaseError.message)
                    }
                    app.mainThread().execute(uithread)
                }
            }
            refWalletT!!.addListenerForSingleValueEvent(listenerWalletRecord!!)
        }
        app.diskIO().execute(run)
    }

    var listenerWallet : ValueEventListener? =null
    var refWallet : DatabaseReference? = null
    fun createWalletAccount(account: WalletAccount, callback: CreateWalletCallback){

        var run = Runnable {

            refWallet =    DBConfig.reference.child(DBConfig.refWallet).child(account.accUserId!!)
            refWallet?.setValue(account)

            listenerWallet = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {


                    //   remove the tracking database
                    //   databaseReference.child("onTripOnline").onDisconnect()
                    var uithread = Runnable {
                        callback.onWalletCreated()
                    }
                    app.mainThread().execute(uithread)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {
                        callback.onFailed(databaseError.message)
                    }
                    app.mainThread().execute(uithread)

                }
            }
            refWallet?.addListenerForSingleValueEvent(listenerWallet!!)
        }
        app.diskIO().execute(run)

        refWallet?.removeEventListener(listenerWallet!!)
    }



}