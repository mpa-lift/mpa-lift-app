package com.uz.mpalift.mpa_lift_app.module.entities

import com.uz.mpalift.mpa_lift_app.module.userManagement.DateConverter
import java.math.BigDecimal
import java.util.*
import kotlin.collections.ArrayList


class WalletAccount {


    var accUserId : String ? = null
    var accUserName : String ? = null
    var accNumber :String ? = null
    var accBalance: String? = null
    var transaction : ArrayList<AccountTransaction> ? = null

    init {
        transaction = ArrayList()
        accBalance = "0.0"
        accNumber = UUID.randomUUID().toString()
    }



    /*
    fun isAccountBalanceMinimum():Boolean{
        val minimumBalanceToTransact = BigDecimal("10000")

        var big = BigDecimal(accBalance)

        if(big.toFloat() >= minimumBalanceToTransact.toFloat()){
            return true
        }
        return false
    }

     */



    class AccountTransaction{
        var transactionId: String = UUID.randomUUID().toString()
        var amount : String = "0.0"
        var payerId : String ? = null
        var receiverId: String ? = null
        var dateOfTransaction = DateConverter.dateToStringWithoutTime(Date())
    }


}