package com.uz.mpalift.mpa_lift_app.module.entities

import java.util.*

class TripVehicle {



    var vehicleId : String? = null
    var vehicleName : String? = null
    var vehicleNoPlate : String? = null
    var vehicleSeats: String? = null
    var vehiclePhoto: String? = null


    var driverId : String? = null

    init {
        vehicleId = UUID.randomUUID().toString()
    }


}