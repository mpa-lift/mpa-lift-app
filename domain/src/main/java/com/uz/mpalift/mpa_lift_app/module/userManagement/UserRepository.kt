package com.uz.mpalift.mpa_lift_app.module.userManagement

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.uz.mpalift.mpa_lift_app.module.AppExecutors
import com.uz.mpalift.mpa_lift_app.module.DBConfig
import com.uz.mpalift.mpa_lift_app.module.DBConfig.Companion.driverLicenceDb
import com.uz.mpalift.mpa_lift_app.module.DBConfig.Companion.reference
import com.uz.mpalift.mpa_lift_app.module.entities.*
import com.uz.mpalift.mpa_lift_app.module.walletManagement.ManageWalletRepository
import com.uz.mpalift.mpa_lift_app.module.userManagement.DateConverter.Companion.dateToStringWithoutTime
import java.util.*
import kotlin.collections.ArrayList

class UserRepository {


    var app = AppExecutors()

    fun registerAsDriver(driver: DrivingLicence, registerDriverCallback: RegisterDriverCallback){

        reference.child(driverLicenceDb).child(driver.licenceId!!).setValue(driver)

        var run = Runnable {

            var reftaks =     reference.child(driverLicenceDb).child(driver.licenceId!!)
            reftaks.setValue(driver)
            reftaks.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    activateLicence(driver.userId!!,registerDriverCallback)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {
                        registerDriverCallback.onError(databaseError.message)
                    }
                    app.mainThread().execute(uithread)
                }
            })

        }
        app.diskIO().execute(run)

    }

    private fun activateLicence(passengerId: String, registerDriverCallback: RegisterDriverCallback){
            var run = Runnable {
                var reftaks =    reference.child(DBConfig.usersDb).child(passengerId).child("drivingLicenceActivated")
                reftaks.setValue(true)
                reftaks.addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {

                        var uithread = Runnable {
                            registerDriverCallback.onSuccess()
                        }
                        app.mainThread().execute(uithread)

                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        var uithread = Runnable {
                            registerDriverCallback.onError(databaseError.message)
                        }
                        app.mainThread().execute(uithread)
                    }
                })
            }
            app.diskIO().execute(run)

    }


    fun registerUserForFCM(passengerId: String,token:String, registerUserForFCMCallback: RegisterUserForFCMCallback){

        app.diskIO().execute(Runnable {

            var reftaks =    reference.child(DBConfig.usersDb).child(passengerId).child("fcmToken")
            reftaks.setValue(token)
            reftaks.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    var uithread = Runnable {
                        registerUserForFCMCallback.onRegister()
                    }
                    app.mainThread().execute(uithread)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {
                        registerUserForFCMCallback.onError(databaseError.message)
                    }
                    app.mainThread().execute(uithread)
                }
            })

        })


    }


    fun registerPassenger(user: MpaLiftUser, registerPassengerCallback: RegisterPassengerCallback){
        var run = Runnable {
            var reftaks =    reference.child(DBConfig.usersDb).child(user.passengerId!!)

            reftaks.setValue(user)
            reftaks.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    var registra = Registrar()
                    registra.userId= user.passengerId
                    registra.userName = user.passengerFirstName
                    registra.registrationDate = dateToStringWithoutTime(Date())
                    recordRegistra(registra,object: RecordRegisterUserCallback{

                        override fun onRegister(registrar: Registrar) {


                            createPassengerWallet(registrar,registerPassengerCallback)

                        }

                        override fun onError(error: String) {
                            var uithread = Runnable {
                                registerPassengerCallback.onError(error)
                            }
                            app.mainThread().execute(uithread)
                        }

                    })

                   }

                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {
                        registerPassengerCallback.onError(databaseError.message)
                    }
                    app.mainThread().execute(uithread)
                }
            })
        }
        app.diskIO().execute(run)
    }

    private fun createPassengerWallet(registrar: Registrar,registerPassengerCallback: RegisterPassengerCallback){

        val walletRepository = ManageWalletRepository()

        val acc = WalletAccount()
        acc.accUserId = registrar.userId
        acc.accUserName = registrar.userName

        walletRepository.createWalletAccount(acc, object : ManageWalletRepository.CreateWalletCallback{

            override fun onWalletCreated() {

                var uithread = Runnable {
                    registerPassengerCallback.onSuccess()
                }
                app.mainThread().execute(uithread)

            }

            override fun onFailed(error: String) {

                var uithread = Runnable {
                    registerPassengerCallback.onError(error)
                }
                app.mainThread().execute(uithread)

            }

        })

    }

    fun getPassengerDetails(passengerId: String,getPassengerDetailsCallback: GetPassengerDetailsCallback){

        // ATTACK THE REPO DATABASE AND GET SAVED CARS
        var run = Runnable {
            val childEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    // Log.d("DRIVER-REQUEST-WAIT", "onChildChanged:")
                    var passenger = snapshot.getValue(MpaLiftUser::class.java)
                    if(passenger!=null) {

                        getLicenceDetails(passenger, getPassengerDetailsCallback)
                    }

                }
                override fun onCancelled(error: DatabaseError) {

                    getPassengerDetailsCallback.onFailed(error.message)

                }
            }
            reference.child(
                DBConfig.usersDb
            ).child(passengerId).addListenerForSingleValueEvent(childEventListener)
        }
        app.diskIO().execute(run)
    }

    private fun getLicenceDetails(passenger:MpaLiftUser, getPassengerDetailsCallback: GetPassengerDetailsCallback){

        // ATTACK THE REPO DATABASE AND GET SAVED CARS
        var run = Runnable {

            val childEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    // Log.d("DRIVER-REQUEST-WAIT", "onChildChanged:")
                    val driverLicence = snapshot.getValue(DrivingLicence::class.java)

                    if(passenger.isDrivingLicenceActivated){
                        passenger.drivingLicence = driverLicence

                        var runMain = Runnable {
                            getPassengerDetailsCallback.onGetPassengerDetails(passenger)
                        }
                        app.mainThread().execute(runMain)


                    }else{

                        var runMain = Runnable {
                            getPassengerDetailsCallback.onGetPassengerDetails(passenger)
                        }
                        app.mainThread().execute(runMain)

                    }

                }
                override fun onCancelled(error: DatabaseError) {

                    getPassengerDetailsCallback.onFailed(error.message)

                }
            }
            reference.child(
                DBConfig.driverLicenceDb
            ).child(passenger.passengerId!!).addListenerForSingleValueEvent(childEventListener)
        }
        app.diskIO().execute(run)
    }






    fun isUserPasswordValid(userId:String,userPassword:String, isUserPasswordValid: IsUserPasswordValidCallback){

            // ATTACK THE REPO DATABASE AND GET SAVED CARS
            var run = Runnable {

                val childEventListener = object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {

                        if (snapshot.value != null){ // Driver
                            //it means user already registered
                            val user = snapshot.getValue(MpaLiftUser::class.java)
                            val pass = user!!.passengerPassword!!

                            /*
                            var messageAfterDecrypt : String ? = null

                            try {
                                messageAfterDecrypt = AESCrypt.decrypt(UserSecurityUtils.key, driver.driverPassword)
                            } catch (e: GeneralSecurityException) { //handle error - could be due to incorrect password or tampered encryptedMsg
                                Log.d("USerPass","security err"+e.message)
                            }

                             */

                            if(pass == userPassword){
                                Log.d("USerPass","Password  match net:"+pass.toString()+" user: "+userPassword)
                                    isUserPasswordValid.isValid(true)
                            }else{

                                Log.d("USerPass","Password  don't match net:"+pass.toString()+" user: "+userPassword)
                                var uithread = Runnable {
                                    isUserPasswordValid.isValid(false)
                                }
                                app.mainThread().execute(uithread)
                            }

                        }else{  // Passenger
                            //It is new users
                            //write an entry to your user table
                            Log.d("USerPass","Password  passenger")
                           //  checkPassenger(userId,userPassword,isUserPasswordValid)
                        }

                    }
                    override fun onCancelled(error: DatabaseError) {
                        Log.d("UserPass", "on canceled:"+error.message)
                        var uithread = Runnable {
                            isUserPasswordValid.onError(error.message)
                        }
                        app.mainThread().execute(uithread)

                    }
                }
                // databaseReference.child(DBConfig.refRequests).equalTo(tripId).addListenerForSingleValueEvent(childEventListener)
                reference.child(DBConfig.usersDb).child(userId).addListenerForSingleValueEvent(childEventListener)
            }
            AppExecutors().diskIO().execute(run)

    }

    fun isUserDriver(userId:String, isUserDriverCallback: isUserDriverCallback){

        Log.d("LicenceDriver","yes in: ")

        // ATTACK THE REPO DATABASE AND GET SAVED CARS
        var run = Runnable {

            val childEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {

                    // onChildAdded() will be called for each node at the first time
                    if (snapshot.value != null){ // Driver
                        //it means user already registered
                        //Add code to show your prompt
                        val passenger = snapshot.getValue(MpaLiftUser::class.java)

                        if(passenger!!.isDrivingLicenceActivated){


                            Log.d("LicenceDriver","yes: "+passenger.passengerFirstName)

                            var uithread = Runnable {
                                isUserDriverCallback.isDriver(true)
                            }
                            app.mainThread().execute(uithread)

                        }else{

                            var uithread = Runnable {
                                isUserDriverCallback.isDriver(false)
                            }
                            app.mainThread().execute(uithread)

                        }
                    }

                }
                override fun onCancelled(error: DatabaseError) {
                    Log.d("DRIVER-GetStartedl", "on canceled:"+error.message)
                    var uithread = Runnable {
                        isUserDriverCallback.onError(error.message)
                    }
                    app.mainThread().execute(uithread)

                }
            }
            // databaseReference.child(DBConfig.refRequests).equalTo(tripId).addListenerForSingleValueEvent(childEventListener)
            DBConfig.reference.child(DBConfig.usersDb).child(userId).addListenerForSingleValueEvent(childEventListener)
        }
        AppExecutors().networkIO().execute(run)

    }




    //////////////////////////////////////// registrar /////////////////////////////////////

    private fun recordRegistra(registrar: Registrar, recordRegisterUserCallback: RecordRegisterUserCallback){
        var run = Runnable {
            var reftaks =    reference.child(DBConfig.registrar).child(registrar.userId!!)

            reftaks.setValue(registrar)
            reftaks.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    var uithread = Runnable {
                        recordRegisterUserCallback.onRegister(registrar)
                    }
                    app.mainThread().execute(uithread)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {
                       recordRegisterUserCallback.onError(databaseError.message)
                    }
                    app.mainThread().execute(uithread)
                }
            })
        }
        app.diskIO().execute(run)
    }

    fun isUserRegistered(userId:String, isUserRegisteredCallback: IsUserRegisteredCallback){
        Log.d("USerPass","Password  begin")

        // ATTACK THE REPO DATABASE AND GET SAVED CARS
        var run = Runnable {

            val childEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {

                    if (snapshot.value != null){ // Driver
                        //it means user already registered
                        val registrar = snapshot.getValue(Registrar::class.java)
                        var uithread = Runnable {
                            isUserRegisteredCallback.isRegistered(true)
                        }
                        app.mainThread().execute(uithread)

                    }else{  // Passenger
                        //It is new users
                        //write an entry to your user table
                        var uithread = Runnable {
                            isUserRegisteredCallback.isRegistered(false)
                        }
                        app.mainThread().execute(uithread)
                    }

                }
                override fun onCancelled(error: DatabaseError) {
                    Log.d("UserPass", "on canceled:"+error.message)
                    var uithread = Runnable {
                        isUserRegisteredCallback.onError(error.message)
                    }
                    app.mainThread().execute(uithread)
                }
            }
            // databaseReference.child(DBConfig.refRequests).equalTo(tripId).addListenerForSingleValueEvent(childEventListener)
            reference.child(DBConfig.registrar).child(userId).addListenerForSingleValueEvent(childEventListener)
        }
        AppExecutors().diskIO().execute(run)

    }








    fun registerTripVehicle(vehicle: TripVehicle, registerTripVehicleCallback: RegisterTripVehicleCallback){

      saveVehicle(vehicle,registerTripVehicleCallback)

    }


    private fun saveVehicle(vehicle: TripVehicle, registerTripVehicleCallback: RegisterTripVehicleCallback){

        var run = Runnable {

            var reftaks =    reference.child(
                DBConfig.vehiclesdB
            ).child(vehicle.vehicleId!!)
            reftaks.setValue(vehicle)
            reftaks.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    var uithread = Runnable {
                        registerTripVehicleCallback.onSuccess()
                    }
                    app.mainThread().execute(uithread)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {
                        registerTripVehicleCallback.onError(databaseError.message)
                    }
                    app.mainThread().execute(uithread)
                }
            })

        }
        app.diskIO().execute(run)


    }


     var vehicles = ArrayList<TripVehicle>()
    fun getRegisteredCars(driverId: String,getDriverVehiclesCallback: GetDriverVehiclesCallback) {

        // ATTACK THE REPO DATABASE AND GET SAVED CARS
        var run = Runnable {

            val childEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    Log.d("DRIVER-REQUEST-WAIT", "onChildChanged:")
                    // onChildAdded() will be called for each node at the first time
                    for (postSnapshot in snapshot.children) {
                        // TODO: handle the post
                        val vehicle = postSnapshot.getValue(TripVehicle::class.java)
                        if(vehicle?.driverId == driverId) {
                            vehicles.add(vehicle)
                        }
                    }
                    // Log.d("DRIVER-REQUEST-LISTNER", "onChildChanged:" + request?.passengerId +" tripid: "+request?.tripId +" status: "+request?.isAccepted)
                    var runMain = Runnable {

                        if(vehicles.isNotEmpty()) {

                            getDriverVehiclesCallback.onResponded(vehicles)

                        }else{

                            getDriverVehiclesCallback.onResponseEmpty()
                        }
                    }
                    app.mainThread().execute(runMain)
                    // MainThreadExecutor(500).execute(runMain)
                }
                override fun onCancelled(error: DatabaseError) {

                    getDriverVehiclesCallback.onResponseFailed(error.message)

                }
            }
            // databaseReference.child(DBConfig.refRequests).equalTo(tripId).addListenerForSingleValueEvent(childEventListener)
            reference.child(
                DBConfig.vehiclesdB
            ).addValueEventListener(childEventListener)
        }
        app.diskIO().execute(run)



    }




    interface RegisterUserForFCMCallback{

        fun onRegister()
        fun onError(error: String)

    }




    interface IsUserRegisteredCallback{

        fun isRegistered(boolean: Boolean)
        fun onError(error: String)

    }

    interface RecordRegisterUserCallback{

        fun onRegister(registrar: Registrar)
        fun onError(error: String)

    }



    interface IsUserPasswordValidCallback{

        fun isValid(boolean: Boolean)
        fun onError(error: String)

    }

    interface isUserDriverCallback{

        fun isDriver(boolean: Boolean)
        fun onError(error: String)

    }



    interface RegisterDriverCallback{

        fun onSuccess()
        fun onError(error: String)

    }



    interface GetDriverDetailsCallback{
        fun onGetDriverDetails(driver : DrivingLicence)
        fun onFailed(error:String)
    }



    interface RegisterTripVehicleCallback{

        fun onSuccess()
        fun onError(error: String)

    }


    interface GetDriverVehiclesCallback{

        fun onResponded(vehicles: MutableList<TripVehicle>)
        fun onResponseEmpty()
        fun onResponseFailed(error:String)
    }


    interface RegisterPassengerCallback{

        fun onSuccess()
        fun onError(error: String)

    }


    interface GetPassengerDetailsCallback{
        fun onGetPassengerDetails(user : MpaLiftUser)
        fun onFailed(error:String)
    }



}