package com.uz.mpalift.mpa_lift_app.module.entities

import java.util.*

class Bill {

    var billId: String = UUID.randomUUID().toString()
    var fare: String? = null
    var userId: String? = null
    var userName: String? = null
    var tripId: String? = null
    var distance: String? = null



}