package com.uz.mpalift.mpa_lift_app.module.tripSupply

import android.location.Location
import android.location.LocationManager
import android.util.Log
import com.firebase.geofire.GeoFire
import com.firebase.geofire.GeoLocation
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.*
import com.uz.mpalift.mpa_lift_app.module.AppExecutors
import com.uz.mpalift.mpa_lift_app.module.DBConfig
import com.uz.mpalift.mpa_lift_app.module.DBConfig.Companion.reference
import com.uz.mpalift.mpa_lift_app.module.entities.PendingTrip
import com.uz.mpalift.mpa_lift_app.module.entities.Trip
import com.uz.mpalift.mpa_lift_app.module.entities.TripRequest
import com.uz.mpalift.mpa_lift_app.module.tripMgt.TripManagementRepository


class TripSupplyRepository {



    var app = AppExecutors()


    interface RemovePendingTripCallback{

        fun onSuccess()
        fun onFailed(error:String)

    }
    interface RecordPendingTripCallback{

        fun onSuccess()
        fun onFailed(error:String)

    }
    interface GetPendingTripCallback{

        fun onGetPendingTrip(pendingTrip: PendingTrip)
        fun onFailed(error:String)

    }




    interface OnTripCreatedCallback{

        fun onSuccess()
        fun onFailed(error:String)
    }


    private interface OnCheckTripActive{
        fun onCheckActive(isActive: Boolean)
        fun onFailed(error:String)
    }



    //used when you are resuming monitoring your requests
   private fun getPendingTrip(userId:String, getPendingTripCallback:GetPendingTripCallback) {

        // The driver will track a req db by his trip Id
        // So the request child ref should be headed by a trip Id
        var run = Runnable {

            val childEventListener = object :
                ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    // onChildAdded() will be called for each node at the first time
                    val request = snapshot.getValue(PendingTrip::class.java)

                    Log.d("MyPendingRequestx","useridG:  "+userId)

                    if(request!=null) {
                        Log.d("MyPendingRequest",request.tripId!!)
                        var runMain = Runnable {
                            getPendingTripCallback.onGetPendingTrip(request)
                        }
                        app.mainThread().execute(runMain)
                    }else{
                        getPendingTripCallback.onFailed("No requests!")
                    }
                    // MainThreadExecutor(500).execute(runMain)
                }

                override fun onCancelled(error: DatabaseError) {

                    var runMain = Runnable {

                        getPendingTripCallback.onFailed(error.message)

                    }
                    app.mainThread().execute(runMain)

                }
            }
            reference.child(DBConfig.refPendingTrips).child(userId)
                .addValueEventListener(childEventListener)

        }
        app.diskIO().execute(run)


    }
    private fun recordPendingTrip(pendingTrip: PendingTrip, callback:RecordPendingTripCallback){

        var myrun = Runnable {

            //add a request and its Id
            reference.child(DBConfig.refPendingTrips).child(pendingTrip.userId!!).setValue(pendingTrip)
            reference.addListenerForSingleValueEvent(object :
                ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    app.mainThread().execute(Runnable {
                        callback.onSuccess()
                    })
                }

                override fun onCancelled(databaseError: DatabaseError) {

                    app.mainThread().execute(Runnable {
                        callback.onFailed(databaseError.message)
                    })


                }
            })
        }
        app.diskIO().execute(myrun)

    }

    fun removePendingTrip(userId: String, callback:RemovePendingTripCallback){

        var myrun = Runnable {

            //add a request and its Id
            reference.child(DBConfig.refPendingTrips).child(userId).removeValue()
            reference.addListenerForSingleValueEvent(object :
                ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    app.mainThread().execute(Runnable {
                        callback.onSuccess()
                    })
                }

                override fun onCancelled(databaseError: DatabaseError) {

                    app.mainThread().execute(Runnable {
                        callback.onFailed(databaseError.message)
                    })


                }
            })
        }
        app.diskIO().execute(myrun)

    }



    fun createTrip(trip: Trip, onTripCreatedCallback: OnTripCreatedCallback){


        removeAllEndedTrips()

        var tripOriginLat =  trip.tripOrigin!!.latitude
        var tripOriginLon =  trip.tripOrigin!!.longitude

        var tripDesLat =  trip.tripDestination!!.latitude
        var tripDestLon =  trip.tripDestination!!.longitude


        //convert the double to latlng for maps
        var locationOrigin =  LatLng(tripOriginLat!!,tripOriginLon!!)
        var locationDestination =  LatLng(tripDesLat!!,tripDestLon!!)
        //  MapUtils(this).saveTrip( DBConfig.database.reference, DBConfig.user!!,it)
        var run = Runnable {

            var reftaks =   reference.child(DBConfig.refCarSupply).child(trip.tripId!!)
            reftaks.setValue(trip)
            reftaks.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {



                    //Store Driver Availability for tracking with the trip id
                    var dbRefDriverTripsAvailable =  FirebaseDatabase.getInstance().getReference(
                        DBConfig.refCarSupplyOnline)
                    val geofireDriverOrigin = GeoFire(dbRefDriverTripsAvailable)
                    geofireDriverOrigin.setLocation( trip.tripId, GeoLocation(locationOrigin.latitude,locationDestination.longitude))


                    var uithread = Runnable {

                        onTripCreatedCallback.onSuccess()

                    }
                    app.mainThread().execute(uithread)




                    /*
                    val pendingTrip = PendingTrip()
                    pendingTrip.tripId = trip.tripId
                    pendingTrip.userId = trip.driverId

                    recordPendingTrip(pendingTrip, object : RecordPendingTripCallback{

                        override fun onSuccess() {
                        }

                        override fun onFailed(error: String) {
                            onTripCreatedCallback.onFailed(error)
                        }

                    })
                    */


                }

                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {

                        onTripCreatedCallback.onFailed(databaseError.message)
                    }
                    app.mainThread().execute(uithread)
                }
            })

        }
        app.diskIO().execute(run)
    }





    private fun removeAllEndedTrips(){

        var myrun = Runnable {

            //add a request and its Id
            var  refDb = reference.child(DBConfig.refEndedTrips)
            refDb.removeValue()

            refDb.addListenerForSingleValueEvent(object :
                ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {


                }

                override fun onCancelled(databaseError: DatabaseError) {

                }
            })
        }
        app.diskIO().execute(myrun)

    }








    private fun isTripStillActive(tripId: String, onCheckTripActive: OnCheckTripActive){

        var run = Runnable {

            var reftaks =   reference.child(DBConfig.refCarSupply).child(tripId)
            reftaks.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    val trip = dataSnapshot.getValue(Trip::class.java)
                    // The Trip is Still On
                    if(trip?.startedTrip?.tripStatus == Trip.StartTrip.TRIP_INACTIVE &&
                        trip.startedTrip?.tripStatus == Trip.StartTrip.TRIP_STARTED){

                        onCheckTripActive.onCheckActive(true)
                    }else{
                        onCheckTripActive.onCheckActive(false)
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {
                        onCheckTripActive.onFailed(databaseError.message)
                }
            })

        }
        app.diskIO().execute(run)

    }

    fun getRegisteredCars(){


    }


}