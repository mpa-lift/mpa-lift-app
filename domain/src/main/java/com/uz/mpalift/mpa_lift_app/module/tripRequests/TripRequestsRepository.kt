package com.uz.mpalift.mpa_lift_app.module.tripRequests

import android.location.Location
import android.location.LocationManager
import android.util.Log
import com.firebase.geofire.GeoFire
import com.firebase.geofire.GeoLocation
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.*
import com.uz.mpalift.mpa_lift_app.module.AppExecutors
import com.uz.mpalift.mpa_lift_app.module.DBConfig
import com.uz.mpalift.mpa_lift_app.module.DBConfig.Companion.reference
import com.uz.mpalift.mpa_lift_app.module.entities.TripResponse
import com.uz.mpalift.mpa_lift_app.module.entities.TripRequest
import com.uz.mpalift.mpa_lift_app.module.tripSupply.TripSupplyRepository


/*
    interface SendRequestCallback
    interface MonitorRequestStatusCallback
    interface TrackRequestedDriverCallback
    interface NoOfRequestsReceivedCallback
    interface TripRequestRemovedCallback


    interface WaitForRequestCallback
    interface RespondToRequestCallback

 */

class TripRequestsRepository {


    var app= AppExecutors()
    val tripSupplyRepository = TripSupplyRepository()



    /// PASSENGERS //////////////////////////////////////////////////////////////////////////////


    interface RemoveTripRequestCallback{

        fun onRequestRemoved()
        fun onFailed(error:String)

    }


    interface IsRequestPendingCallback{

        fun isRequestPending(isPending: Boolean)
        fun onFailed(error:String)

    }

    interface NoOfRequestsReceivedCallback{

        fun onRequestReceived(noOfRequest:Int)
        fun onFailed(error:String)

    }


    interface MonitorRequestStatusCallback{

        fun onRequestResponded(request: TripRequest)
        fun onRequestFailed(error:String)
    }

    interface TrackRequestedDriverCallback{

        fun onSuccess(info: String)
        fun onFailed(error:String)
    }



    interface SendRequestCallback{

        fun onRequestSent(res: TripResponse)
        fun onRequestSeatsUnAvailable(msg:String)
        fun onRequestFailed(error:String)
    }



     ///Track Request coming
    fun trackDriverByLocation(tripId: String, myLocation: Location, driverArrivedCallback: TrackRequestedDriverCallback){
        //Ill go to carSupplyOnline cars that are comming
        //and i will get the tripId of the request that was accepted
        // and track the car supply online
        //remember the loc is always changing so the method will be called all the time

        var run = Runnable {

            val childEventListener = object :
                ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                   // Log.d("DRIVER-REQUEST-WAIT", "onChildChanged:")

                    //val request = snapshot.getValue(TripRequest::class.java)
                    Log.d(
                        "TripRepo-D",
                        "Data Exists: Ondata change"
                    )

                    if(dataSnapshot.exists()){

                        Log.d("TripRepo-D", "Data Exists")

                       //  val map = dataSnapshot.value as HashMap<String,TripLocation>
                       //  val listOfValues = ArrayList(map.values)

                        val location =
                            GeoFire.getLocationValue(
                                dataSnapshot
                            )

                        var driverLocation =
                            Location(LocationManager.GPS_PROVIDER)
                        driverLocation.latitude = location.latitude
                        driverLocation.longitude = location.longitude

                        var distance = myLocation.distanceTo(driverLocation)
                        var distanceKm = distance/1000

                        Log.d("TripRepo-D", "Data Exists; distance")

                        if(distanceKm < 50){
                            Log.d(
                                "TripRepo-D",
                                "distance less: " + distanceKm
                            )
                            var runMain = Runnable {
                                driverArrivedCallback.onSuccess("$distanceKm km")
                            }
                            app.mainThread().execute(runMain)

                        }else if(distanceKm > 100){
                            Log.d(
                                "TripRepo-D",
                                "distance more: " + distanceKm
                            )
                            var runMain = Runnable {
                                //driverArrivedCallback.onSuccess("$distanceKm km")
                            }
                            app.mainThread().execute(runMain)

                        }
                    }

                }

                override fun onCancelled(error: DatabaseError) {
                      driverArrivedCallback.onFailed(error.message)
                }
            }

            // databaseReference.child(DBConfig.refRequests).equalTo(tripId).addListenerForSingleValueEvent(childEventListener)

            reference.child(DBConfig.refCarSupplyOnline).child(tripId).addValueEventListener(childEventListener)

        }
        app.networkIO().execute(run)

    }

    private fun getLocationValue(dataSnapshot: DataSnapshot): GeoLocation? {
        return try {
            val typeIndicator: GenericTypeIndicator<Map<String?, Any?>?> =
                object :
                    GenericTypeIndicator<Map<String?, Any?>?>() {}
            val data =
                dataSnapshot.getValue(
                    typeIndicator
                )!!
            val location = data["l"] as List<*>?
            val latitudeObj = location!![0] as Number
            val longitudeObj = location[1] as Number
            val latitude: Double = latitudeObj.toDouble()
            val longitude: Double = longitudeObj.toDouble()
            if (location.size == 2 && GeoLocation.coordinatesValid(
                    latitude,
                    longitude
                )
            ) {
                GeoLocation(latitude, longitude)
            } else {
                null
            }
        } catch (e: NullPointerException) {
            null
        } catch (e: ClassCastException) {
            null
        }
    }

    fun sendDriverARequest(tripRequest: TripRequest, noOfSeatsRequired: String, callback: SendRequestCallback){

         // res.driversCurrentLocation =  LatLng(37.424601,-122.094098)  //Garcia Ave, Mountain View, CA 94043, USA
         getNoOfRequests(tripRequest.tripId!!, object: NoOfRequestsReceivedCallback {

             override fun onRequestReceived(noOfRequest: Int) {

                 var noOfRequiredSeats =  noOfSeatsRequired.toInt()

                 if(noOfRequest <= noOfRequiredSeats){

                     sendRequest(tripRequest,callback)

                 }else{

                     var seats = "All $noOfRequest seats booked"
                     callback.onRequestSeatsUnAvailable(seats)

                 }

             }

             override fun onFailed(error: String) {

             }

         })


        //implement request from here

    }

       private fun sendRequest(tripRequest: TripRequest, callback: SendRequestCallback){

        var res = TripResponse()
        var myrun = Runnable {

            //add a request and its Id
            reference.child(DBConfig.refRequests).child(tripRequest.requestId!!).setValue(tripRequest)
            reference.addListenerForSingleValueEvent(object :
                ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    //  dbRefTripRequests.removeEventListener(this)
                    //  convert the doubles to LatLong
                    var location = LatLng(
                        tripRequest.origin!!.latitude!!,
                        tripRequest.destination!!.longitude!!
                    )


                    //Create the response
                    res.isRequestSent = true
                    res.responseMessage = "success"
                    res.requestId = tripRequest.requestId
                    res.tripId = tripRequest.tripId


                    var latLng = LatLng(
                        tripRequest.origin!!.latitude!!,
                        tripRequest.origin!!.longitude!!
                    )
                    res.driverOrigin = latLng
                    ///////////////////////////////



                    var uithread = Runnable {
                        callback.onRequestSent(res)
                    }
                    app.mainThread().execute(uithread)


                }

                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {
                        callback.onRequestFailed(databaseError.message)
                    }
                    app.mainThread().execute(uithread)


                }
            })
        }
        app.diskIO().execute(myrun)

    }

       private fun getNoOfRequests(tripId: String, noOfRequestsReceivedCallback: NoOfRequestsReceivedCallback){

        var run = Runnable {

            val childEventListener = object :
                ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {

                    var totalSize = snapshot.childrenCount.toInt()
                    noOfRequestsReceivedCallback.onRequestReceived(totalSize)

                    /*
                    for (postSnapshot in snapshot.children) {
                        val request = postSnapshot.getValue(TripRequest::class.java)
                        requests.add(message!!)
                    }
                     */
                }

                override fun onCancelled(error: DatabaseError) {
                    noOfRequestsReceivedCallback.onFailed(error.message)
                }
            }

            //The the trips of a certain
            var query = reference.child(DBConfig.refRequests).orderByChild("tripId").equalTo(tripId).addValueEventListener(childEventListener)


        }
        app.diskIO().execute(run)



    }


    fun isRequestPending(passengerId: String, isRequestPendingCallback: IsRequestPendingCallback){

        var run = Runnable {

            val childEventListener = object :
                ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {

                    for (postSnapshot in snapshot.children) {
                        // TODO: handle the post
                        val req = postSnapshot.getValue(TripRequest::class.java)
                        if(req?.passengerId == passengerId){
                           if(req.requestStatus == TripRequest.PENDING){

                               AppExecutors().mainThread().execute(Runnable {
                                   isRequestPendingCallback.isRequestPending(true)
                               })

                           }else{
                               AppExecutors().mainThread().execute(Runnable {
                                   isRequestPendingCallback.isRequestPending(false)
                               })
                           }
                        }else{

                        }
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    isRequestPendingCallback.onFailed(error.message)
                }
            }

            //The the trips of a certain
            var query = reference.child(DBConfig.refRequests)
                .addListenerForSingleValueEvent(childEventListener)

        }
        app.diskIO().execute(run)

    }


    fun monitorSentRequestAcceptance(tripId:String, monitorRequestCallback: MonitorRequestStatusCallback) {

        // The driver will track a req db by his trip Id
        // So the request child ref should be headed by a trip Id
        var run = Runnable {

            val childEventListener = object :
                ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {

                    // onChildAdded() will be called for each node at the first time
                    val request = snapshot.getValue(TripRequest::class.java)

                    //   Log.d("DRIVER-REQUEST-LISTNER", "onChildChanged:" + request?.passengerId +" tripid: "+request?.tripId +" status: "+request?.isAccepted)


                    var runMain = Runnable {

                        if (request != null) {
                            monitorRequestCallback.onRequestResponded(request)
                        }

                    }
                    app.mainThread().execute(runMain)
                    // MainThreadExecutor(500).execute(runMain)


                }

                override fun onCancelled(error: DatabaseError) {

                }
            }
            reference.child(DBConfig.refRequests).child(tripId)
                .addValueEventListener(childEventListener)

           }
            app.diskIO().execute(run)

        }
    //// PASSENGERS END ////////////////////////////////////////////////////////////////////////////////////








    //// DRIVERS ///////////////////////////////////////////////////////////////////////////////////////////

    interface WaitForRequestCallback{

        fun onRequestReceived(requests: MutableList<TripRequest>)
        fun onRequestFailed(error:String)
    }


    interface RespondToRequestCallback{

        fun onResponded()
        fun onResponseFailed(error:String)
    }


    var waitedRequests = ArrayList<TripRequest>()
    fun waitForRequest(tripId : String, waitForRequestCallback: WaitForRequestCallback){

        Log.d("DRIVER-REQUEST-WAIT", "onChildStart:")

        var run = Runnable {

            val childEventListener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    Log.d("DRIVER-REQUEST-WAIT", "onChildChanged:")

                    //IF THERE IS A CHANGE GET ALL THE REQUESTS OF THAT ID
                    for (dataSnapshot in snapshot.children){
                        // onChildAdded() will be called for each node at the first tiim
                        val request = dataSnapshot.getValue(TripRequest::class.java)
                        if(request?.tripId == tripId) {
                            waitedRequests.add(request)
                        }
                    }
                    var runMain = Runnable {

                        if(waitedRequests.isNotEmpty()) {
                            waitForRequestCallback.onRequestReceived(waitedRequests)
                        }else{
                            waitForRequestCallback.onRequestFailed("Empty")
                        }

                    }
                    app.mainThread().execute(runMain)
                    // MainThreadExecutor(500).execute(runMain)

                }

                override fun onCancelled(error: DatabaseError) {

                }
            }

            // databaseReference.child(DBConfig.refRequests).equalTo(tripId).addListenerForSingleValueEvent(childEventListener)

            reference.child(DBConfig.refRequests).addValueEventListener(childEventListener)

        }
        app.diskIO().execute(run)


    }


    private var refRespondToRequest : DatabaseReference ? =null
    private var listenerRespondToRequest : ValueEventListener ? =null
    fun respondToRequest(driverId:String,request: TripRequest, tripRequestResponse: String, callback: RespondToRequestCallback){

        // remove pending trips after request has been attended to, pending requests are for the driver to accept  on resume
        Log.d("TripRequestID:",""+request.requestId!!+" Res: "+tripRequestResponse)

        var run = Runnable {
            refRespondToRequest =    reference.child(DBConfig.refRequests).child(request.requestId!!).child("requestStatus")
            refRespondToRequest!!.setValue(tripRequestResponse)

            listenerRespondToRequest =
                object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {

                        tripSupplyRepository.removePendingTrip(driverId,object:TripSupplyRepository.RemovePendingTripCallback{

                            override fun onSuccess() {
                                refRespondToRequest?.removeEventListener(listenerRespondToRequest!!)

                                var uithread = Runnable {
                                    callback.onResponded()

                                }
                                app.mainThread().execute(uithread)

                            }

                            override fun onFailed(error: String) {

                                var uithread = Runnable {
                                    callback.onResponseFailed(error)
                                }
                                app.mainThread().execute(uithread)

                            }

                        })


                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        var uithread = Runnable {
                            callback.onResponseFailed(databaseError.message)
                        }
                        app.mainThread().execute(uithread)

                    }
                }

            refRespondToRequest!!.addListenerForSingleValueEvent(listenerRespondToRequest!!)


        }
        app.networkIO().execute(run)

    }



    fun removeTripRequest(requestId:String, callback: RemoveTripRequestCallback){

        // remove pending trips after request has been attended to, pending requests are for the driver to accept  on resume

        var run = Runnable {
            var reftaks =    reference.child(DBConfig.refRequests).child(requestId)

            reftaks.removeValue()
            reftaks.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    var uithread = Runnable {
                        callback.onRequestRemoved()
                    }
                    app.mainThread().execute(uithread)
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    var uithread = Runnable {
                        callback.onFailed(databaseError.message)
                    }
                    app.mainThread().execute(uithread)

                }
            })

        }
        app.diskIO().execute(run)



    }








}